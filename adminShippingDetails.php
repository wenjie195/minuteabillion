<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BidData.php';
require_once dirname(__FILE__) . '/classes/BidRecord.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<meta property="og:title" content="<?php echo _ADMIN_SHIPPING_DETAILS ?> | Bid Win 劲拍" />
<title><?php echo _ADMIN_SHIPPING_DETAILS ?> | Bid Win 劲拍</title>
<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header.php'; ?>

<div class="width100 black-bg min-height menu-distance same-padding text-center force-center">
	<img src="img/shipping.png" class="title-icon" alt="<?php echo _ADMIN_SHIPPING_DETAILS ?>" title="<?php echo _ADMIN_SHIPPING_DETAILS ?>">
	<h1 class="title-h1 blue-text opacity-hover"  onclick="goBack()"><img src="img/back.png" alt="<?php echo _BOTTOM_BACK ?>" title="<?php echo _BOTTOM_BACK ?>" class="back-png"><?php echo _ADMIN_SHIPPING_DETAILS3 ?></h1>
	<div class="title-border margin-bottom30"></div>
    <div class="clear"></div>    
    
    <?php
    if(isset($_POST['bid_uid']))
    {
    $conn = connDB();
    $bidDetails = getBidRecord($conn,"WHERE uid = ? ", array("uid") ,array($_POST['bid_uid']),"s");
    ?>

    <form method="POST"  action="utilities/adminUpdateShippingMethodFunction.php">
    <!-- <form method="POST"  action="#"> -->

		<div class="width100 overflow">
                <?php 
                    $bidUid = $bidDetails[0]->getTradeUid();
                    $conn = connDB();
                    $bidData = getBidData($conn,"WHERE uid = ? ", array("uid") ,array($bidUid),"s");
                    $bidImage = $bidData[0]->getImage(); 
                ?>
        	<img src="bidItemImage/<?php echo $bidImage; ?>"class="item-png" alt="">
            <p class="item-name-p"><?php echo $bidDetails[0]->getItemName();?></p>
        </div>
        <div class="dual-input">
            <p class="input-top-p"><?php echo _BOTTOM_BID ?></p>
           	<p class="no-input"><?php echo $bidDetails[0]->getAmount();?> <?php echo _PROFILE_CREDITS ?></p>
        </div>
        <div class="dual-input second-dual-input">
            <p class="input-top-p"><?php echo _PROFILE_AUCTION_ID ?></p>
            <p class="no-input">
                <?php echo $bidId = $bidData[0]->getBidId(); ?>
            </p>
        </div>

        <div class="clear"></div>

        <div class="dual-input">
            <p class="input-top-p"><?php echo _ADMIN_WINNER ?></p>
            <p class="no-input"><?php echo $bidDetails[0]->getUsername();?></p>
        </div>
        <div class="dual-input second-dual-input">
            <p class="input-top-p"><?php echo _ADMIN_BID_ENDING_TIME ?></p>
            <p class="no-input">
                <?php echo $bidId = $bidData[0]->getEndingTime();;?>
            </p>
            <!-- <input class="input-name clean" type="text" placeholder="<?php //echo _MAINJS_INDEX_CONTACT ?>" value="<?php //echo $userData->getPhone() ;?>" id="update_contact" name="update_contact" required> -->
        </div>

        <div class="clear"></div>

        <div class="dual-input">
            <p class="input-top-p"><?php echo _MAINJS_INDEX_CONTACT ?></p>
            <p class="no-input">
                <?php 
                    $userUid = $bidDetails[0]->getUserUid();
                    $conn = connDB();
                    $userData = getUser($conn,"WHERE uid = ? ", array("uid") ,array($userUid),"s");
                    echo $userPhoneNo = $userData[0]->getPhone();
                ?>
            </p>
        </div>
        <div class="dual-input second-dual-input">
            <p class="input-top-p"><?php echo _MAINJS_INDEX_EMAIL ?></p>
            <p class="no-input"><?php echo $userEmail = $userData[0]->getEmail();;?></p>
        </div>

        <div class="clear"></div>

        <div class="dual-input">
            <p class="input-top-p"><?php echo _MAINJS_INDEX_ADDRESS ?></p>
            <p class="no-input"><?php echo $userEmail = $userData[0]->getAddress();;?></p>
        </div>
        <div class="dual-input second-dual-input">
            <p class="input-top-p"><?php echo _MAINJS_INDEX_POSTCODE ?></p>
            <p class="no-input"><?php echo $userEmail = $userData[0]->getPostcode();;?></p>
        </div>

        <div class="clear"></div>

        <div class="dual-input">
            <p class="input-top-p"><?php echo _MAINJS_INDEX_CITY ?></p>
            <p class="no-input"><?php echo $userEmail = $userData[0]->getCity();;?></p>
        </div>
        <div class="dual-input second-dual-input">
            <p class="input-top-p"><?php echo _MAINJS_INDEX_COUNTRY ?></p>
            <p class="no-input"><?php echo $userEmail = $userData[0]->getCountry();;?></p>
        </div>

        <div class="clear"></div>

        <div class="dual-input">
            <p class="input-top-p"><?php echo _ADMIN_SHIPPING_METHOD ?></p>
            <input class="input-name clean" type="text" placeholder="<?php echo _ADMIN_SHIPPING_METHOD ?>" id="shipping_method" name="shipping_method" required>
        </div>
        <div class="dual-input second-dual-input">
            <p class="input-top-p"><?php echo _ADMIN_DELIVERY_DATE ?></p>
            <input class="input-name clean" type="datetime-local" placeholder="<?php echo _ADMIN_DELIVERY_DATE ?>" id="shipping_datetime" name="shipping_datetime" required>
        </div>

        <input class="input-name clean" type="hidden" value="<?php echo $_POST['bid_uid'];?>" id="bid_uid" name="bid_uid" readonly>

        <div class="width100 text-center margin-top20">
            <button class="blue-button white-text clean pointer" name="submit"><?php echo _MAINJS_INDEX_SUBMIT ?></button>
        </div>

    </form>

    <?php
    }
    ?>

</div>

<?php include 'bottomButton.php'; ?>
<?php include 'js.php'; ?>

</body>
</html>