<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<meta property="og:title" content="<?php echo _ADMIN_REJECT ?> | Bid Win 劲拍" />
<title><?php echo _ADMIN_REJECT ?> | Bid Win 劲拍</title>
<?php include 'css.php'; ?>
</head>
<body class="body">

<div class="width100 min-height100 menu-distance same-padding text-center">
    <img src="img/register.png" class="title-icon" alt="<?php echo _ADMIN_REASON_OF_REJECT ?>" title="<?php echo _ADMIN_REASON_OF_REJECT ?>">
    <h1 class="title-h1 blue-text"><?php echo _ADMIN_REASON_OF_REJECT3 ?></h1>
    <div class="title-border margin-bottom30"></div>
    <div class="width100 overflow">
    <form>
            <p class="input-top-p"><?php echo _ADMIN_REASON_OF_REJECT ?></p>
            <textarea class="input-name clean input-textarea" type="text" placeholder="<?php echo _ADMIN_REASON_OF_REJECT ?>"  required></textarea>                       
                        <button class="blue-button clean"><?php echo _MAINJS_INDEX_SUBMIT ?></button>
                        
                           
    </form>    
    </div>
</div>

<?php include 'js.php'; ?>


<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Profile Updated !!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "fail to update profile !!"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }

    if($_SESSION['messageType'] == 2)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Password Updated !!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "fail to update password !!"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "New Password does not match with Retype Password !!";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "password length must be more that 5"; 
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "Wrong Password !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>