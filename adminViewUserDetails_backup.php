<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BidRecord.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

// $uid = $_SESSION['uid'];

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<meta property="og:title" content="<?php echo _ADMIN_MEMBER ?> | minuteabillion" />
<title><?php echo _ADMIN_MEMBER ?> | minuteabillion</title>
<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header.php'; ?>

<div class="width100 black-bg min-height menu-distance same-padding text-center">

    <img src="img/profile-page.png" class="title-icon" alt="<?php echo _ADMIN_MEMBER ?>" title="<?php echo _ADMIN_MEMBER ?>">
    <h1 class="title-h1 blue-text"><?php echo _ADMIN_MEMBER_DETAILS ?></h1>
    <div class="title-border"></div>
    <div class="clear"></div> 
    
    <?php
    if(isset($_POST['user_uid']))
    {
    $conn = connDB();
    $userDetails = getUser($conn,"WHERE uid = ? ", array("uid") ,array($_POST['user_uid']),"s");
    ?>

        <!-- <form method="POST"  action="utilities/adminEditUserProfileFunction.php"> -->

            <div class="dual-input">
                <p class="input-top-p"><?php echo _MAINJS_INDEX_NICKNAME ?></p>
                <input class="input-name clean" type="text" value="<?php echo $userDetails[0]->getNickname() ;?>" id="update_nickname" name="update_nickname" readonly>
            </div>
            <div class="dual-input second-dual-input">
                <p class="input-top-p"><?php echo _MAINJS_INDEX_USERNAME ?></p>
                <input class="input-name clean" type="text" value="<?php echo $userDetails[0]->getUsername() ;?>" id="update_username" name="update_username" readonly>
            </div>

            <div class="clear"></div>

            <div class="dual-input">
                <p class="input-top-p"><?php echo _MAINJS_INDEX_EMAIL ?></p>
                <input class="input-name clean" type="email" value="<?php echo $userDetails[0]->getEmail() ;?>" id="update_email" name="update_email" readonly>
            </div>
            <div class="dual-input second-dual-input">
                <p class="input-top-p"><?php echo _MAINJS_INDEX_CONTACT ?></p>
                <input class="input-name clean" type="text" value="<?php echo $userDetails[0]->getPhone() ;?>" id="update_contact" name="update_contact" readonly>
            </div>

            <div class="clear"></div>

            <div class="dual-input">
                <p class="input-top-p"><?php echo _MAINJS_INDEX_ADDRESS ?></p>
                <input class="input-name clean" type="text" value="<?php echo $userDetails[0]->getAddress() ;?>" id="update_address" name="update_address" readonly>
            </div>
            <div class="dual-input second-dual-input">
                <p class="input-top-p"><?php echo _MAINJS_INDEX_POSTCODE ?></p>
                <input class="input-name clean" type="text" value="<?php echo $userDetails[0]->getPostcode() ;?>" id="update_postcode" name="update_postcode" readonly>
            </div>

            <div class="clear"></div>

            <div class="dual-input">
                <p class="input-top-p"><?php echo _MAINJS_INDEX_CITY ?></p>
                <input class="input-name clean" type="text" value="<?php echo $userDetails[0]->getCity() ;?>" id="update_city" name="update_city" readonly>
            </div>
            <div class="dual-input second-dual-input">
                <p class="input-top-p"><?php echo _MAINJS_INDEX_COUNTRY ?></p>
                <input class="input-name clean" type="text" value="<?php echo $userDetails[0]->getCountry() ;?>" id="update_country" name="update_country" readonly>
            </div>

            <div class="clear"></div>

            <div class="dual-input">
                <p class="input-top-p"><?php echo _JS_USDT_ADDRESS ?></p>
                <input class="input-name clean" type="text" value="<?php echo $userDetails[0]->getUsdtWallet() ;?>" id="update_usdt_add" name="update_usdt_add" readonly>
            </div>
            <div class="dual-input second-dual-input">
                <p class="input-top-p"><?php echo _JS_BTC_ADDRESS ?></p>
                <input class="input-name clean" type="text" value="<?php echo $userDetails[0]->getBtcWallet() ;?>" id="update_btc_add" name="update_btc_add" readonly>
            </div>

            <input class="input-name clean" type="hidden" value="<?php echo $userDetails[0]->getUid() ;?>" id="user_uid" name="user_uid" readonly>

            <!-- <div class="width100 text-center margin-top20">
                <button class="blue-button white-text clean pointer" name="submit"><?php //echo _MAINJS_INDEX_SUBMIT ?></button>
            </div> -->

        <!-- </form>  -->

    <?php
    }
    ?>
                            
</div>

<style>
.profile-div{
background: rgba(9,197,249,1);
background: -moz-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: -webkit-gradient(left top, right bottom, color-stop(0%, rgba(9,197,249,1)), color-stop(100%, rgba(4,92,233,1)));
background: -webkit-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: -o-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: -ms-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: linear-gradient(135deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#09c5f9', endColorstr='#045ce9', GradientType=1 );
}
</style>

<?php include 'bottomButton.php'; ?>
<?php include 'js.php'; ?>

</body>
</html>