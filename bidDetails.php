<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BidData.php';
require_once dirname(__FILE__) . '/classes/BidRecord.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

// $bidDetails = getBidRecord($conn,"WHERE shipping_details = 'Shipped' ");
//$bidDetails = getBidRecord($conn,"WHERE status = 'Win' ");

$bid_uid = $_POST['bid_uid'];
$bidDetails = getBidRecord($conn, "WHERE trade_uid =?",array("trade_uid"),array($bid_uid),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<meta property="og:title" content="<?php echo _ADMIN_ALL_BID ?> | Bid Win 劲拍" />
<title><?php echo _ADMIN_ALL_BID ?> | Bid Win 劲拍</title>
<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header.php'; ?>
<div class="width100 black-bg min-height menu-distance same-padding text-center">

	<img src="img/items.png" class="title-icon" alt="<?php echo _ADMIN_ALL_BID ?>" title="<?php echo _ADMIN_ALL_BID ?>">
	<h1 class="title-h1 blue-text"><?php echo _ADMIN_ALL_BID3 ?></h1>
	<div class="title-border margin-bottom30"></div>
	<div class="clear"></div>
            <!-- <p class="link-p"><a href="adminViewLiveBid.php" class="white-to-blue-link"><//?php echo ?></p> -->
                <div class="table-scroll">
                    <table class="table-css">
                        <thead>
                            <tr>
                                <th><?php echo _PROFILE_NO ?></th>
                                <th><?php echo _ADMIN_MEMBER ?></th>
                                <th><?php echo _PROFILE_AUCTION_ID ?></th>
                                <th><?php echo _PROFILE_ITEM ?></th>
                                <th><?php echo _ADMIN_REQUEST_DATE ?></th>
                                <th><?php echo _PROFILE_STATUS ?></th>
                            </tr>
                        </thead>
                        <tbody>
                        <tbody>
                            <tr>
                                <?php
                                if($bidDetails)
                                {
                                for($cnt = 0;$cnt < count($bidDetails) ;$cnt++)
                                {
                                ?>
                                    <tr>
                                        <td><?php echo ($cnt+1)?></td>
                                        <td><?php echo $bidDetails[$cnt]->getUsername();?></td>
                                        <td>
                                            <?php 
                                                $tradeUid = $bidDetails[$cnt]->getTradeUid();
                                                $conn = connDB();
                                                $bidData = getBidData($conn,"WHERE uid = ? ", array("uid") ,array($tradeUid),"s");
                                                echo $bidId = $bidData[0]->getBidId();
                                            ?>
                                        </td>
                                        <td><?php echo $bidDetails[$cnt]->getItemName();?></td>
                                        <td><?php echo $bidDetails[$cnt]->getDateUpdated();?></td>
                                        <td><?php echo $bidDetails[$cnt]->getStatus();?></td>
                                    </tr> 
                                <?php
                                }
                                }
                                ?>  
                            </tr>                    	                                  
                        </tbody>                         
                        </tbody>
                    </table>

                    <div class="clear";>

                    <form method="POST" action="utilities/bidDetailsFunction.php">  
                     <!-- <//?php
                        if($bidDetails)
                        {
                            for($cnt = 0;$cnt < count($bidDetails) ;$cnt++)
                            {
                                if($bidDetails[$cnt]->getTradeUid()==$bid_uid)
                                {
                                ?>
                                <input type="hidden" value="<//?php echo $bidDetails[$cnt]->getStatus();?>" >
                                <input type="hidden" value="<//?php echo $bidDetails[$cnt]->getUid()."<br>"; ?>" >
                                <input type="hidden" value="<//?php echo $bidDetails[$cnt]->getUserUid()."<br>"; ?>" >
                                <//?php
                                }
                            }
                        }
                    ?> -->
                        <div>
                            <button class="clean blue-button small-btn" type="submit" name="bid_uid" value="<?php echo $bid_uid;?>">
                                <?php echo "Revert Credit"; ?>
                            </button>
                        </div>
                    </form> 
                
                </div>             
    	</div>
    
</div>



<?php include 'bottomButton.php'; ?>
<?php include 'js.php'; ?>

<!-- <script>
$('.table-css tr').click(function(){
   window.location.href = $(this).data('link');
});
</script> -->

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Shipped Details Updated !!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "fail to update shipping details !!"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR !!";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "Revert Winning Record !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>