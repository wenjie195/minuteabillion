<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BidData.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

// $uid = $_SESSION['uid'];
// if (isset($_SESSION['uid']))
// {
//     $uid = $_SESSION['uid'];
// }

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<meta property="og:title" content="minuteabillion" />
<title>minuteabillion</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 black-bg min-height menu-distance">
    <div class="banner-div width100 overflow same-padding" id="firefly" >
    	<div class="left-banner-content float-left">
        	<h1 class="white-banner-title"><?php echo _INDEX_BID_TO_WIN ?></h1>
            <div class="fake-blue-button"><?php echo _INDEX_JOIN_US_NEXT_BID ?></div>
        </div>
        <div class="right-coin-content float-right">
        	<img src="img/coin.png"  alt="minuteabillion" title="minuteabillion" data-wow-iteration="infinite" data-wow-duration="6s" class="span3 wow pulse coin-png animated">
        </div>
    </div>
    <div class="width100 overflow same-padding">
    	<div class="width100 overflow text-center section-padding">
        	<img src="img/ranking.png" class="title-icon" alt="<?php echo _INDEX_RANKING ?>" title="<?php echo _INDEX_RANKING ?>">
            <h1 class="title-h1 blue-text"><?php echo _INDEX_RANKING_BOARD ?></h1>
            <div class="title-border"></div>
            <div class="clear"></div>
            <h2 class="white-text month-text">August 2020</h2>
            <div class="second-rank rank-div lower-rank">
            	<div class="rank-profile silver-div">
                	<img src="img/profile-02.png" class="profile-pic">
                </div>
                <p class="gold-text rank-name">2. Jess <img src="img/crown2.png" class="crown"></p>
                <p class="white-text rank-credit">900 <?php echo _PROFILE_CREDITS ?></p>
            </div>
            <div class="first-rank rank-div">
            	<div class="rank-profile gold-div">
                	<img src="img/profile-01.png" class="profile-pic">
                </div>
 				<p class="gold-text rank-name">1. Jack <img src="img/crown1.png" class="crown"></p>
                <p class="white-text rank-credit">1000 <?php echo _PROFILE_CREDITS ?></p>               
            </div>  
            <div class="third-rank rank-div lower-rank">
            	<div class="rank-profile bronze-div">
                	<img src="img/profile-03.png" class="profile-pic">
                </div>
 				<p class="gold-text rank-name">3. Angela <img src="img/crown3.png" class="crown"></p>
                <p class="white-text rank-credit">800 <?php echo _PROFILE_CREDITS ?></p>               
            </div>
            <div class="clear"></div>
            <div class="table-scroll">
            	<table class="table-css">
                	<thead>
                    	<tr>
                        	<th><?php echo _PROFILE_NO ?></th>
                            <th><?php echo _INDEX_USER ?></th>
                            <th><?php echo _INDEX_TOTAL_BID_AMOUNT ?></th>
                        </tr>
                    </thead>
                	<tbody>
                    	<tr>
                        	<td>4.</td>
                            <td>Maggie</td>
                            <td>700 <?php echo _PROFILE_CREDITS ?></td>
                        </tr>
                    	<tr>
                        	<td>5.</td>
                            <td>Jimmy</td>
                            <td>600 <?php echo _PROFILE_CREDITS ?></td>
                        </tr>                        
                    	<tr>
                        	<td>6.</td>
                            <td>Ken</td>
                            <td>500 <?php echo _PROFILE_CREDITS ?></td>
                        </tr>                        
                    	<tr>
                        	<td>7.</td>
                            <td>Cindy</td>
                            <td>400 <?php echo _PROFILE_CREDITS ?></td>
                        </tr>                          
                    	<tr>
                        	<td>8.</td>
                            <td>Alicia</td>
                            <td>300 <?php echo _PROFILE_CREDITS ?></td>
                        </tr>   
                    	<tr>
                        	<td>9.</td>
                            <td>Amy</td>
                            <td>200 <?php echo _PROFILE_CREDITS ?></td>
                        </tr>                        
                    	<tr>
                        	<td>10.</td>
                            <td>Keith</td>
                            <td>100 <?php echo _PROFILE_CREDITS ?></td>
                        </tr>                                             
                    </tbody>
                </table>
            
            </div>
            <div class="clear"></div>
            <div class="width100 overflow text-center section-padding">
                        <img src="img/bid.png" class="title-icon" alt="<?php echo _INDEX_HOW_TO_BID ?>?" title="<?php echo _INDEX_HOW_TO_BID ?>?">
                        <h1 class="title-h1 blue-text"><?php echo _INDEX_HOW_TO_BID ?>?</h1>
                        <div class="title-border"></div>
                        <div class="clear"></div> 
                        <h2 class="white-text month-text"><?php echo _INDEX_3_STEPS ?></h2>   
                        <div class="three-div">
                        	<img src="img/steps-01.png" class="step-png" alt="<?php echo _INDEX_BIDDING_STEPS ?>" title="<?php echo _INDEX_BIDDING_STEPS ?>">
                            <p class="content-title"><?php echo _INDEX_STEP1 ?></p>
                            <p class="content-text"><?php echo _INDEX_JOIN_MEMBER ?></p>
                        </div>        
                        <div class="three-div mid-three-div">
                        	<img src="img/steps-02.png" class="step-png" alt="<?php echo _INDEX_BIDDING_STEPS ?>" title="<?php echo _INDEX_BIDDING_STEPS ?>">
                            <p class="content-title"><?php echo _INDEX_STEP2 ?></p>
                            <p class="content-text"><?php echo _INDEX_START_YOUR_BID ?></p>
                        </div> 
                        <div class="three-div">
                        	<img src="img/steps-03.png" class="step-png" alt="<?php echo _INDEX_BIDDING_STEPS ?>" title="<?php echo _INDEX_BIDDING_STEPS ?>">
                            <p class="content-title"><?php echo _INDEX_STEP3 ?></p>
                            <p class="content-text"><?php echo _INDEX_MAKE_PAYMENT ?></p>
                        </div>                                                                         
            </div>
            <div class="clear"></div>
            <div class="width100 overflow text-center section-padding">
                        <img src="img/trophy.png" class="title-icon" alt="<?php echo _INDEX_WINNER_BOARD ?>" title="<?php echo _INDEX_WINNER_BOARD ?>">
                        <h1 class="title-h1 blue-text"><?php echo _INDEX_WINNER_BOARD ?></h1>
                        <div class="title-border"></div>
                        <div class="clear"></div> 
                        <div class="table-scroll margin-top30">
                            <table class="table-css">
                                <thead>
                                    <tr>
                                        <th><?php echo _PROFILE_NO ?></th>
                                        <th><?php echo _INDEX_USER ?></th>
                                        <th><?php echo _PROFILE_AUCTION_ID ?></th>
                                        <th><?php echo _PROFILE_ITEM ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1.</td>
                                        <td>Maggie</td>
                                        <td>J123123123</td>
                                        <td>Bitcoin - 0.1</td>
                                    </tr>
                                    <tr>
                                        <td>2.</td>
                                        <td>Jimmy</td>
                                        <td>J123123121</td>
                                        <td>Bitcoin - 0.1</td>
                                    </tr>                        
                                    <tr>
                                        <td>3.</td>
                                        <td>Ken</td>
                                        <td>J123123124</td>
                                        <td>Bitcoin - 0.1</td>
                                    </tr>                        
                                    <tr>
                                        <td>4.</td>
                                        <td>Cindy</td>
                                        <td>J123123125</td>
                                        <td>Bitcoin - 0.1</td>
                                    </tr>                          
                                    <tr>
                                        <td>5.</td>
                                        <td>Alicia</td>
                                        <td>J123123126</td>
                                        <td>Bitcoin - 0.1</td>
                                    </tr>                                              
                                </tbody>
                            </table>
                        
                        </div>                        
                        
            </div>
            <div class="clear"></div>  
            <div class="bidding-big-width margin-top30 text-center ow-index-bid">
                        <img src="img/icon.png" class="title-icon box-icon" alt="<?php echo _INDEX_LATEST_BIDDING_ITEM ?>" title="<?php echo _INDEX_LATEST_BIDDING_ITEM ?>">
                        <h1 class="title-h1 blue-text"><?php echo _INDEX_LATEST_BIDDING_ITEM ?></h1>
                        <div class="title-border margin-bottom30"></div>
                        <div class="clear"></div> 
                        
                            <!-- <div id="divLiveBidding">
                            </div> -->

                        <?php
                        if (isset($_SESSION['uid']))
                        {
                        ?>

                            <div id="divLiveBidding">
                            </div>
						<!-- Repeat -->
                            <div class="bidding-repeat-div">
                                <img src="img/item-01.png" class="step-png" alt="BITCOIN" title="BITCOIN">
                                <p class="content-title bid-item-title text-overflow">BITCOIN</p>
                                <p class="content-text"><?php echo _PROFILE_TOTAL_BIDS ?>: 7</p>
                                <div class="blue-button open-coming"><?php echo _INDEX_PLACE_BID ?></div>
                                <p class="content-text white-text margin-top20" id="timer"></p>
                            </div>  
                         <!-- ENd of Repeat -->
                         <!-- Delete -->      
                            <div class="bidding-repeat-div">
                                <img src="img/item-02.png" class="step-png" alt="100 GRAM GOLD BILLION" title="100 GRAM GOLD BILLION">
                                <p class="content-title bid-item-title text-overflow">100 GRAM GOLD BILLION</p>
                                <p class="content-text"><?php echo _PROFILE_TOTAL_BIDS ?>: 7</p>
                                <div class="blue-button open-coming"><?php echo _INDEX_PLACE_BID ?></div>
                                <p class="content-text white-text margin-top20" id="timer2"></p>
                            </div> 
                            <div class="bidding-repeat-div">
                                <img src="img/item-03.png" class="step-png" alt="PURE GOLD" title="PURE GOLD">
                                <p class="content-title bid-item-title text-overflow">PURE GOLD</p>
                                <p class="content-text"><?php echo _PROFILE_TOTAL_BIDS ?>: 7</p>
                                <div class="blue-button open-coming"><?php echo _INDEX_PLACE_BID ?></div>
                                <p class="content-text white-text margin-top20" id="timer3"></p>
                            </div>   
						<!-- End of Delete -->
                        <?php
                        }
                        else
                        {
                        ?>

						<!-- Repeat -->
                            <div class="bidding-repeat-div">
                                <img src="img/item-01.png" class="step-png" alt="BITCOIN" title="BITCOIN">
                                <p class="content-title bid-item-title text-overflow">BITCOIN</p>
                                <p class="content-text"><?php echo _PROFILE_TOTAL_BIDS ?>: 7</p>
                                <div class="blue-button open-login"><?php echo _INDEX_PLACE_BID ?></div>
                                <p class="content-text white-text margin-top20" id="timer"></p>
                            </div>  
                         <!-- ENd of Repeat -->
                         <!-- Delete -->      
                            <div class="bidding-repeat-div">
                                <img src="img/item-02.png" class="step-png" alt="100 GRAM GOLD BILLION" title="100 GRAM GOLD BILLION">
                                <p class="content-title bid-item-title text-overflow">100 GRAM GOLD BILLION</p>
                                <p class="content-text"><?php echo _PROFILE_TOTAL_BIDS ?>: 7</p>
                                <div class="blue-button open-login"><?php echo _INDEX_PLACE_BID ?></div>
                                <p class="content-text white-text margin-top20" id="timer2"></p>
                            </div> 
                            <div class="bidding-repeat-div">
                                <img src="img/item-03.png" class="step-png" alt="PURE GOLD" title="PURE GOLD">
                                <p class="content-title bid-item-title text-overflow">PURE GOLD</p>
                                <p class="content-text"><?php echo _PROFILE_TOTAL_BIDS ?>: 7</p>
                                <div class="blue-button open-login"><?php echo _INDEX_PLACE_BID ?></div>
                                <p class="content-text white-text margin-top20" id="timer3"></p>
                            </div>   
						<!-- End of Delete -->
                        <?php
                        }
                        ?>

                        <!-- <div class="three-div">
                        	<img src="img/item-01.png" class="step-png" alt="Bidding Item" title="Bidding Item">
                            <p class="content-title bid-item-title">BITCOIN</p>
                            <p class="content-text">Total Bids: 7</p>
                            <div class="blue-button open-bid">Place Bid</div>
                            <p class="content-text white-text margin-top20" id="timer"></p>
                        </div>        
                        <div class="three-div mid-three-div">
                        	<img src="img/item-02.png" class="step-png" alt="Bidding Item" title="Bidding Item">
                            <p class="content-title bid-item-title">100 GRAM GOLD BILLION</p>
                            <p class="content-text">Total Bids: 7</p>
                            <div class="blue-button open-bid">Place Bid</div>
                            <p class="content-text white-text margin-top20" id="timer2"></p>
                        </div> 
                        <div class="three-div">
                        	<img src="img/item-03.png" class="step-png" alt="Bidding Item" title="Bidding Item">
                            <p class="content-title bid-item-title">PURE GOLD</p>
                            <p class="content-text">Total Bids: 7</p>
                            <div class="blue-button open-bid">Place Bid</div>
                            <p class="content-text white-text margin-top20" id="timer3"></p>
                        </div>                                                                          -->
            </div>            
            <div class="clear"></div>
            <div class="width100 overflow text-center section-padding">
                <img src="img/contact.png" class="title-icon phone-icon" alt="<?php echo _INDEX_CONTACT_US2 ?>" title="<?php echo _INDEX_CONTACT_US2 ?>">
                <h1 class="title-h1 blue-text"><?php echo _INDEX_CONTACT_US ?></h1>
                <div class="title-border"></div>
                <div class="clear"></div>
                <h2 class="white-text month-text"><?php echo _INDEX_GET_IN_TOUCH ?></h2> 
                <p class="month-text"><img src="img/email.png" class="mail-png" alt="<?php echo _MAINJS_INDEX_EMAIL ?>" title="<?php echo _MAINJS_INDEX_EMAIL ?>"> hello@bidwin.online</p>			
                <p class="month-text open-contact opacity-hover"><img src="img/support.png" class="mail-png" alt="<?php echo _INDEX_CONTACT_US2 ?>" title="<?php echo _INDEX_CONTACT_US2 ?>"> <?php echo _INDEX_CONTACT_US2 ?></p>
                <p class="month-text opacity-hover"><a href="biddingRules.php"><img src="img/rules.png" class="mail-png" alt="<?php echo _BIDDING_RULES2 ?>" title="<?php echo _INDEX_CONTACT_US2 ?>"> <?php echo _BIDDING_RULES2 ?></a></p>           
            </div>
            <div class="clear"></div>
            <div class="width100 overflow text-center section-padding">
                <img src="img/shipping.png" class="title-icon" alt="<?php echo _INDEX_SHIPPING_METHOD ?>" title="<?php echo _INDEX_SHIPPING_METHOD ?>">
                <h1 class="title-h1 blue-text"><?php echo _INDEX_SHIPPING_METHOD ?></h1>
                <div class="title-border margin-bottom30"></div>
                <div class="clear"></div>
                <img src="img/shipping-company-01.png" class="web-shipping shipping-icon" alt="ups" title="ups">
                <img src="img/shipping-company-02.png" class="web-shipping shipping-icon" alt="DHL" title="DHL">
                <img src="img/shipping-company-03.png" class="web-shipping shipping-icon"  alt="SF EXPRESS" title="SF EXPRESS">
                <img src="img/shipping-company-04.png" class="web-shipping shipping-icon last-shipping-icon" alt="FedEx" title="FedEx">
                <img src="img/shipping-method-01.png" class="mobile-shipping width100" >
            </div>            
            <div class="clear"></div>
            <div class="width100 overflow text-center section-padding">
                        <img src="img/payment.png" class="title-icon" alt="<?php echo _INDEX_PAYMENT_METHOD ?>" title="<?php echo _INDEX_PAYMENT_METHOD ?>">
                        <h1 class="title-h1 blue-text"><?php echo _INDEX_PAYMENT_METHOD ?></h1>
                        <div class="title-border margin-bottom30"></div>
                        <div class="clear"></div> 
                        <p class="white-text content-text"><?php echo _INDEX_WE_ACCEPT ?></p> 
                        <img src="img/payment-method.png" class="payment-method" alt="Bitcoin USDT"    title="Bitcoin USDT">          
            </div>  
    </div>
    



</div>
<style>
.home-div{
background: rgba(9,197,249,1);
background: -moz-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: -webkit-gradient(left top, right bottom, color-stop(0%, rgba(9,197,249,1)), color-stop(100%, rgba(4,92,233,1)));
background: -webkit-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: -o-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: -ms-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: linear-gradient(135deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#09c5f9', endColorstr='#045ce9', GradientType=1 );	
}

</style>

<?php include 'bottomButton.php'; ?>
<?php include 'js.php'; ?>

<script type="text/javascript">
    $(document).ready(function()
    {
        $("#divLiveBidding").load("indexLiveBid.php");
    setInterval(function()
    {
        $("#divLiveBidding").load("indexLiveBid.php");
    }, 1000);
    });
</script>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully Login!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Wrong Password !!"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "There are no user with this username ! <br> Please register !";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }

    if($_SESSION['messageType'] == 2)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Reset password link has been sent to your email !"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "no user with ths email !!"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "wrong email format !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }

    if($_SESSION['messageType'] == 3)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Register Successfully !!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "fail to register !!"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "username and email has been registered by others <br> please try a new one !!";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "password length must more than 5 !!";
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "Password does not match with Retype-Password !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }

}
?>

<script type="text/javascript" src="js/firefly.js"></script>
<script>
  $.firefly({
    color: '#3399ff',
    minPixel: 1,
    maxPixel: 4,
    total : 60,
    on: '#firefly'
});</script>
<script>

// Set the date we're counting down to
var countDownDate = new Date("Aug 30, 2020 15:37:25").getTime();

// Update the count down every 1 second
var x = setInterval(function() {

  // Get today's date and time
  var now = new Date().getTime();

  // Find the distance between now and the count down date
  var distance = countDownDate - now;

  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);

  // Display the result in the element with id="demo"
  document.getElementById("timer").innerHTML = days + "d " + hours + "h "
  + minutes + "m " + seconds + "s ";
  document.getElementById("timer2").innerHTML = days + "d " + hours + "h "
  + minutes + "m " + seconds + "s ";
  document.getElementById("timer3").innerHTML = days + "d " + hours + "h "
  + minutes + "m " + seconds + "s ";  
  // If the count down is finished, write some text
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("timer").innerHTML = "Ended";
	document.getElementById("timer2").innerHTML = "Ended";
	document.getElementById("timer3").innerHTML = "Ended";
  }
}, 1000);
</script>
<?php

if( $_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['send_email_button'])) {

    // EDIT THE 2 LINES BELOW AS REQUIRED
    $email_to = "sherry2.vidatech@gmail.com, hello@bidwin.online";
    $email_subject = "Contact Form via minuteabillion website";
 
    function died($error) 
	{
        // your error code can go here
		echo '<script>alert("We are very sorry, but there were error(s) found with the form you submitted.\n\nThese errors appear below.\n\n';
		echo $error;
        echo '\n\nPlease go back and fix these errors.\n\n")</script>';
        die();
    }
 
 
    // validation expected data exists
    if(!isset($_POST['name']) ||
        !isset($_POST['email']) ||
		!isset($_POST['telephone'])) {
        died('We are sorry, but there appears to be a problem with the form you submitted.');       
    }
	
     
 
    $first_name = $_POST['name']; // required
    $email_from = $_POST['email']; // required
	$telephone = $_POST['telephone']; //required
    $comments = $_POST['comments']; 
    $contactOption = $_POST['contact-option']; // required
    $contactMethod = null;
	
	//$error_message = '<script>alert("The name you entered does not appear to be valid.");</script>';
	//if($first_name == ""){
	//	echo $error_message;
	//}

    if($contactOption == null || $contactOption == ""){
        $contactMethod = "don\'t bother me";
    }else if($contactOption == "contact-more-info"){
        $contactMethod = "I want to be contacted with more information about your company's offering marketing services and consulting";
    }else if($contactOption == "contact-on-request"){
        $contactMethod = "I just want to be contacted based on my request/ inquiry";
    }else{
        $contactMethod = "error getting contact options";
		$error_message .="Error getting contact options\n\n";
    }

    $error_message = "";
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
 
  if(!preg_match($email_exp,$email_from)) {
    $error_message .= 'The email address you entered does not appear to be valid.\n';
  }
 
 
    $string_exp = "/^[A-Za-z .'-]+$/";
 
  if(!preg_match($string_exp,$first_name)) {
    $error_message .= 'The name you entered does not appear to be valid.\n';
  }
 


 
  if(strlen($error_message) > 0) {
    died($error_message);
  }
 
    $email_message = "Form details below.\n\n";
 
     
    function clean_string($string) {
      $bad = array("content-type","bcc:","to:","cc:","href");
      return str_replace($bad,"",$string);
    }
 
    $email_message .= "Name: ".clean_string($first_name)."\n";
    $email_message .= "Email: ".clean_string($email_from)."\n";
	$email_message .= "Telephone: ".clean_string($telephone)."\n";   
    $email_message .= "Message : ".clean_string($comments)."\n";
    $email_message .= "Contact Option : ".clean_string($contactMethod)."\n";

// create email headers
$headers = 'From: '.$email_from."\r\n".
'Reply-To: '.$email_from."\r\n" .
'X-Mailer: PHP/' . phpversion();
@mail($email_to, $email_subject, $email_message, $headers);  
echo '<script>alert("Thank you! We will be in contact shortly!")</script>';

?>
<!-- include your own success html here -->

<!--Thank you for contacting us. We will be in touch with you very soon.-->
<?php
 
}
?>

</body>
</html>