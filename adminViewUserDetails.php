<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BidRecord.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

// $uid = $_SESSION['uid'];

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<meta property="og:title" content="<?php echo _ADMIN_MEMBER ?> | Bid Win 劲拍" />
<title><?php echo _ADMIN_MEMBER ?> | Bid Win 劲拍</title>
<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header.php'; ?>

<div class="width100 black-bg min-height menu-distance same-padding text-center">

    <img src="img/profile-page.png" class="title-icon" alt="<?php echo _ADMIN_MEMBER ?>" title="<?php echo _ADMIN_MEMBER ?>">
    <h1 class="title-h1 blue-text"><?php echo _ADMIN_MEMBER_DETAILS ?></h1>
    <div class="title-border"></div>
    <div class="clear"></div> 
    
    <?php
    if(isset($_POST['user_uid']))
    {
    $conn = connDB();
    $userDetails = getUser($conn,"WHERE uid = ? ", array("uid") ,array($_POST['user_uid']),"s");
    ?>

        <!-- <form method="POST"  action="utilities/adminEditUserProfileFunction.php"> -->
        <form method="POST"  action="utilities/adminBanUserFunction.php" class="force-center">
            <div class="dual-input">
                <p class="input-top-p"><?php echo _MAINJS_INDEX_NICKNAME ?></p>
                <p class="no-input"><?php echo $userDetails[0]->getNickname() ;?></p>
            </div>
            <div class="dual-input second-dual-input">
                <p class="input-top-p"><?php echo _MAINJS_INDEX_USERNAME ?></p>
                <p class="no-input"><?php echo $userDetails[0]->getUsername() ;?></p>
            </div>

            <div class="clear"></div>

            <div class="dual-input">
                <p class="input-top-p"><?php echo _MAINJS_INDEX_EMAIL ?></p>
                <p class="no-input"><?php echo $userDetails[0]->getEmail() ;?></p>
            </div>
            <div class="dual-input second-dual-input">
                <p class="input-top-p"><?php echo _MAINJS_INDEX_CONTACT ?></p>
                <p class="no-input"><?php echo $userDetails[0]->getPhone() ;?></p>
            </div>

            <div class="clear"></div>

            <div class="dual-input">
                <p class="input-top-p"><?php echo _MAINJS_INDEX_ADDRESS ?></p>
                <p class="no-input"><?php echo $userDetails[0]->getAddress() ;?></p>
            </div>
            <div class="dual-input second-dual-input">
                <p class="input-top-p"><?php echo _MAINJS_INDEX_POSTCODE ?></p>
                <p class="no-input"><?php echo $userDetails[0]->getPostcode() ;?></p>
            </div>

            <div class="clear"></div>

            <div class="dual-input">
                <p class="input-top-p"><?php echo _MAINJS_INDEX_CITY ?></p>
                <p class="no-input"><?php echo $userDetails[0]->getCity() ;?></p>
            </div>
            <div class="dual-input second-dual-input">
                <p class="input-top-p"><?php echo _MAINJS_INDEX_COUNTRY ?></p>
                <p class="no-input"><?php echo $userDetails[0]->getCountry() ;?></p>
            </div>

            <div class="clear"></div>

            <div class="dual-input">
                <p class="input-top-p"><?php echo _JS_USDT_ADDRESS ?></p>
                <p class="no-input"><?php echo $userDetails[0]->getUsdtWallet() ;?></p>
            </div>
            <div class="dual-input second-dual-input">
                <p class="input-top-p"><?php echo _JS_BTC_ADDRESS ?></p>
                <p class="no-input"><?php echo $userDetails[0]->getBtcWallet() ;?></p>
            </div>
			<div class="clear"></div>
            <div class="dual-input">
                <p class="input-top-p"><?php echo _PROFILE_STATUS ?></p>
                <select class="input-name clean" id="update_status" name="update_status">
                	<!-- <option><?php //echo _PROFILE_ACTIVE ?></option>
                    <option><?php //echo _PROFILE_BANNED ?></option> -->
                        <?php
                            if($userDetails[0]->getLoginType() == '1')
                            {
                            ?>
                                <option selected value="1"  name='1'><?php echo _PROFILE_ACTIVE ?></option>
                                <option value="2"  name='2'><?php echo _PROFILE_BANNED ?></option>
                            <?php
                            }
                            else if($userDetails[0]->getLoginType() == '2')
                            {
                            ?>
                                <option value="1"  name='1'><?php echo _PROFILE_ACTIVE ?></option>
                                <option selected value="2"  name='2'><?php echo _PROFILE_BANNED ?></option>
                            <?php
                            }
                        ?>
                </select>
            </div>            
            <div class="clear"></div>
            
            <input class="input-name clean" type="hidden" value="<?php echo $userDetails[0]->getUid() ;?>" id="user_uid" name="user_uid" readonly>

           <div class="width100 text-center margin-top20">
                <button class="blue-button white-text clean pointer" name="submit"><?php echo _MAINJS_INDEX_SUBMIT ?></button>
            </div>
			
        </form> 

    <?php
    }
    ?>
                            
</div>

<style>
.profile-div{
background: rgba(9,197,249,1);
background: -moz-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: -webkit-gradient(left top, right bottom, color-stop(0%, rgba(9,197,249,1)), color-stop(100%, rgba(4,92,233,1)));
background: -webkit-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: -o-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: -ms-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: linear-gradient(135deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#09c5f9', endColorstr='#045ce9', GradientType=1 );
}
</style>

<?php include 'bottomButton.php'; ?>
<?php include 'js.php'; ?>

</body>
</html>