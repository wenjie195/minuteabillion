<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BidData.php';
require_once dirname(__FILE__) . '/classes/BidRecord.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<meta property="og:title" content="<?php echo _ADMIN_BID_DETAILS ?> | minuteabillion" />
<title><?php echo _ADMIN_BID_DETAILS ?> | minuteabillion</title>
<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header.php'; ?>

<div class="width100 black-bg min-height menu-distance same-padding text-center">
	<img src="img/bidding.png" class="title-icon" alt="<?php echo _ADMIN_BID_DETAILS ?>" title="<?php echo _ADMIN_BID_DETAILS ?>">
	<h1 class="title-h1 blue-text opacity-hover"  onclick="goBack()"><img src="img/back.png" alt="<?php echo _BOTTOM_BACK ?>" title="<?php echo _BOTTOM_BACK ?>" class="back-png"><?php echo _ADMIN_BID_DETAILS ?></h1>
	<div class="title-border margin-bottom30"></div>
    <div class="clear"></div>    
 
    <form method="POST"  action="utilities/adminUpdateShippingMethodFunction.php">
    <!-- <form method="POST"  action="#"> -->

		<div class="width100 overflow">

        	<!--<img src="bidItemImage/<?php echo $bidImage; ?>"class="item-png" alt="">-->
			<img src="img/coin.png" class="item-png" alt="">
            <!--<p class="item-name-p"><//?php echo $bidDetails[0]->getItemName();?></p>-->
			<p class="item-name-p">echo Item Name </p>
        </div>
        <div class="dual-input">
            <p class="input-top-p"><?php echo _PROFILE_AUCTION_ID ?></p>
           	<p class="no-input">echo aution id</p>
        </div>
        <div class="dual-input second-dual-input">
            <p class="input-top-p"><?php echo _PROFILE_STATUS ?></p>
            <p class="no-input">
                echo Status
            </p>
        </div>

        <div class="clear"></div>

        <div class="dual-input">
            <p class="input-top-p"><?php echo _ADD_BID_STARTING_DATE ?></p>
            <p class="no-input">echo starting time</p>
        </div>
        <div class="dual-input second-dual-input">
            <p class="input-top-p"><?php echo _ADMIN_BID_ENDING_TIME ?></p>
            <p class="no-input">
               echo ending time
            </p>
            <!-- <input class="input-name clean" type="text" placeholder="<?php //echo _MAINJS_INDEX_CONTACT ?>" value="<?php //echo $userData->getPhone() ;?>" id="update_contact" name="update_contact" required> -->
        </div>
		<div class="clear"></div>
		<p class="item-name-p margin-top50"><?php echo _ADMIN_WINNER_DETAILS ?></p>
            <div class="table-scroll">
            	<table class="table-css">
                	<thead>
                    	<tr>
                        	<th><?php echo _PROFILE_NO ?></th>
                            <th><?php echo _MAINJS_INDEX_USERNAME ?></th>
                            <th><?php echo _INDEX_BID_AMOUNT ?></th>
                            <th><?php echo _ADMIN_SHIPPING_METHOD ?></th>
                            <th><?php echo _ADMIN_DELIVERY_DATE ?></th>
                        </tr>
                    </thead>
                	<tbody>
						<tr>
							<td>1.</td>
							<td>echo Username</td>
							<td>echo Bid amount</td>
							<td>echo Shipping Method</td>
							<td>echo delivery date</td>
						</tr>
                    <tbody>
						<tr></tr>
						
				</table>
			</div>

    </form>



</div>

<?php include 'bottomButton.php'; ?>
<?php include 'js.php'; ?>

</body>
</html>