<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Payments.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$completedDeposit = getPayments($conn, "WHERE status = 'Completed' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<meta property="og:title" content="<?php echo _ADMIN_COMPLETED_DEPOSIT ?> | Bid Win 劲拍" />
<title><?php echo _ADMIN_COMPLETED_DEPOSIT ?> | Bid Win 劲拍</title>
<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header.php'; ?>
<div class="width100 black-bg min-height menu-distance same-padding text-center">

	<img src="img/payment.png" class="title-icon" alt="<?php echo _ADMIN_COMPLETED_DEPOSIT ?>" title="<?php echo _ADMIN_COMPLETED_DEPOSIT ?>">
	<h1 class="title-h1 blue-text"><?php echo _ADMIN_COMPLETED_DEPOSIT3 ?></h1>
	<div class="title-border margin-bottom30"></div>
	<div class="clear"></div>
              <p class="link-p"><a href="depositRequest.php" class="white-to-blue-link"><?php echo _ADMIN_REQUEST ?></a> | <?php echo _ADMIN_COMPLETED ?></p>
                
            <div class="table-scroll margin-top20 small-table">
            	<table class="table-css">
                	<thead>
                    	<tr>
                        	<th><?php echo _PROFILE_NO ?></th>
                            <th><?php echo _ADMIN_REQUEST_DATE ?></th>
                            <th><?php echo _ADMIN_MEMBER ?></th>
                            <th><?php echo _DEPOSIT_AMOUNT ?> (<?php echo _PROFILE_CREDITS ?>)</th>
                            <th><?php echo _ADMIN_PAYMENT_REFERENCE ?> (ID)</th>
                            <th><?php echo _ADMIN_PAYMENT_REFERENCE ?> (KEY)</th>
                            <th><?php echo _PROFILE_STATUS ?></th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php
                        if($completedDeposit)
                        {
                            for($cnt = 0;$cnt < count($completedDeposit) ;$cnt++)
                            {
                            ?>    
                                <tr>
                                    <td><?php echo ($cnt+1)?></td>
                                    <td><?php echo $completedDeposit[$cnt]->getDateCreated();?></td>
                                    <td><?php echo $completedDeposit[$cnt]->getUsername();?></td>
                                    <td><?php echo $completedDeposit[$cnt]->getEnteredAmount();?></td>  
                                    
                                    <td>
                                        <?php $xy=explode("&",$completedDeposit[$cnt]->getGatewayId());
                                        $xx= explode("=",$xy[0]);
                                        echo $xx[1];?>
                                    </td> 
                                    <td>
                                        <?php $xy=explode("&",$completedDeposit[$cnt]->getGatewayId());
                                        $xx= explode("=",$xy[1]);
                                        echo $xx[1];?>
                                    </td>

                                    <td><img src="img/tick.png" class="action-png" alt="<?php echo _ADMIN_APPROVE ?>" title="<?php echo _ADMIN_APPROVE ?>"></td>
                                </tr>
                            <?php
                            }
                        }
                        ?> 
                    </tbody>

                	<!-- <tbody>
                    	<tr>
                        	<td>1.</td>
                            <td>06/06/2020 10:00</td>
                            <td>Alicia</td>
                            <td>2500</td>
                            <td>JXGAHSJHSJ</td>
                            <td><img src="img/tick.png" class="action-png" alt="<?php echo _ADMIN_APPROVE ?>" title="<?php echo _ADMIN_APPROVE ?>"></td>
                        </tr>                           
                    </tbody> -->
                </table>
            
            </div>              
    	</div>
    
</div>
<style>
.topup-div{
background: rgba(9,197,249,1);
background: -moz-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: -webkit-gradient(left top, right bottom, color-stop(0%, rgba(9,197,249,1)), color-stop(100%, rgba(4,92,233,1)));
background: -webkit-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: -o-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: -ms-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: linear-gradient(135deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#09c5f9', endColorstr='#045ce9', GradientType=1 );	
}

</style>


<?php include 'bottomButton.php'; ?>
<?php include 'js.php'; ?>


<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Profile Updated !!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "fail to update profile !!"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }

    if($_SESSION['messageType'] == 2)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Password Updated !!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "fail to update password !!"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "New Password does not match with Retype Password !!";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "password length must be more that 5"; 
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "Wrong Password !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>