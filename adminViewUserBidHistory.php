<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BidRecord.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

// $uid = $_SESSION['uid'];

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<meta property="og:title" content="<?php echo _PROFILE_BIDDING_HISTORY ?> | Bid Win 劲拍" />
<title><?php echo _PROFILE_BIDDING_HISTORY ?> | Bid Win 劲拍</title>
<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header.php'; ?>

<div class="width100 black-bg min-height menu-distance same-padding text-center">

    <img src="img/bidding.png" class="title-icon" alt="<?php echo _PROFILE_BIDDING_HISTORY ?>" title="<?php echo _PROFILE_BIDDING_HISTORY ?>">
    <h1 class="title-h1 blue-text"><?php echo _PROFILE_BIDDING_HISTORY3 ?></h1>
    <div class="title-border"></div>
    <div class="clear"></div> 
    
    <?php
    if(isset($_POST['user_uid']))
    {
    $conn = connDB();
    $bidDetails = getBidRecord($conn,"WHERE user_uid = ? ", array("user_uid") ,array($_POST['user_uid']),"s");
    ?>
        <div class="table-scroll margin-top30">
            <table class="table-css">
                <thead>
                    <tr>
                        <th><?php echo _PROFILE_NO ?></th>
                        <th><?php echo _PROFILE_ITEM ?></th>
                        <th><?php echo _DEPOSIT_AMOUNT ?></th>
                        <th><?php echo _PROFILE_STATUS ?></th>
                        <th><?php echo _MAINJS_INDEX_DATE ?></th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    if($bidDetails)
                    {
                        for($cnt = 0;$cnt < count($bidDetails) ;$cnt++)
                        {
                        ?>    
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $bidDetails[$cnt]->getItemName();?></td>
                                <td><?php echo $bidDetails[$cnt]->getAmount();?></td>
                                <td>
                                    <?php 
                                        $currentStatus = $bidDetails[$cnt]->getStatus();
                                        if($currentStatus == '')
                                        {
                                            echo "Bidding";
                                        }
                                        elseif($currentStatus == 'Refund')
                                        {
                                            echo "Lose";
                                        }
                                        else
                                        {
                                            echo $currentStatus;
                                        }
                                    ?>
                                </td>
                                <td><?php echo $bidDetails[$cnt]->getDateCreated();?></td>
                            </tr>
                        <?php
                        }
                    }
                    ?> 
                </tbody>

            </table>
        </div>  
    <?php
    }
    ?>
                            
</div>

<style>
.profile-div{
background: rgba(9,197,249,1);
background: -moz-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: -webkit-gradient(left top, right bottom, color-stop(0%, rgba(9,197,249,1)), color-stop(100%, rgba(4,92,233,1)));
background: -webkit-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: -o-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: -ms-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: linear-gradient(135deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#09c5f9', endColorstr='#045ce9', GradientType=1 );
}
</style>

<?php include 'bottomButton.php'; ?>
<?php include 'js.php'; ?>

</body>
</html>