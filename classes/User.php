<?php
class User {
    /* Member variables */
    var $id,$uid,$username,$email,$password,$salt,$nickname,$phone,$address,$postcode,$city,$country,$usdtWallet,$usdtCredit,$btcWallet,$btcCredit,$totalUsdtBid,$totalBtcBid,
            $totalWin,$image,$loginType,$userType,$dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $uid
     */
    public function setId($id)
    {
        $this->id = $id;
    }
            
    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @param mixed $salt
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    }

    /**
     * @return mixed
     */
    public function getNickname()
    {
        return $this->nickname;
    }

    /**
     * @param mixed $nickname
     */
    public function setNickname($nickname)
    {
        $this->nickname = $nickname;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * @param mixed $postcode
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getUsdtWallet()
    {
        return $this->usdtWallet;
    }

    /**
     * @param mixed $usdtWallet
     */
    public function setUsdtWallet($usdtWallet)
    {
        $this->usdtWallet = $usdtWallet;
    }

    /**
     * @return mixed
     */
    public function getUsdtCredit()
    {
        return $this->usdtCredit;
    }

    /**
     * @param mixed $usdtCredit
     */
    public function setUsdtCredit($usdtCredit)
    {
        $this->usdtCredit = $usdtCredit;
    }

    /**
     * @return mixed
     */
    public function getBtcWallet()
    {
        return $this->btcWallet;
    }

    /**
     * @param mixed $btcWallet
     */
    public function setBtcWallet($btcWallet)
    {
        $this->btcWallet = $btcWallet;
    }

    /**
     * @return mixed
     */
    public function getBtcCredit()
    {
        return $this->btcCredit;
    }

    /**
     * @param mixed $btcCredit
     */
    public function setBtcCredit($btcCredit)
    {
        $this->btcCredit = $btcCredit;
    }

    /**
     * @return mixed
     */
    public function getTotalUsdtBid()
    {
        return $this->totalUsdtBid;
    }

    /**
     * @param mixed $totalUsdtBid
     */
    public function setTotalUsdtBid($totalUsdtBid)
    {
        $this->totalUsdtBid = $totalUsdtBid;
    }

    /**
     * @return mixed
     */
    public function getTotalBtcBid()
    {
        return $this->getTotalBtcBid;
    }

    /**
     * @param mixed $getTotalBtcBid
     */
    public function setTotalBtcBid($getTotalBtcBid)
    {
        $this->getTotalBtcBid = $getTotalBtcBid;
    }

    /**
     * @return mixed
     */
    public function getTotalWin()
    {
        return $this->totalWin;
    }

    /**
     * @param mixed $totalWin
     */
    public function setTotalWin($totalWin)
    {
        $this->totalWin = $totalWin;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return mixed
     */
    public function getLoginType()
    {
        return $this->loginType;
    }

    /**
     * @param mixed $loginType
     */
    public function setLoginType($loginType)
    {
        $this->loginType = $loginType;
    }

    /**
     * @return mixed
     */
    public function getUserType()
    {
        return $this->userType;
    }

    /**
     * @param mixed $userType
     */
    public function setUserType($userType)
    {
        $this->userType = $userType;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getUser($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","uid","username","email","password","salt","nickname","phone","address","postcode","city","country","usdt_wallet","usdt_credit",
                                "btc_wallet","btc_credit","total_usdt_bid","total_btc_bid","total_win","image","login_type","user_type","date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"user");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$uid,$username,$email,$password,$salt,$nickname,$phone,$address,$postcode,$city,$country,$usdtWallet,$usdtCredit,$btcWallet,$btcCredit,
                                $totalUsdtBid,$totalBtcBid,$totalWin,$image,$loginType,$userType,$dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $user = new User;
            $user->setId($id);
            $user->setUid($uid);
            $user->setUsername($username);
            $user->setEmail($email);
            $user->setPassword($password);
            $user->setSalt($salt);
            $user->setNickname($nickname);
            $user->setPhone($phone);
            $user->setAddress($address);
            $user->setPostcode($postcode);
            $user->setCity($city);
            $user->setCountry($country);
            $user->setUsdtWallet($usdtWallet);
            $user->setUsdtCredit($usdtCredit);
            $user->setBtcWallet($btcWallet);
            $user->setBtcCredit($btcCredit);

            $user->setTotalUsdtBid($totalUsdtBid);
            $user->setTotalBtcBid($totalBtcBid);
            $user->setTotalWin($totalWin);

            $user->setImage($image);
            $user->setLoginType($loginType);
            $user->setUserType($userType);
            $user->setDateCreated($dateCreated);
            $user->setDateUpdated($dateUpdated);

            array_push($resultRows,$user);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}
