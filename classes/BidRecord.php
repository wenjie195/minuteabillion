<?php
class BidRecord {
    /* Member variables */
    var $id,$tradeUid,$uid,$userUid,$username,$currentCredit,$amount,$itemName,$betType,$startRate,$endRate,$timeline,$result,$resultEdited,$status,$statusEdited,
            $shippingStatus,$shippingMethod,$shippingTimeline,$editBy,$dateCreated,$dateUpdated;
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTradeUid()
    {
        return $this->tradeUid;
    }

    /**
     * @param mixed $tradeUid
     */
    public function setTradeUid($tradeUid)
    {
        $this->tradeUid = $tradeUid;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getUserUid()
    {
        return $this->userUid;
    }

    /**
     * @param mixed $userUid
     */
    public function setUserUid($userUid)
    {
        $this->userUid = $userUid;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getCurrentCredit()
    {
        return $this->currentCredit;
    }

    /**
     * @param mixed $currentCredit
     */
    public function setCurrentCredit($currentCredit)
    {
        $this->currentCredit = $currentCredit;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getItemName()
    {
        return $this->itemName;
    }

    /**
     * @param mixed $itemName
     */
    public function setItemName($itemName)
    {
        $this->itemName = $itemName;
    }

    /**
     * @return mixed
     */
    public function getBetType()
    {
        return $this->betType;
    }

    /**
     * @param mixed $betType
     */
    public function setBetType($betType)
    {
        $this->betType = $betType;
    }

    /**
     * @return mixed
     */
    public function getStartRate()
    {
        return $this->startRate;
    }

    /**
     * @param mixed $startRate
     */
    public function setStartRate($startRate)
    {
        $this->startRate = $startRate;
    }

    /**
     * @return mixed
     */
    public function getEndRate()
    {
        return $this->endRate;
    }

    /**
     * @param mixed $endRate
     */
    public function setEndRate($endRate)
    {
        $this->endRate = $endRate;
    }

    /**
     * @return mixed
     */
    public function getTimeline()
    {
        return $this->timeline;
    }

    /**
     * @param mixed $timeline
     */
    public function setTimeline($timeline)
    {
        $this->timeline = $timeline;
    }

    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param mixed $betType
     */
    public function setResult($result)
    {
        $this->result = $result;
    }

    /**
     * @return mixed
     */
    public function getResultEdited()
    {
        return $this->resultEdited;
    }

    /**
     * @param mixed $resultEdited
     */
    public function setResultEdited($resultEdited)
    {
        $this->resultEdited = $resultEdited;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getStatusEdited()
    {
        return $this->statusEdited;
    }

    /**
     * @param mixed $statusEdited
     */
    public function setStatusEdited($statusEdited)
    {
        $this->statusEdited = $statusEdited;
    }

    /**
     * @return mixed
     */
    public function getShippingStatus()
    {
        return $this->shippingStatus;
    }

    /**
     * @param mixed $shippingStatus
     */
    public function setShippingStatus($shippingStatus)
    {
        $this->shippingStatus = $shippingStatus;
    }

    /**
     * @return mixed
     */
    public function getShippingMethod()
    {
        return $this->shippingMethod;
    }

    /**
     * @param mixed $shippingMethod
     */
    public function setShippingMethod($shippingMethod)
    {
        $this->shippingMethod = $shippingMethod;
    }

    /**
     * @return mixed
     */
    public function getShippingTimeline()
    {
        return $this->shippingTimeline;
    }

    /**
     * @param mixed $shippingTimeline
     */
    public function setShippingTimeline($shippingTimeline)
    {
        $this->shippingTimeline = $shippingTimeline;
    }

    /**
     * @return mixed
     */
    public function getEditBy()
    {
        return $this->editBy;
    }

    /**
     * @param mixed $editBy
     */
    public function setEditBy($editBy)
    {
        $this->editBy = $editBy;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getBidRecord($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","trade_uid","uid","user_uid","username","current_credit","amount","item_name","bet_type","start_rate","end_rate","timeline","result",
                                "result_edited","status","status_edited","shipping_details","shipping_method","shipping_timeline","edit_by","date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"bid_record");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$tradeUid,$uid,$userUid,$username,$currentCredit,$amount,$itemName,$betType,$startRate,$endRate,$timeline,$result,$resultEdited,$status,
                                $statusEdited,$shippingStatus,$shippingMethod,$shippingTimeline,$editBy,$dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new BidRecord;
            $class->setId($id);
            $class->setTradeUid($tradeUid);
            $class->setUid($uid);
            $class->setUserUid($userUid);
            $class->setUsername($username);
            $class->setCurrentCredit($currentCredit);
            $class->setAmount($amount);
            $class->setItemName($itemName);
            $class->setBetType($betType);
            $class->setStartRate($startRate);
            $class->setEndRate($endRate);
            $class->setTimeline($timeline);
            $class->setResult($result);
            $class->setResultEdited($resultEdited);
            $class->setStatus($status);
            $class->setStatusEdited($statusEdited);

            $class->setShippingStatus($shippingStatus);
            $class->setShippingMethod($shippingMethod);
            $class->setShippingTimeline($shippingTimeline);

            $class->setEditBy($editBy);
            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}
