<?php
class BidData {
    /* Member variables */
    var $id,$uid,$bidId,$bidName,$startingTime,$duration,$endingTime,$startTime,$finishTime,$image,$totalBid,$status,$type,$dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getBidId()
    {
        return $this->bidId;
    }

    /**
     * @param mixed $bidId
     */
    public function setBidId($bidId)
    {
        $this->bidId = $bidId;
    }

    /**
     * @return mixed
     */
    public function getBidName()
    {
        return $this->bidName;
    }

    /**
     * @param mixed $bidName
     */
    public function setBidName($bidName)
    {
        $this->bidName = $bidName;
    }

    /**
     * @return mixed
     */
    public function getStartingTime()
    {
        return $this->startingTime;
    }

    /**
     * @param mixed $startingTime
     */
    public function setStartingTime($startingTime)
    {
        $this->startingTime = $startingTime;
    }

    /**
     * @return mixed
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param mixed $duration
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
    }

    /**
     * @return mixed
     */
    public function getEndingTime()
    {
        return $this->endingTime;
    }

    /**
     * @param mixed $endingTime
     */
    public function setEndingTime($endingTime)
    {
        $this->endingTime = $endingTime;
    }

    /**
     * @return mixed
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * @param mixed $startTime
     */
    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;
    }

    /**
     * @return mixed
     */
    public function getFinishTime()
    {
        return $this->finishTime;
    }

    /**
     * @param mixed $finishTime
     */
    public function setFinishTime($finishTime)
    {
        $this->finishTime = $finishTime;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return mixed
     */
    public function getTotalBid()
    {
        return $this->totalBid;
    }

    /**
     * @param mixed $totalBid
     */
    public function setTotalBid($totalBid)
    {
        $this->totalBid = $totalBid;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getBidData($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","uid","bid_id","bid_name","starting_time","duration","ending_time","start_time","finish_time","image","total_bid","status","type",
                            "date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"bid_data");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */

        $stmt->bind_result($id,$uid,$bidId,$bidName,$startingTime,$duration,$endingTime,$startTime,$finishTime,$image,$totalBid,$status,$type,$dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new BidData;
            $class->setId($id);
            $class->setUid($uid);
            $class->setBidId($bidId);
            $class->setBidName($bidName);

            $class->setStartingTime($startingTime);
            $class->setDuration($duration);
            $class->setEndingTime($endingTime);

            $class->setStartTime($startTime);
            $class->setFinishTime($finishTime);

            $class->setImage($image);
            $class->setTotalBid($totalBid);
            $class->setStatus($status);

            $class->setType($type);
            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);

            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}
