<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BidData.php';
require_once dirname(__FILE__) . '/classes/BidRecord.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

// $soundfile = "noti.mp3";

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$bidAmount = getBidRecord($conn,"WHERE user_uid = ? ", array("user_uid") ,array($uid),"s");

$winningBid = getBidRecord($conn,"WHERE user_uid = ? AND start_rate = 'Start' ", array("user_uid") ,array($uid),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<meta property="og:title" content="<?php echo _PROFILE ?> | Bid Win 劲拍" />
<title><?php echo _PROFILE ?> | Bid Win 劲拍</title>
<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header.php'; ?>
<div class="width100 black-bg min-height menu-distance same-padding text-center">

    <?php
    if($winningBid != "") 
    {
        ?>
        <div id="win-modal" class="win-modal-div modal-css">
            <div class="modal-content-css contact-modal-content text-center win-modal-css">
            
                <div class="clear"></div>
                <h1 class="title-h1 blue-text"><?php echo _PROFILE_CONGRATULATIONS ?></h1>
                <!-- <div class="title-border margin-bottom30"></div> -->

                <!-- <div class="clear"></div>	    -->

                <p class="win-p"><?php echo _PROFILE_YOU_HAVE_WIN ?></p>
				<div class="background">
					  <span></span>
					  <span></span>
					  <span></span>
					  <span></span>
					  <span></span>
					  <span></span>
					  <span></span>
					  <span></span>
					  <span></span>
					  <span></span>
					  <span></span>
					  <span></span>
					  <span></span>
					  <span></span>
					  <span></span>
					  <span></span>
					  <span></span>
					  <span></span>
					  <span></span>
					  <span></span>
	                <img src="img/congrats.png"  data-wow-iteration="infinite" data-wow-duration="2s" class="span3 wow pulse coin-png animated animated congrats" style="visibility: visible; animation-duration: 2s; animation-iteration-count: infinite; animation-name: pulse;">
				</div>
                <!-- <div class="clear"></div>	 -->

                <a href="userWinRemindPage.php" target="_blank">
                    <div class="blue-button white-text clean pointer close-win margin-bottom50" ><?php echo _PROFILE_VIEW_DETAILS ?></div>
                </a>

            </div>					
        </div>



        <?php
    }
    else
    {   }
    ?>

    <div class="clear"></div>

	<img src="img/profile-page.png" class="title-icon" alt="<?php echo _PROFILE ?>" title="<?php echo _PROFILE ?>">
	<h1 class="title-h1 blue-text"><?php echo _PROFILE ?></h1>
	<div class="title-border margin-bottom30"></div>
	<div class="clear"></div>

        <!-- <div class="profile-pic-div">
            <a href="updateProfilePic.php" class=" hover1">
                <img src="img/default-pic.jpg" class="profile-pic2 hover1a pointer" alt="<?php echo $userData->getUsername();?>" title="<?php echo $userData->getUsername();?>">
                <img src="img/update.png" class="profile-pic2 update-pic hover1b pointer" alt="<?php echo _PROFILE_UPDATE ?>" title="<?php echo _PROFILE_UPDATE ?>">
            </a>
        </div> -->

        <div class="profile-pic-div">
        	<a href="updateProfilePic.php" class=" hover1">
                <?php 
                    $proPic = $userData->getImage();
                    if($proPic == "")
                    {
                    ?>
                        <img src="img/default-pic.jpg" class="profile-pic2 hover1a pointer" alt="<?php echo $userData->getUsername();?>" title="<?php echo $userData->getUsername();?>">
                        <img src="img/update.png" class="profile-pic2 update-pic hover1b pointer" alt="<?php echo _PROFILE_UPDATE ?>" title="<?php echo _PROFILE_UPDATE ?>">
                    <?php
                    }
                    else
                    {
                    ?>
                        <img src="upload/<?php echo $proPic?>" class="profile-pic2 hover1a pointer" alt="Profile Picture" title="Profile Picture">
                        <img src="img/update.png" class="profile-pic2 update-pic hover1b pointer" alt="<?php echo _PROFILE_UPDATE ?>" title="<?php echo _PROFILE_UPDATE ?>">
                    <?php
                    }
                ?>
            </a>
        </div>
    
    
 	<p class="gold-text rank-name opacity-hover"><a href="editProfile.php"><?php echo $userData->getUsername();?> <img src="img/edit.png" class="crown"  alt="<?php echo _PROFILE_EDIT ?>" title="<?php echo _PROFILE_EDIT ?>"></a></p>
    <p class="white-text"><?php echo $userData->getBtcCredit();?> <?php echo _PROFILE_CREDITS ?></p> 
    <div class="clear"></div>
    <a href="depositFund.php" ><div class="blue-button margin-bottom20"><?php echo _PROFILE_DEPOSIT_FUND ?></div></a>
    <a href="depositFundHistory.php" class="white-to-blue-link"><?php echo _PROFILE_DEPOSIT_HISTORY ?></a>
     	<div class="width100 overflow text-center section-padding">
        	<img src="img/bidding.png" class="title-icon box-icon" alt="<?php echo _PROFILE_BIDDING_HISTORY ?>" title="<?php echo _PROFILE_BIDDING_HISTORY ?>">
            <h1 class="title-h1 blue-text"><?php echo _PROFILE_BIDDING_HISTORY ?></h1>
            <div class="title-border margin-bottom30"></div>
            <div class="clear"></div>  
            <div class="table-scroll">
            	<table class="table-css">
                	<thead>
                    	<tr>
                        	<th><?php echo _PROFILE_NO ?></th>
                            <th><?php echo _PROFILE_ITEM ?></th>
                            <th><?php echo _PROFILE_BIDDING_END_TIME ?></th>
                            <th><?php echo _PROFILE_STATUS ?></th>
							<th><?php echo _ADMIN_DETAILS ?></th>
                        </tr>
                    </thead>
                	<tbody>
                        <?php
                        if($bidAmount)
                        {
                            for($cnt = 0;$cnt < count($bidAmount) ;$cnt++)
                            {
                            ?>
                                <tr>
                                    <td><?php echo ($cnt+1)?></td>
                                    <td><?php echo $bidAmount[$cnt]->getItemName();?></td>
                                    <td>
                                        <?php 
                                            $tradeUid = $bidAmount[$cnt]->getTradeUid();
                                            $conn = connDB();
                                            $bidDetails = getBidData($conn,"WHERE uid = ? ", array("uid") ,array($tradeUid),"s");
                                            echo $bidDetails[0]->getEndingTime();
                                        ?>
                                    </td>
                                    <td>
                                        <?php 
                                            $bidResult = $bidAmount[$cnt]->getStatus();
                                            if($bidResult == 'Win') 
                                            {
                                                echo $bidResult;
                                            }
                                            elseif($bidResult == 'Refund') 
                                            {
                                                echo "Lose";
                                            }
                                            else
                                            {
                                                echo "Winner Selection";
                                            }
                                        ?>
                                    </td>
									<td>
                                        <form method="POST" action="userBidDetails.php">
                                            <button class="clean blue-button small-btn" type="submit" name="bid_uid" value="<?php echo $tradeUid;?>">
                                                <?php echo _ADMINDASH_VIEW ?>
                                            </button>
                                        </form> 									
									</td>
                                </tr> 
                            <?php
                            }
                        }
                        ?> 
                    	<!-- <tr>
                        	<td>1.</td>
                            <td>0.05 Bitcoin</td>
                            <td>J123123123</td>
                            <td>06/06/2020 10:00</td>
                            <td>J123123123</td>
                            <td>5000</td>
                            <td>Bidding</td>
                        </tr>
                    	<tr>
                        	<td>2.</td>
                            <td>0.05 Bitcoin</td>
                            <td>J123123124</td>
                            <td>06/06/2020 10:00</td>
                            <td>J123123123</td>
                            <td>5000</td>
                            <td>Bidding</td>
                        </tr>                        
                    	<tr>
                        	<td>3.</td>
                            <td>0.05 Bitcoin</td>
                            <td>J123123125</td>
                            <td>06/06/2020 10:00</td>
                            <td>J123123123</td>
                            <td>5000</td>
                            <td>Bidding</td>
                        </tr>                                                  -->
                    </tbody>
                </table>
            
            </div>              
    	</div>   
</div>

<style>
.profile-div{
background: rgba(9,197,249,1);
background: -moz-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: -webkit-gradient(left top, right bottom, color-stop(0%, rgba(9,197,249,1)), color-stop(100%, rgba(4,92,233,1)));
background: -webkit-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: -o-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: -ms-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: linear-gradient(135deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#09c5f9', endColorstr='#045ce9', GradientType=1 );
}

</style>

<?php include 'bottomButton.php'; ?>
<?php include 'js.php'; ?>


<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Profile Updated !!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "fail to update profile !!"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }

    if($_SESSION['messageType'] == 2)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Password Updated !!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "fail to update password !!"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "New Password does not match with Retype Password !!";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "password length must be more that 5"; 
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "Wrong Password !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }

    if($_SESSION['messageType'] == 3)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "insufficient credit !!"; 
        }
        // else if($_GET['type'] == 2)
        // {
        //     $messageType = "fail to update profile !!"; 
        // }
        // else if($_GET['type'] == 3)
        // {
        //     $messageType = "ERROR !!";
        // }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }

    if($_SESSION['messageType'] == 4)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "notification stop !!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "fail to stop notification !!"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }

}
?>

</body>
</html>