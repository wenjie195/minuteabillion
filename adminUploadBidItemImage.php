<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BidData.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];
$newBidUid = $_SESSION['new_bid_uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<meta property="og:title" content="<?php echo _ADMIN_UPLOAD_BID_ITEM_IMAGE ?> | Bid Win 劲拍" />
<title><?php echo _ADMIN_UPLOAD_BID_ITEM_IMAGE ?> | Bid Win 劲拍</title>

<script src="jquery.min.js"></script>
<script src="bootstrap.min.js"></script>
<script src="croppie.js"></script>
<link rel="stylesheet" href="bootstrap.min.css" />
<link rel="stylesheet" href="croppie.css" />

<?php include 'css.php'; ?>
<style>
	.profile-preview img {
    width: 200px;
    height: 200px;
	border-radius: 0%;
}
.center-input {
    margin: 15px auto;
    width: 210px;
}
</style>
</head>
<body class="body">
<?php include 'header.php'; ?>
<div class="width100 black-bg min-height menu-distance same-padding text-center">
	
    <img src="img/items.png" class="title-icon" alt="<?php echo _ADMIN_UPLOAD_BID_ITEM_IMAGE ?>" title="<?php echo _ADMIN_UPLOAD_BID_ITEM_IMAGE ?>">
	<h1 class="title-h1 blue-text"><?php echo _ADMIN_UPLOAD_BID_ITEM_IMAGE3 ?></h1>
	<div class="title-border margin-bottom30"></div>
	<div class="clear"></div>
		
		<div class="">
			<input type="file" class="center-input" name="image_one" id="image_one" accept="image/*" required>
			<br />
			<div id="uploaded_image" class="profile-preview"></div>
        </div>
        <div class="clear"></div>

</div>
	
<div class="clear"></div>

<?php include 'bottomButton.php'; ?>
<!-- <//?php include 'js.php'; ?> -->

<div id="uploadimageModal" class="modal update-profile-div" role="dialog">
	<div class="modal-dialog update-profile-second-div">
		<div class="modal-content update-profile-third-div">
			<div class="modal-header update-profile-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><?php echo _ADMIN_UPLOAD_AND_CROP ?></h4>
				<p class="small-note ow-black-text text-center"><?php echo _ADMIN_CROP_NOTE ?></p>
			</div>
			<div class="modal-body update-profile-body">
				<div id="image_demo" class="profile-pic-crop"></div>
                <div class="clear"></div>
                <div class="width100 overflow text-center">
					<button class="blue-button clean btn-success crop_image"><?php echo _ADMIN_CROP_IMAGE ?></button>
                </div>
			</div>
		</div>
	</div>
</div>
  
<script>
$(document).ready(function(){

	$image_crop = $('#image_demo').croppie({
    enableExif: true,
    viewport: {
      width:'default',
      height:'default',
      type:'square' //circle
    },
    boundary:{
      width:'default',
      height:'default'
    }
  });

	$('#image_one').on('change', function(){
		var reader = new FileReader();
		reader.onload = function (event){
			$image_crop.croppie('bind',{
				url: event.target.result
			}).then(function(){
			console.log('jQuery bind complete');
			});
		}
		reader.readAsDataURL(this.files[0]);
		$('#uploadimageModal').modal('show');
	});

	$('.crop_image').click(function(event){
		$image_crop.croppie('result',{
			type: 'canvas',
			size: 'original'
		}).then(function(response){
		$.ajax({
		url:"adminUploadBidImageFunction.php",
        type: "POST",
        data:{"image": response},
		success:function(data)
		{
			$('#uploadimageModal').modal('hide');
            $('#uploaded_image').html(data);
		}
		});
		})
	});

});
</script>
</body>
</html>