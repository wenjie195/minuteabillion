<?php
// DB SQL Connection
function connDB(){
    // Create connection
    $conn = new mysqli("localhost", "root", "","vidatech_minuteabillion");//for localhost
    // $conn = new mysqli("localhost", "minuvodk_newuser", "HQwL4BJw88XR","minuvodk_renew"); //for live server
    // $conn = new mysqli("localhost", "qlianmen_mbuser", "raSm2fWGJi1B","qlianmen_minbillion"); //for qlianmeng server
    // $conn = new mysqli("localhost", "minuvodk_tester", "dlANc00BSS3p","minuvodk_testing"); //for minuteabillion test server


    mysqli_set_charset($conn,'UTF8');//because cant show chinese characters so need to include this to show
    //for when u insert chinese characters inside, because if dont include this then it will become unknown characters in the database
    mysqli_query($conn,"SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'")or die(mysqli_error($conn));

// Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    return $conn;
}
