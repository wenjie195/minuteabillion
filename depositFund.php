<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<meta property="og:title" content="<?php echo _PROFILE_DEPOSIT_FUND ?> | Bid Win 劲拍" />
<title><?php echo _PROFILE_DEPOSIT_FUND ?> | Bid Win 劲拍</title>
<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header.php'; ?>
<div class="width100 black-bg min-height menu-distance same-padding text-center">

	<img src="img/payment.png" class="title-icon phone-icon" alt="<?php echo _PROFILE_DEPOSIT_FUND ?>" title="<?php echo _PROFILE_DEPOSIT_FUND ?>">
	<h1 class="title-h1 blue-text"><?php echo _PROFILE_DEPOSIT_FUND3 ?></h1>
	<div class="title-border margin-bottom30"></div>
	<div class="clear"></div>
                <h2 class="white-text month-text"><?php echo _DEPOSIT_SELECT_PACKAGE ?></h2> 
                <div class="clear"></div>
                <div class="width100 overflow">
					<a href="paymentForm.php" target="_blank">
                    <div class="overflow clean transparent-button three-div sub-div">
                       
                            <img src="img/crown3.png" class="middle-icon" alt="<?php echo _DEPOSIT_VIP_PACKAGE ?>" title="<?php echo _DEPOSIT_VIP_PACKAGE ?>">
                            <p class="package-title"><?php echo _DEPOSIT_VIP_PACKAGE ?></p>
                            <p class="credit-p">499</p>
                            <p class="ps-p"><?php echo _DEPOSIT_VIP1 ?><br><?php echo _DEPOSIT_VIP2 ?></p>
                        
                    </div>  
					</a>
					<a href="paymentForm.php" target="_blank">
                    <div class="overflow clean transparent-button three-div mid-three-div sub-div">
                   
                            <img src="img/crown2.png" class="middle-icon" alt="<?php echo _DEPOSIT_PREMIUM_PACKAGE ?>" title="<?php echo _DEPOSIT_PREMIUM_PACKAGE ?>">
                            <p class="package-title"><?php echo _DEPOSIT_PREMIUM_PACKAGE ?></p>
                            <p class="credit-p">999</p>
                            <p class="ps-p"><?php echo _DEPOSIT_PREMIUM1 ?><br><?php echo _DEPOSIT_PREMIUM2 ?></p>
                
                    </div>  
					</a>
					<a href="paymentForm.php" target="_blank">
                    <div class="overflow clean transparent-button three-div sub-div">
                       
                            <img src="img/crown1.png" class="middle-icon" alt="<?php echo _DEPOSIT_ELITE_PACKAGE ?>" title="<?php echo _DEPOSIT_ELITE_PACKAGE ?>">
                            <p class="package-title"><?php echo _DEPOSIT_ELITE_PACKAGE ?></p>
                            <p class="credit-p">1699</p>
                            <p class="ps-p"><?php echo _DEPOSIT_ELITE1 ?><br><?php echo _DEPOSIT_ELITE2 ?></p>
                        
                    </div> 
					</a>
                </div>
                <div class="clear"></div>
                <div class="extra-div">
                    <p class="extra-p">
                        <?php echo _DEPOSIT_TRANSACTION1 ?>
                    </p>
                    <p class="extra-p">    
                        <?php echo _DEPOSIT_TRANSACTION2 ?>
                    </p>
                    <p class="extra-p">       
                        <?php echo _DEPOSIT_TRANSACTION3 ?>
                    </p>
                </div>
				<div class="clear"></div>
				<div class="width100 text-center">
						<a href="paymentForm.php"><div class="blue-button white-text clean pointer" name="submit"><?php echo _PAYMENT_NEXT ?></div></a>
				</div>
				<div class="clear"></div>
	
	
	
    	</div>
    
</div>

<style>
.topup-div{
background: rgba(9,197,249,1);
background: -moz-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: -webkit-gradient(left top, right bottom, color-stop(0%, rgba(9,197,249,1)), color-stop(100%, rgba(4,92,233,1)));
background: -webkit-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: -o-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: -ms-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: linear-gradient(135deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#09c5f9', endColorstr='#045ce9', GradientType=1 );
}
</style>

<!-- <script type="text/javascript">
   setTimeout(function() { 
       window.location.href = "depositFundHistory.php";
   },5000);
</script> -->

<script type="text/javascript">
setTimeout(onUserInactivity, 15000)
function onUserInactivity() {
   window.location.href = "depositFundHistory.php"
}
</script>

<?php include 'bottomButton.php'; ?>
<?php include 'js.php'; ?>


<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Profile Updated !!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "fail to update profile !!"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }

    if($_SESSION['messageType'] == 2)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Password Updated !!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "fail to update password !!"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "New Password does not match with Retype Password !!";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "password length must be more that 5"; 
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "Wrong Password !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>