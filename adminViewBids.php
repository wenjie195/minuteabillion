<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BidData.php';
require_once dirname(__FILE__) . '/classes/BidRecord.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$allBids = getBidData($conn, "ORDER BY date_created DESC");
// $allBids = getBidData($conn,"WHERE user_uid = ? ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<meta property="og:title" content="All Bids | Bid Win 劲拍" />
<title>All Bids | Bid Win 劲拍</title>
<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header.php'; ?>

<div class="width100 black-bg min-height menu-distance same-padding text-center">

    <img src="img/bidding.png" class="title-icon" alt="All Bids" title="All Bids">
    <h1 class="title-h1 blue-text">Bidding History</h1>
    <div class="title-border"></div>
    <div class="clear"></div> 
    
        <div class="table-scroll margin-top30">
            <table class="table-css small-table">
                <thead>
                    <tr>
                        <th><?php echo _PROFILE_NO ?></th>
                        <th><?php echo _PROFILE_ITEM ?></th>
                        <th><?php echo _ADD_BID_STARTING_DATE ?></th>
                        <th><?php echo _ADD_BID_ENDING_DATE ?></th>
                        <th>Status</th>
                        <th>Date</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    if($allBids)
                    {
                        for($cnt = 0;$cnt < count($allBids) ;$cnt++)
                        {
                        ?>    
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $allBids[$cnt]->getBidName();?></td>
                                <td><?php echo $allBids[$cnt]->getStartingTime();?></td>
                                <td><?php echo $allBids[$cnt]->getEndingTime();?></td>
                                <td>
                                    <?php 
                                        $status = $allBids[$cnt]->getStatus();
                                        if($status == 'Running')
                                        {   
                                            echo "Bidding";
                                        }
                                        elseif($status == 'Stop')
                                        {   
                                            echo "Ended";
                                        }
                                        else
                                        {   
                                            echo "Winner Selection";
                                        }
                                    ?>
                                </td>
                                <td><?php echo $allBids[$cnt]->getDateCreated();?></td>
                            </tr>
                        <?php
                        }
                    }
                    ?> 
                </tbody>

            </table>
        </div>  
                            
</div>



<?php include 'bottomButton.php'; ?>
<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "New Bidding Added Successfully !!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "fail to add new bidding !!"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>