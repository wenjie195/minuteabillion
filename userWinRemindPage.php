<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BidData.php';
require_once dirname(__FILE__) . '/classes/BidRecord.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

// $soundfile = "noti.mp3";

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$bidAmount = getBidRecord($conn,"WHERE user_uid = ? ", array("user_uid") ,array($uid),"s");

$winningBid = getBidRecord($conn,"WHERE user_uid = ? AND start_rate = 'Start' ", array("user_uid") ,array($uid),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Winning Item | Bid Win 劲拍" />
<title>Winning Items | Bid Win 劲拍</title>
<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header.php'; ?>

<div class="width100 black-bg min-height menu-distance same-padding text-center">

    <?php
    if($winningBid != "") 
    {
        ?>

        <!-- <div id="win-modal" class="win-modal-div modal-css">
            <div class="modal-content-css contact-modal-content text-center win-modal-css"> -->
            
                <div class="clear"></div>
                <h1 class="title-h1 blue-text"><?php echo _PROFILE_CONGRATULATIONS ?></h1>
                <div class="title-border margin-bottom30"></div>

                <div class="clear"></div>	   

                <p class="win-p"><?php echo _PROFILE_YOU_HAVE_WIN ?></p>
                
                <div class="clear"></div>	

                <?php
                for($cntBB = 0;$cntBB < count($winningBid) ;$cntBB++)
                {
                ?>
                <?php $tradeUid = $winningBid[$cntBB]->getTradeUid();?>
                <?php
                    $conn = connDB();
                    $bidData = getBidData($conn,"WHERE uid = ? ", array("uid") ,array($tradeUid),"s");
                ?>
                    <div class="bidding-repeat-div">
                        <img src="bidItemImage/<?php echo $bidData[0]->getImage(); ?>"class="item-png" alt="<?php echo $bidData[0]->getBidName();?>" title="<?php echo $bidData[0]->getBidName();?>">
                        <!-- <p class="item-name-p  ow-black-text text-overflow"> -->
                        <p class="item-name-p white-text text-overflow">
                        <?php echo $bidData[0]->getBidName();?>
                        </p>
                        <div class="width100 text-center margin-top-login">
                            <form method="POST" action="utilities/stopReminderFunction.php" >
                                <button  type="submit" name="bid_uid" value="<?php echo $winningBid[$cntBB]->getUid();?>" class="blue-button white-text clean pointer close-win margin-bottom50"><?php echo _PROFILE_OKAY ?></button>
                            </form>
                        </div>
                    </div>
                <?php
                }
                ?>

            <!-- </div>					
        </div> -->



        <?php
    }
    else
    {   }
    ?>

</div>

<style>
.profile-div{
background: rgba(9,197,249,1);
background: -moz-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: -webkit-gradient(left top, right bottom, color-stop(0%, rgba(9,197,249,1)), color-stop(100%, rgba(4,92,233,1)));
background: -webkit-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: -o-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: -ms-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: linear-gradient(135deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#09c5f9', endColorstr='#045ce9', GradientType=1 );
}

</style>

<?php include 'bottomButton.php'; ?>
<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "notification stop !!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "fail to stop notification !!"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }

}
?>

</body>
</html>