<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BidData.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<meta property="og:title" content="<?php echo _ADMINDASH_ADD_NEW_BIDDING ?> | Bid Win 劲拍" />
<title><?php echo _ADMINDASH_ADD_NEW_BIDDING ?> | Bid Win 劲拍</title>
<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header.php'; ?>

<div class="width100 black-bg min-height menu-distance same-padding text-center">
	<img src="img/add.png" class="title-icon" alt="<?php echo _ADD_BID ?>" title="<?php echo _ADD_BID ?>">
    <h1 class="title-h1 blue-text"><?php echo _ADD_BID ?></h1>
    
    <div class="title-border margin-bottom30"></div>
    
    <div class="clear"></div>  

   
        <form method="POST"  action="utilities/addNewBidFunction.php">
            <div class="dual-input">
                <p class="input-top-p"><?php echo _ADD_BID_ITEM_NAME ?></p>
                <input class="input-name clean" type="text" placeholder="<?php echo _ADD_BID_ITEM_NAME ?>" id="item_name" name="item_name" required>
            </div>

            <div class="dual-input second-dual-input">
                <p class="input-top-p"><?php echo _ADD_BID_STARTING_DATE ?></p>
                <input class="input-name clean" type="datetime-local" placeholder="Starting Time" id="starting_date" name="starting_date" required>
            </div>

            <div class="clear"></div>

            <div class="dual-input">
                <p class="input-top-p"><?php echo _ADD_BID_ENDING_DATE ?></p>
                <input class="input-name clean" type="datetime-local" placeholder="Ending Time" id="ending_date" name="ending_date" required>
            </div>
			<!--
            <div class="dual-input second-dual-input">
                <p class="input-top-p">Image</p>
                <input class="input-name clean" type="text" placeholder="Image" id="image" name="image" required>
            </div>
			-->
            <div class="clear"></div>

            <div class="width100 text-center margin-top20">
                <button class="blue-button white-text clean pointer" name="submit"><?php echo _MAINJS_INDEX_SUBMIT ?></button>
            </div>
        </form> 

</div>

<?php include 'bottomButton.php'; ?>
<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "New Bidding Added Successfully !!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "starting time is over !!"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "ending time had passed !!";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "ending time is early than starting time !!";
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "starting time and ending time are the same !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>