<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BidData.php';
require_once dirname(__FILE__) . '/classes/BidRecord.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>
<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<meta property="og:title" content="<?php echo _JS_CONFIRM_BIDDING2 ?> | Bid Win 劲拍" />
<title><?php echo _JS_CONFIRM_BIDDING2 ?> | Bid Win 劲拍</title>
<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header.php'; ?>
<div class="width100 black-bg min-height100 menu-distance same-padding text-center star-bg">

    <?php
    if(isset($_POST['bid_uid']))
    {
    $conn = connDB();
    $bidDetails = getBidData($conn,"WHERE uid = ? ", array("uid") ,array($_POST['bid_uid']),"s");

    // $bidDetails = getBidRecord($conn,"WHERE trade_uid = ? ", array("trade_uid") ,array($_POST['bid_uid']),"s");

    $bidRows = getBidRecord($conn,"WHERE trade_uid = ? ", array("trade_uid") ,array($_POST['bid_uid']),"s");
    if($bidDetails)
    {   
        $totalBid = count($bidRows);
    }
    else
    {   $totalBid = 0;   }

    ?>
        <form method="POST" action="utilities/submitBidFunction.php" class="hover1">

            <img src="img/bidding.png" class="title-icon margin-top50" alt="<?php echo _JS_CONFIRM_BIDDING ?>" title="<?php echo _JS_CONFIRM_BIDDING ?>">
            <h1 class="title-h1 blue-text"><?php echo _JS_CONFIRM_BIDDING ?></h1>
            <div class="title-border margin-bottom30"></div>
            <p class="p-text"><?php echo _PROFILE_ITEM ?></p>
            <p class="p-title"><?php echo $bidDetails[0]->getBidName(); ?></p>    
            <p class="p-text"><?php echo _PROFILE_AUCTION_ID ?></p>
            <p class="p-title"><?php echo $bidDetails[0]->getBidId(); ?></p>      
            <p class="p-text"><?php echo _PROFILE_TOTAL_BIDS ?></p>
            <p class="p-title"><?php echo $totalBid; ?></p>
            <!-- <p class="p-title ow-black-text">7</p> -->

            <input class="input-name clean" type="hidden" value="<?php echo $bidDetails[0]->getBidName(); ?>" id="bid_item_name" name="bid_item_name" readonly>
            <input class="input-name clean" type="hidden" value="<?php echo $bidDetails[0]->getUid(); ?>" id="bid_item_uid" name="bid_item_uid" readonly>

            
                <input class="checkbox-budget" type="radio" name="budget" id="bid100" value="100">
                <label class="for-checkbox-budget ow-bid ow-bid1" for="bid100">
                    <span data-hover="100">100</span>
                </label>
                <input class="checkbox-budget" type="radio" name="budget" id="bid250" value="250">
                <label class="for-checkbox-budget middle-checkbox ow-bid ow-bid2" for="bid250">							
                    <span data-hover="250">250</span>
                </label>
                <input class="checkbox-budget" type="radio" name="budget" id="bid500" value="500">
                <label class="for-checkbox-budget ow-bid ow-bid3" for="bid500">							
                    <span data-hover="500">500</span>
                </label>
                <input class="checkbox-budget" type="radio" name="budget" id="bid1000" value="1000">
                <label class="for-checkbox-budget ow-bid ow-bid4" for="bid1000">							
                    <span data-hover="1000">1000</span>
                </label>
                <input class="checkbox-budget" type="radio" name="budget" id="bid1500" value="1500">
                <label class="for-checkbox-budget middle-checkbox ow-bid ow-bid5" for="bid1500">							
                    <span data-hover="1500">1500</span>
                </label>
                <input class="checkbox-budget" type="radio" name="budget" id="bid2000" value="2000">
                <label class="for-checkbox-budget ow-bid ow-bid6" for="bid2000">							
                    <span data-hover="2000">2000</span>
                </label> 
                <input class="checkbox-budget" type="radio" name="budget" id="bid3000" value="3000">
                <label class="for-checkbox-budget ow-bid ow-bid7" for="bid3000">							
                    <span data-hover="3000">3000</span>
                </label>                        
                <input class="checkbox-budget" type="radio" name="budget" id="bid4000" value="4000">
                <label class="for-checkbox-budget middle-checkbox ow-bid ow-bid8" for="bid4000">							
                    <span data-hover="4000">4000</span>
                </label>                          
                <input class="checkbox-budget" type="radio" name="budget" id="bid5000" value="5000">
                <label class="for-checkbox-budget ow-bid ow-bid9" for="bid5000">							
                    <span data-hover="5000">5000</span>
                </label>                         
     

            <button class="blue-button clean margin-bottom20" name="submit"><?php echo _MAINJS_INDEX_SUBMIT ?></button>

        </form>
 <div class="clear"></div>

    <?php
    }
    ?>

</div>

<?php include 'bottomButton.php'; ?>
<?php include 'js.php'; ?>
<script type="text/javascript" src="js/firefly.js"></script>
<script>
  $.firefly({
    color: '#3399ff',
    minPixel: 3,
    maxPixel: 8,
    total : 60,
    on: '#firefly'
});</script>
</body>
</html>