<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/BidData.php';
require_once dirname(__FILE__) . '/../classes/BidRecord.php';
require_once dirname(__FILE__) . '/../classes/BidRecordBackup.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$userUid = $_SESSION['uid'];

function registerBidRecord($conn,$uid,$userUid,$username,$itemName,$itemUid,$budget,$currentCredit)
{
     if(insertDynamicData($conn,"bid_record",array("uid","user_uid","username","item_name","trade_uid","amount","current_credit"),
          array($uid,$userUid,$username,$itemName,$itemUid,$budget,$currentCredit),"sssssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

function registerBidRecordBackup($conn,$uid,$userUid,$username,$itemName,$itemUid,$budget,$currentCredit)
{
     if(insertDynamicData($conn,"bid_record_backup",array("uid","user_uid","username","item_name","trade_uid","amount","current_credit"),
          array($uid,$userUid,$username,$itemName,$itemUid,$budget,$currentCredit),"sssssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());

     $userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($userUid),"s");
     $username = $userDetails[0]->getUsername();
     $currentCredit = $userDetails[0]->getBtcCredit();

     $previousTotalBtcBid = $userDetails[0]->getTotalBtcBid();

     $itemName = rewrite($_POST['bid_item_name']);
     $itemUid = $_POST['bid_item_uid'];
     $budget = rewrite($_POST['budget']);

     $newCredit = $currentCredit - $budget;

     $newBtcBidCredit = $previousTotalBtcBid + $budget;

     $bidDetails = getBidData($conn, "WHERE uid =?",array("uid"),array($itemUid),"s");
     $previousTotalBid = $bidDetails[0]->getTotalBid();

     $newValue = 1;

     $newTotalBid = $newValue + $previousTotalBid;

     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $username."<br>";
     // echo $currentCredit."<br>";
     // echo $previousTotalBtcBid."<br>";
     // echo $itemName."<br>";
     // echo $itemUid."<br>";
     // echo $budget."<br>";
     // echo $previousTotalBid."<br>";
     // echo $newTotalBid."<br>";

     $bidRows = getBidRecord($conn," WHERE trade_uid = '$itemUid' AND user_uid = '$userUid' ");
     $existingBidDetails = $bidRows[0];

     if (!$existingBidDetails)
     {
          if($budget > $currentCredit)
          {
               // echo "insufficient credit";
               $_SESSION['messageType'] = 3;
               header('Location: ../profile.php?type=1');
          }
          else
          {
               if(registerBidRecord($conn,$uid,$userUid,$username,$itemName,$itemUid,$budget,$currentCredit))
               {
                    if(registerBidRecordBackup($conn,$uid,$userUid,$username,$itemName,$itemUid,$budget,$currentCredit))
                    {
                         // $_SESSION['messageType'] = 3;
                         // header('Location: ../profile.php?type=1');

                         $user = getUser($conn," uid = ? ",array("uid"),array($userUid),"s");    

                         if(!$user)
                         {   
                              $tableName = array();
                              $tableValue =  array();
                              $stringType =  "";
                              //echo "save to database";
                         
                              if($newCredit)
                              {
                                   array_push($tableName,"btc_credit");
                                   array_push($tableValue,$newCredit);
                                   $stringType .=  "d";
                              }

                              if($newBtcBidCredit)
                              {
                                   array_push($tableName,"total_btc_bid");
                                   array_push($tableValue,$newBtcBidCredit);
                                   $stringType .=  "d";
                              }

                              array_push($tableValue,$userUid);
                              $stringType .=  "s";
                              $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                              if($passwordUpdated)
                              {
                                   // $_SESSION['messageType'] = 1;
                                   // header('Location: ../biddingItems.php?type=1');

                                   // if(!$bidDetails)
                                   if($bidDetails)
                                   {   
                                        $tableName = array();
                                        $tableValue =  array();
                                        $stringType =  "";
                                        //echo "save to database";
                                   
                                        if($newTotalBid)
                                        {
                                             array_push($tableName,"total_bid");
                                             array_push($tableValue,$newTotalBid);
                                             $stringType .=  "s";
                                        }          
                                        array_push($tableValue,$itemUid);
                                        $stringType .=  "s";
                                        $passwordUpdated = updateDynamicData($conn,"bid_data"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                        if($passwordUpdated)
                                        {
                                             // echo "success";
                                             $_SESSION['messageType'] = 1;
                                             header('Location: ../biddingItems.php?type=1');
                                        }
                                        else
                                        {
                                             echo "fail";
                                             // $_SESSION['messageType'] = 1;
                                             // header('Location: ../biddingItems.php?type=2');
                                        }
                                   }
                                   else
                                   {
                                        echo "unable to find bid";
                                        // $_SESSION['messageType'] = 2;
                                        // header('Location: ../biddingItems.php?type=3');
                                   }

                              }
                              else
                              {
                                   $_SESSION['messageType'] = 1;
                                   header('Location: ../biddingItems.php?type=2');
                              }
                         }
                         else
                         {
                         $_SESSION['messageType'] = 2;
                         header('Location: ../biddingItems.php?type=3');
                         }

                    }
                    else
                    {
                         $_SESSION['messageType'] = 1;
                         header('Location: ../biddingItems.php?type=4');
                    }
               }
               else
               {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../biddingItems.php?type=5');
               }
          }
     }
     else
     {
          $_SESSION['messageType'] = 1;
          header('Location: ../biddingItems.php?type=6');
     }
}
else 
{
     header('Location: ../index.php');
}
?>