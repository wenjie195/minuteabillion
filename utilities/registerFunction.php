<?php
if (session_id() == "")
{
     session_start();
}
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
// require_once dirname(__FILE__) . '/mailerFunction.php';

function registerNewUser($conn,$uid,$nickname,$username,$email,$phone,$finalPassword,$salt,$address,$postcode,$city,$country,$UsdtAdd,$BtcAdd)
{
     if(insertDynamicData($conn,"user",array("uid","nickname","username","email","phone","password","salt","address","postcode","city","country","usdt_wallet","btc_wallet"),
          array($uid,$nickname,$username,$email,$phone,$finalPassword,$salt,$address,$postcode,$city,$country,$UsdtAdd,$BtcAdd),"sssssssssssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());
     $nickname = rewrite($_POST['register_nickname']);
     $username = rewrite($_POST['register_username']);
     $email = rewrite($_POST['register_email']);
     $phone = rewrite($_POST['register_contact']);
     $register_password = rewrite($_POST['register_password']);
     $register_password_validation = strlen($register_password);
     $register_retype_password = rewrite($_POST['register_retype_password']);
     $password = hash('sha256',$register_password);
     $salt = substr(sha1(mt_rand()), 0, 100);
     $finalPassword = hash('sha256', $salt.$password);
     $address = rewrite($_POST['register_address']);
     $postcode = rewrite($_POST['register_postcode']);
     $city = rewrite($_POST['register_city']);
     $country = rewrite($_POST['register_country']);
     $UsdtAdd = rewrite($_POST['register_usdt_add']);
     $BtcAdd = rewrite($_POST['register_btc_add']);

     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $nickname."<br>";
     // echo $username."<br>";

     if($register_password == $register_retype_password)
     {
          if($register_password_validation >= 6)
          {
               $usernameRows = getUser($conn," WHERE username = ? ",array("username"),array($username),"s");
               $usernameDetails = $usernameRows[0];

               $userEmailRows = getUser($conn," WHERE email = ? ",array("email"),array($email),"s");
               $userEmailDetails = $userEmailRows[0];

               if (!$usernameDetails && !$userEmailDetails)
               {
                    if(registerNewUser($conn,$uid,$nickname,$username,$email,$phone,$finalPassword,$salt,$address,$postcode,$city,$country,$UsdtAdd,$BtcAdd))
                    {
                         $_SESSION['uid'] = $uid;
                         $_SESSION['user_type'] = 1;
                         header('Location: ../profile.php');
                    }
                    else
                    {
                         $_SESSION['messageType'] = 3;
                         header('Location: ../index.php?type=2');
                         // echo "<script>alert('fail to register !!');window.location='../index.php'</script>";   
                    }
               }
               else
               {
                    $_SESSION['messageType'] = 3;
                    header('Location: ../index.php?type=3');
                    // echo "<script>alert('username and email has been registered by others <br> please try a new one !!');window.location='../index.php'</script>";   
               }
          }
          else 
          {
               $_SESSION['messageType'] = 3;
               header('Location: ../index.php?type=4');
               // echo "<script>alert('password length must more than 5 !!');window.location='../index.php'</script>";   
          }
     }
     else 
     {
          $_SESSION['messageType'] = 3;
          header('Location: ../index.php?type=5');
          // echo "<script>alert('Password does not match with Retype-Password !!');window.location='../index.php'</script>";   
     }      
}
else 
{
     header('Location: ../index.php');
}
?>