<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $nickname = rewrite($_POST["update_nickname"]);
    $username = rewrite($_POST["update_username"]);
    $email = rewrite($_POST["update_email"]);
    $contact = rewrite($_POST["update_contact"]);
    $address = rewrite($_POST["update_address"]);
    $postcode = rewrite($_POST["update_postcode"]);
    $city = rewrite($_POST["update_city"]);
    $country = rewrite($_POST["update_country"]);
    $usdtAdd = rewrite($_POST["update_usdt_add"]);
    $btcAdd = rewrite($_POST["update_btc_add"]);

    // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $uid."<br>";
    // echo $name."<br>";

    $user = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");    

    if(!$user)
    // if($user)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";

        if($nickname)
        {
            array_push($tableName,"nickname");
            array_push($tableValue,$nickname);
            $stringType .=  "s";
        }
        if($username)
        {
            array_push($tableName,"username");
            array_push($tableValue,$username);
            $stringType .=  "s";
        }
        if($email)
        {
            array_push($tableName,"email");
            array_push($tableValue,$email);
            $stringType .=  "s";
        }
        if($contact)
        {
            array_push($tableName,"phone");
            array_push($tableValue,$contact);
            $stringType .=  "s";
        }
        if($address)
        {
            array_push($tableName,"address");
            array_push($tableValue,$address);
            $stringType .=  "s";
        }
        if($postcode)
        {
            array_push($tableName,"postcode");
            array_push($tableValue,$postcode);
            $stringType .=  "s";
        }
        if($city)
        {
            array_push($tableName,"city");
            array_push($tableValue,$city);
            $stringType .=  "s";
        }
        if($country)
        {
            array_push($tableName,"country");
            array_push($tableValue,$country);
            $stringType .=  "s";
        }
        if($usdtAdd)
        {
            array_push($tableName,"usdt_wallet");
            array_push($tableValue,$usdtAdd);
            $stringType .=  "s";
        }
        if($btcAdd)
        {
            array_push($tableName,"btc_wallet");
            array_push($tableValue,$btcAdd);
            $stringType .=  "s";
        }

        array_push($tableValue,$uid);
        $stringType .=  "s";
        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($passwordUpdated)
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../profile.php?type=1');
        }
        else
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../profile.php?type=2');
        }
    }
    else
    {
        $_SESSION['messageType'] = 1;
        header('Location: ../profile.php?type=3');
    }
}
else 
{
    header('Location: ../index.php');
}
?>