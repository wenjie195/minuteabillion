<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/BidData.php';
require_once dirname(__FILE__) . '/../classes/BidRecord.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $Arr = $_POST["arr"];
     $ArrImplode = implode(",",$Arr); //display array value
     $ArrExplode = explode(",",$ArrImplode);
   
     $biddingUid = rewrite($_POST['bid_uid']);

     $bidData = getBidData($conn," WHERE uid = ? ",array("uid"),array($biddingUid),"s");
     $bidStatus = "Stop";

     // $filter = 'Update';
     $resultStatus = "Win";
     $shippingStatus = "Pending";

     $failureBid = "Refund";

     //   FOR DEBUGGING
     // echo "<br>";
     // echo $Arr."<br>";
     // echo $ArrImplode."<br>";
     // echo $ArrExplode."<br>";
     // echo $biddingUid."<br>";

     if($Arr == '')
     { 
          $_SESSION['messageType'] = 4;
          header('Location: ../adminViewBiddingItems.php?type=1');
     }
     else
     {
          if($bidData)
          {   
               $tableName = array();
               $tableValue =  array();
               $stringType =  "";
               //echo "save to database";

               if($bidStatus)
               {
                    array_push($tableName,"status");
                    array_push($tableValue,$bidStatus);
                    $stringType .=  "s";
               }
               array_push($tableValue,$biddingUid);
               $stringType .=  "s";
               $updateUserCredit = updateDynamicData($conn,"bid_data"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
               if($updateUserCredit)
               {
                    // echo "success";

                    if ($ArrExplode)
                    {
                         for ($i=0; $i <count($ArrExplode) ; $i++)
                         {
                              $tableName = array();
                              $tableValue =  array();
                              $stringType =  "";
                              if($resultStatus)
                              {
                                   array_push($tableName,"status");
                                   array_push($tableValue,$resultStatus);
                                   $stringType .=  "s";
                              }
                              if($shippingStatus)
                              {
                                   array_push($tableName,"shipping_details");
                                   array_push($tableValue,$shippingStatus);
                                   $stringType .=  "s";
                              }

                              array_push($tableValue,$ArrExplode[$i]);
                              $stringType .=  "s";
                              $customerDetails = updateDynamicData($conn,"bid_record", " WHERE uid=? ",$tableName,$tableValue,$stringType);
                         }
                    }
                    else
                    {
                         echo "error";
                    }
                    // $customerDetails = getCustomerDetails($conn, "WHERE phone=?",array("phone"),array($ArrExplode[$i]), "s");
                    if ($customerDetails)
                    {
                         // if($bidData)
                         if($biddingUid)
                         {   
                              $tableName = array();
                              $tableValue =  array();
                              $stringType =  "";
                              //echo "save to database";
                              
                              if($failureBid)
                              {
                                   array_push($tableName,"status");
                                   array_push($tableValue,$failureBid);
                                   $stringType .=  "s";
                              }
                              array_push($tableValue,$biddingUid);
                              $stringType .=  "s";
                              $updateUserCredit = updateDynamicData($conn,"bid_record"," WHERE trade_uid = ? AND (status is NULL OR status = 'Revert') ",$tableName,$tableValue,$stringType);
                              if($updateUserCredit)
                              {
                                   if($failureBid)
                                   {
                                        $bidRecord = getBidRecord($conn," WHERE trade_uid = ? AND status = 'Refund' ",array("trade_uid"),array($biddingUid),"s");
                                        // $bidRecord = getBidRecord($conn," WHERE status = ? ",array("status"),array($failureBid),"s");
                                        // $bidRecord = getBidRecord($conn," WHERE status = 'Refund' ");
                                        // echo count($bidRecord);
                                        // count($bidRecord);
                                        for($cnt = 0;$cnt < count($bidRecord) ;$cnt++)
                                        {
                                             $userUid = $bidRecord[$cnt]->getUserUid();
                                             $amount = $bidRecord[$cnt]->getAmount();
                                             $userRecord = getUser($conn," WHERE uid = ? ",array("uid"),array($userUid),"s");
                                             for($i = 0;$i < count($userRecord) ;$i++)
                                             {
                                                  $btcRecord = $userRecord[$i]->getBtcCredit();
                                                  // $totalWin = $userRecord[$i]->getTotalWin();
                                                  $refund = $amount + $btcRecord;
                                                  // $addWin = $totalWin + 1;

                                                  $tableName = array();
                                                  $tableValue =  array();
                                                  $stringType =  "";
                                                  if($refund)
                                                  {
                                                       array_push($tableName,"btc_credit");
                                                       array_push($tableValue,$refund);
                                                       $stringType .=  "s";
                                                  }
                                                  // if($addWin)
                                                  // {
                                                  //      array_push($tableName,"total_win");
                                                  //      array_push($tableValue,$addWin);
                                                  //      $stringType .=  "s";
                                                  // }
                                                  array_push($tableValue,$userUid);
                                                  $stringType .=  "s";
                                                  $updateUserBtcCredit = updateDynamicData($conn,"user", " WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                  if($updateUserBtcCredit)
                                                  {
                                                       // header('Location: ../adminViewBiddingItems.php');


                                                       if($resultStatus)
                                                       {
                                                            $winBidRecord = getBidRecord($conn," WHERE trade_uid = ? AND status = 'Win' ",array("trade_uid"),array($biddingUid),"s");
                                                            // count($bidRecord);
                                                            for($cnt = 0;$cnt < count($winBidRecord) ;$cnt++)
                                                            {
                                                                 $winnerUid = $winBidRecord[$cnt]->getUserUid();
                                                                 $amount = $winBidRecord[$cnt]->getAmount();
                                                                 $userWinRecord = getUser($conn," WHERE uid = ? ",array("uid"),array($winnerUid),"s");
                                                                 for($i = 0;$i < count($userWinRecord) ;$i++)
                                                                 {
                                                                      $totalWin = $userWinRecord[$i]->getTotalWin();
                                                                      $addWin = $totalWin + 1;
                    
                                                                      $tableName = array();
                                                                      $tableValue =  array();
                                                                      $stringType =  "";
                                                                      if($addWin)
                                                                      {
                                                                           array_push($tableName,"total_win");
                                                                           array_push($tableValue,$addWin);
                                                                           $stringType .=  "d";
                                                                      }
                                                                      array_push($tableValue,$winnerUid);
                                                                      $stringType .=  "s";
                                                                      $updateUserTotalWin = updateDynamicData($conn,"user", " WHERE uid = ? ",$tableName,$tableValue,$stringType);
                                                                      if($updateUserTotalWin)
                                                                      {
                                                                           header('Location: ../adminViewBiddingItems.php');
                                                                      }
                                                                      else
                                                                      {
                                                                           echo "fail";
                                                                      }
                                                                 }
                                                            }
                    
                                                       }  


                                                  }
                                                  else
                                                  {
                                                       echo "fail";
                                                  }
                                             }
                                        }

                                   }                         
                              }
                              else
                              {
                                   echo "fail";
                              }
                         }
                         else
                         {
                              echo "error";
                         }
                    }
                    else
                    {
                         echo "fail";
                    }
               }
               else
               {
                    echo "fail";
               }
          }
          else
          {
               echo "error";
          }
     }
}
else 
{
     header('Location: ../index.php');
}  
?>