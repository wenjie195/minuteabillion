<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Payments.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $depositUid = rewrite($_POST['deposit_uid']);
     $depositDetails = getPayments($conn, "WHERE uid =?",array("uid"),array($depositUid),"s");

     $status = "Rejected";

     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $currentCredit."<br>";
     // echo $amount."<br>";
     // echo $newCredit."<br>";

     if(isset($_POST['deposit_uid']))
     {   
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database";
          if($status)
          {
               array_push($tableName,"status");
               array_push($tableValue,$status);
               $stringType .=  "s";
          }              
          array_push($tableValue,$depositUid);
          $stringType .=  "s";
          $orderUpdated = updateDynamicData($conn,"payments"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          
          if($orderUpdated)
          {
               // echo "success aa";
               // $_SESSION['messageType'] = 1;
               header('Location: ../depositRequest.php');
          }
          else
          {
               echo "fail aa";
          }
     }
     else
     {
          echo "dunno aa";
     }
}
else 
{
     header('Location: ../index.php');
}
?>