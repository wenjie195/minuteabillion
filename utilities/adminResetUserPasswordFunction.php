<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $userUid = rewrite($_POST["user_uid"]);

     $editPassword_new  = "123321";
     $editPassword_reenter  = "123321";

     $user = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
     $userDetails = $user[0];

     //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $editPassword_new."<br>";
     // echo $editPassword_reenter."<br>";

     $password = hash('sha256',$editPassword_new);
     $salt = substr(sha1(mt_rand()), 0, 100);
     $finalPassword = hash('sha256', $salt.$password);

     $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("password","salt"),array($finalPassword,$salt,$userUid),"sss");
     if($passwordUpdated)
     {
          $_SESSION['messageType'] = 1;
          // header('Location: ../adminDashboard.php?type=1');
          header('Location: ../adminViewMembers.php?type=1');
     }
     else 
     {
          $_SESSION['messageType'] = 1;
          // header('Location: ../adminDashboard.php?type=2');
          header('Location: ../adminViewMembers.php?type=2');
     }
}
else 
{
     header('Location: ../index.php');
}  
?>