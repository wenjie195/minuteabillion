<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/BidRecord.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $shippingMethod = rewrite($_POST["shipping_method"]);

    $timelime = rewrite($_POST["shipping_datetime"]);
    $shippingTimeline = str_replace('T', ' ', trim($timelime));

    $bidUid = rewrite($_POST["bid_uid"]);
    $shippingStatus = "Shipped";

    // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $uid."<br>";
    // echo $name."<br>";

    $bidData = getBidRecord($conn," WHERE uid = ? ",array("uid"),array($bidUid),"s");    

    if($bidData)
    // if(!$bidData)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";

        if($shippingStatus)
        {
            array_push($tableName,"shipping_details");
            array_push($tableValue,$shippingStatus);
            $stringType .=  "s";
        }
        if($shippingMethod)
        {
            array_push($tableName,"shipping_method");
            array_push($tableValue,$shippingMethod);
            $stringType .=  "s";
        }
        if($shippingTimeline)
        {
            array_push($tableName,"shipping_timeline");
            array_push($tableValue,$shippingTimeline);
            $stringType .=  "s";
        }

        array_push($tableValue,$bidUid);
        $stringType .=  "s";
        $updateShippingDetails = updateDynamicData($conn,"bid_record"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($updateShippingDetails)
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../adminShippingRequest.php?type=1');
        }
        else
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../adminShippingRequest.php?type=2');
        }
    }
    else
    {
        $_SESSION['messageType'] = 1;
        header('Location: ../adminShippingRequest.php?type=3');
    }
}
else 
{
    header('Location: ../index.php');
}
?>