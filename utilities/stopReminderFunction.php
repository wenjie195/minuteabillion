<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/User.php';
require_once dirname(__FILE__) . '/../classes/BidRecord.php';
// require_once dirname(__FILE__) . '/classes/BidRecord.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $bidUid = rewrite($_POST["bid_uid"]);
    $notification = "Stop";

    // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $uid."<br>";
    // echo $name."<br>";

    $bidValue = getBidRecord($conn,"WHERE uid = ? ", array("uid") ,array($_POST['bid_uid']),"s");

    if($bidValue)
    // if($user)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";

        if($notification)
        {
            array_push($tableName,"start_rate");
            array_push($tableValue,$notification);
            $stringType .=  "s";
        }

        array_push($tableValue,$bidUid);
        $stringType .=  "s";
        $passwordUpdated = updateDynamicData($conn,"bid_record"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($passwordUpdated)
        {
            // $_SESSION['messageType'] = 4;
            // header('Location: ../profile.php?type=1');
            // header('Location: ../profile.php');
            header('Location: ../userWinRemindPage.php');
        }
        else
        {
            // $_SESSION['messageType'] = 4;
            // header('Location: ../profile.php?type=2');
            $_SESSION['messageType'] = 1;
            header('Location: ../userWinRemindPage.php?type=2');
        }
    }
    else
    {
        // $_SESSION['messageType'] = 4;
        // header('Location: ../profile.php?type=3');
        $_SESSION['messageType'] = 1;
        header('Location: ../userWinRemindPage.php?type=2');
    }
}
else 
{
    header('Location: ../index.php');
}
?>