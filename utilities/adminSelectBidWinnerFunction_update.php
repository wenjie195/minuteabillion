<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/BidData.php';
require_once dirname(__FILE__) . '/../classes/BidRecord.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $Arr = $_POST["arr"];
     $ArrImplode = implode(",",$Arr); //display array value
     $ArrExplode = explode(",",$ArrImplode);
   
     $biddingUid = rewrite($_POST['bid_uid']);

     $bidData = getBidData($conn," WHERE uid = ? ",array("uid"),array($biddingUid),"s");
     $bidStatus = "Stop";

     // $filter = 'Update';
     $resultStatus = "Win";
     $shippingStatus = "Pending";

     $failureBid = "Refund";

     //   FOR DEBUGGING
     // echo "<br>";
     // echo $Arr."<br>";
     // echo $ArrImplode."<br>";
     // echo $ArrExplode."<br>";
     // echo $biddingUid."<br>";

     if($bidData)
     {   
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database";

          if($bidStatus)
          {
               array_push($tableName,"status");
               array_push($tableValue,$bidStatus);
               $stringType .=  "s";
          }
          array_push($tableValue,$biddingUid);
          $stringType .=  "s";
          $updateUserCredit = updateDynamicData($conn,"bid_data"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          if($updateUserCredit)
          {
               // echo "success";

               if ($ArrExplode)
               {
                    for ($i=0; $i <count($ArrExplode) ; $i++)
                    {
                         $tableName = array();
                         $tableValue =  array();
                         $stringType =  "";
                         if($resultStatus)
                         {
                              array_push($tableName,"status");
                              array_push($tableValue,$resultStatus);
                              $stringType .=  "s";
                         }
                         if($shippingStatus)
                         {
                              array_push($tableName,"shipping_details");
                              array_push($tableValue,$shippingStatus);
                              $stringType .=  "s";
                         }

                         array_push($tableValue,$ArrExplode[$i]);
                         $stringType .=  "s";
                         $customerDetails = updateDynamicData($conn,"bid_record", " WHERE uid=? ",$tableName,$tableValue,$stringType);
                         // $customerDetails = getCustomerDetails($conn, "WHERE phone=?",array("phone"),array($ArrExplode[$i]), "s");
                         if ($customerDetails)
                         {
                              // $_SESSION['messageType'] = 1;
                              // header('Location: ../adminViewBiddingItems.php?type=1');
                              $_SESSION['bids_uid'] = $biddingUid;
                              header('Location: ../adminViewBiddingItems.php');

                              // if($bidData)
                              // {   
                              //      $tableName = array();
                              //      $tableValue =  array();
                              //      $stringType =  "";
                              //      //echo "save to database";
                         
                              //      if($failureBid)
                              //      {
                              //           array_push($tableName,"status");
                              //           array_push($tableValue,$failureBid);
                              //           $stringType .=  "s";
                              //      }
                              //      array_push($tableValue,$biddingUid);
                              //      $stringType .=  "s";
                              //      $updateUserCredit = updateDynamicData($conn,"bid_record"," WHERE uid = ? AND status != 'Win' ",$tableName,$tableValue,$stringType);
                              //      if($updateUserCredit)
                              //      {
                              //           // echo "success";
                              //           header('Location: ../adminViewBiddingItems.php');                         
                              //      }
                              //      else
                              //      {
                              //           echo "fail";
                              //      }
                              // }
                              // else
                              // {
                              //      echo "error";
                              // }

                         }
                         else
                         {
                              echo "fail";
                         }
                    }
               }
               else
               {
                    echo "error";
               }


          }
          else
          {
               echo "fail";
          }
     }
     else
     {
          echo "error";
     }


     // if ($ArrExplode)
     // {
     //      for ($i=0; $i <count($ArrExplode) ; $i++)
     //      {
     //           $tableName = array();
     //           $tableValue =  array();
     //           $stringType =  "";
     //           // if($filter)
     //           // {
     //           //      array_push($tableName,"filter");
     //           //      array_push($tableValue,$filter);
     //           //      $stringType .=  "s";
     //           // }

     //           // if($resultWinnerName)
     //           // {
     //           //      array_push($tableName,"result");
     //           //      array_push($tableValue,$resultWinnerName);
     //           //      $stringType .=  "s";
     //           // }
     //           if($resultStatus)
     //           {
     //                array_push($tableName,"status");
     //                array_push($tableValue,$resultStatus);
     //                $stringType .=  "s";
     //           }

     //           array_push($tableValue,$ArrExplode[$i]);
     //           $stringType .=  "s";
     //           $customerDetails = updateDynamicData($conn,"bid_record", " WHERE uid=? ",$tableName,$tableValue,$stringType);
     //           // $customerDetails = getCustomerDetails($conn, "WHERE phone=?",array("phone"),array($ArrExplode[$i]), "s");
     //           if ($customerDetails)
     //           {
     //                echo "success";
     //           }
     //           else
     //           {
     //                echo "fail";
     //           }
     //      }
     // }
     // else
     // {
     //      echo "error";
     // }

}
else 
{
     header('Location: ../index.php');
}  
?>