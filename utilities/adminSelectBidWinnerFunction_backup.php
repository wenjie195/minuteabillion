<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/BidData.php';
require_once dirname(__FILE__) . '/../classes/BidRecord.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $bidUid = rewrite($_POST["bid_uid"]);

     $bidRows = getBidRecord($conn," WHERE uid = ? ",array("uid"),array($bidUid),"s");
     $tradeUid = $bidRows[0]->getTradeUid();
     $userUid = $bidRows[0]->getUserUid();
     $resultWinnerName = $bidRows[0]->getUsername();
     $bidAmount = $bidRows[0]->getAmount();

     $user = getUser($conn, "WHERE uid =?",array("uid"),array($userUid),"s");
     $userBtcCredit = $user[0]->getBtcCredit();

     $winnerNewCredit = $userBtcCredit + $bidAmount;

     $bidData = getBidData($conn," WHERE uid = ? ",array("uid"),array($tradeUid),"s");
     $bidStatus = "Stop";
     $resultStatus = "Win";

     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $bidUid."<br>";
     // echo $userUid."<br>";
     // echo $tradeUid."<br>";
     // echo $resultWinnerName."<br>";
     // echo $bidAmount."<br>";
     // echo $userBtcCredit."<br>";
     // echo $bidIDDD."<br>";

     if($user)
     {   
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database";

          if($winnerNewCredit)
          {
               array_push($tableName,"btc_credit");
               array_push($tableValue,$winnerNewCredit);
               $stringType .=  "s";
          }
          array_push($tableValue,$userUid);
          $stringType .=  "s";
          $updateUserCredit = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          if($updateUserCredit)
          {
               if($bidData)
               {   
                    $tableName = array();
                    $tableValue =  array();
                    $stringType =  "";
                    //echo "save to database";
          
                    if($bidStatus)
                    {
                         array_push($tableName,"status");
                         array_push($tableValue,$bidStatus);
                         $stringType .=  "s";
                    }
                    array_push($tableValue,$tradeUid);
                    $stringType .=  "s";
                    $updateUserCredit = updateDynamicData($conn,"bid_data"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                    if($updateUserCredit)
                    {
                         if($bidRows)
                         {   
                              $tableName = array();
                              $tableValue =  array();
                              $stringType =  "";
                              //echo "save to database";
                    
                              if($resultWinnerName)
                              {
                                   array_push($tableName,"result");
                                   array_push($tableValue,$resultWinnerName);
                                   $stringType .=  "s";
                              }
                              if($resultStatus)
                              {
                                   array_push($tableName,"status");
                                   array_push($tableValue,$resultStatus);
                                   $stringType .=  "s";
                              }
                              array_push($tableValue,$bidUid);
                              $stringType .=  "s";
                              $updateUserCredit = updateDynamicData($conn,"bid_record"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                              if($updateUserCredit)
                              {
                                   $_SESSION['messageType'] = 1;
                                   header('Location: ../adminViewBiddingItems.php?type=1');
                              }
                              else
                              {
                                   $_SESSION['messageType'] = 2;
                                   header('Location: ../adminViewBiddingItems.php?type=2');
                              }
                         }
                         else
                         {
                             $_SESSION['messageType'] = 2;
                             header('Location: ../adminViewBiddingItems.php?type=3');
                         }
                    }
                    else
                    {
                         $_SESSION['messageType'] = 1;
                         header('Location: ../adminViewBiddingItems.php?type=2');
                    }
               }
               else
               {
                   $_SESSION['messageType'] = 1;
                   header('Location: ../adminViewBiddingItems.php?type=3');
               }
          }
          else
          {
               $_SESSION['messageType'] = 3;
               header('Location: ../adminViewBiddingItems.php?type=2');
          }
     }
     else
     {
         $_SESSION['messageType'] = 3;
         header('Location: ../adminViewBiddingItems.php?type=3');
     }
}
else 
{
     header('Location: ../index.php');
}  
?>