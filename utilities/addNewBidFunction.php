<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/BidData.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

// function registerNewBidData($conn,$uid,$bidId,$bidName,$startingTime,$duration,$endingTime,$image,$status)
// {
//      if(insertDynamicData($conn,"bid_data",array("uid","bid_id","bid_name","starting_time","duration","ending_time","image","status"),
//           array($uid,$bidId,$bidName,$startingTime,$duration,$endingTime,$image,$status),"ssssssss") === null)
//      {
//           echo "gg";
//      }
//      else{    }
//      return true;
// }

function registerNewBidData($conn,$uid,$bidId,$bidName,$startingTime,$endingTime,$duration,$status,$startTime,$finishTime)
{
     if(insertDynamicData($conn,"bid_data",array("uid","bid_id","bid_name","starting_time","ending_time","duration","status","start_time","finish_time"),
          array($uid,$bidId,$bidName,$startingTime,$endingTime,$duration,$status,$startTime,$finishTime),"sssssssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());
     $bidId = uniqid();

     $bidName = rewrite($_POST['item_name']);

     // $startingTime = $_POST['starting_date'];
     $startTime = $_POST['starting_date'];
     $startingTime = str_replace('T', ' ', trim($startTime));

     $endTime = $_POST['ending_date'];
     $endingTime = str_replace('T', ' ', trim($endTime));

     $date1 = strtotime($startingTime);  
     $date2 = strtotime($endingTime);  

     $startTime = strtotime($startingTime);  
     $finishTime = strtotime($endingTime);  

     //close temp
     // $duration = rewrite($_POST['duration']);
     // $image = rewrite($_POST['image']);

     $status = "Running";

     // $Date1 = $startingTime;
     // $date = new DateTime($Date1);
     // $additional = $duration;
     // $date->modify($additional);
     // // $expiredDate = $date->format('Y-m-d');
     // $endingTime = $date->format('Y-m-d H:i');
     
     // $diff = abs($endingTime - $startingTime); 

     $tz = 'Asia/Kuala_Lumpur';
     $timestamp = time();
     $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
     $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
     $currentTime = $dt->format('Y-m-d H:i');

     // Formulate the Difference between two dates 
     $diff = abs($date2 - $date1);  
     $years = floor($diff / (365*60*60*24));  
     $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));  
     $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24)); 
     $hours = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24) / (60*60));  
     $minutes = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60);

     $spacing = " " ;
     $input1 = "Years";
     $input2 = "Months";
     $input3 = "Days";
     $input4 = "Hours";
     $input5 = "Minutes";
     $duration = $years.$input1.$spacing.$months.$input2.$spacing.$days.$input3.$spacing.$hours.$input4.$spacing.$minutes.$input5;

     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $bidName."<br>";
     // echo $startingTime."<br>";
     // echo $endingTime."<br>";
     // echo $diff."<br>";
     // echo $years."<br>";
     // echo $months."<br>";
     // echo $days."<br>";
     // echo $hours."<br>";
     // echo $minutes."<br>";
     // echo $duration."<br>";

     // $bidDataRows = getBidData($conn," WHERE bid_name = ? ",array("bid_name"),array($bidName),"s");
     // $existingBidData = $bidDataRows[0];

     if($startingTime < $currentTime)
     {
          // echo "late add on bid";
          $_SESSION['messageType'] = 1;
          header('Location: ../adminAddNewBidding.php?type=2');
     }
     elseif($endingTime < $currentTime)
     // if($endingTime < $currentTime)
     {
          // echo "ending time is less than current time";
          $_SESSION['messageType'] = 1;
          header('Location: ../adminAddNewBidding.php?type=3');
     }
     // elseif($startingTime < $endingTime)
     elseif($startingTime > $endingTime)
     {
          // echo "end time early than start time";
          $_SESSION['messageType'] = 1;
          header('Location: ../adminAddNewBidding.php?type=4');
     }
     elseif($startingTime == $endingTime)
     {
          // echo "start time and end time same";
          $_SESSION['messageType'] = 1;
          header('Location: ../adminAddNewBidding.php?type=5');
     }
     else
     {
          if (!$existingBidData)
          {
               if(registerNewBidData($conn,$uid,$bidId,$bidName,$startingTime,$endingTime,$duration,$status,$startTime,$finishTime))
               {     
                    $_SESSION['new_bid_uid'] = $uid;
                    header('Location: ../adminUploadBidItemImage.php');
               }
               else
               {
                    $_SESSION['messageType'] = 1;
                    header('Location: ../adminViewBids.php?type=2');
               }
          }
          else
          {
               $_SESSION['messageType'] = 1;
               header('Location: ../adminViewBids.php?type=3'); 
          }  
     }

     // if (!$existingBidData)
     // {
     //      if(registerNewBidData($conn,$uid,$bidId,$bidName,$startingTime,$endingTime,$duration,$status))
     //      {
     //           // $_SESSION['messageType'] = 1;
     //           // header('Location: ../adminViewBids.php?type=1');

     //           $_SESSION['new_bid_uid'] = $uid;
     //           header('Location: ../adminUploadBidItemImage.php');
     //      }
     //      else
     //      {
     //           $_SESSION['messageType'] = 1;
     //           header('Location: ../adminViewBids.php?type=2');
     //      }
     // }
     // else
     // {
     //      $_SESSION['messageType'] = 1;
     //      header('Location: ../adminViewBids.php?type=3'); 
     // }  
}
else 
{
     header('Location: ../index.php');
}
?>