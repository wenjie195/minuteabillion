<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/BidData.php';
require_once dirname(__FILE__) . '/../classes/BidRecord.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();
   
     $biddingUid = rewrite($_POST['bid_uid']);

     $bidDetails = getBidRecord($conn, "WHERE trade_uid =?",array("trade_uid"),array($biddingUid),"s");
     $userUid = $bidDetails[0]->getUserUid(); 
     $bidAmount = $bidDetails[0]->getAmount(); 
     $tradingUid = $bidDetails[0]->getUid(); 

     $userRows = getUser($conn, "WHERE uid =?",array("uid"),array($userUid),"s");
     $currentCredit = $userRows[0]->getBtcCredit(); 

     $refundCredit = $currentCredit + $bidAmount;

     $bidData = getBidData($conn," WHERE uid = ? ",array("uid"),array($biddingUid),"s");
     $bidStatus = "Bid Cancelled";
     $result = "Bid Cancelled";

     //   FOR DEBUGGING
     // echo "<br>";
     // echo $Arr."<br>";
     // echo $ArrImplode."<br>";
     // echo $ArrExplode."<br>";
     // echo $biddingUid."<br>";

     if($bidData)
     {   
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database";

          if($bidStatus)
          {
               array_push($tableName,"status");
               array_push($tableValue,$bidStatus);
               $stringType .=  "s";
          }
          array_push($tableValue,$biddingUid);
          $stringType .=  "s";
          $updateBidData = updateDynamicData($conn,"bid_data"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          if($updateBidData)
          {

               if($userRows)
               {   
                    $tableName = array();
                    $tableValue =  array();
                    $stringType =  "";
                    //echo "save to database";
          
                    if($refundCredit)
                    {
                         array_push($tableName,"btc_credit");
                         array_push($tableValue,$refundCredit);
                         $stringType .=  "s";
                    }
                    array_push($tableValue,$userUid);
                    $stringType .=  "s";
                    $updateUserCredit = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                    if($updateUserCredit)
                    {
          
                         if($bidDetails)
                         {   
                              $tableName = array();
                              $tableValue =  array();
                              $stringType =  "";
                              //echo "save to database";
                    
                              if($result)
                              {
                                   array_push($tableName,"result");
                                   array_push($tableValue,$result);
                                   $stringType .=  "s";
                              }
                              array_push($tableValue,$tradingUid);
                              $stringType .=  "s";
                              $updateUserCredit = updateDynamicData($conn,"bid_record"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                              if($updateUserCredit)
                              {
                                   header('Location: ../adminAllBid.php');
                              }
                              else
                              {
                                   echo "fail";
                              }
                         }
                         else
                         {
                              echo "error";
                         }

                    }
                    else
                    {
                         echo "fail";
                    }
               }
               else
               {
                    echo "error";
               }

          }
          else
          {
               echo "fail";
          }
     }
     else
     {
          echo "error";
     }
}
else 
{
     header('Location: ../index.php');
}  
?>