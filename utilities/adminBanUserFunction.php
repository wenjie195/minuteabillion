<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

// $uid = $_SESSION['uid'];

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();


    $status = rewrite($_POST["update_status"]);

    $userUid = rewrite($_POST["user_uid"]);

    // //   FOR DEBUGGING 
    // echo "<br>";
    // echo $uid."<br>";
    // echo $name."<br>";

    $user = getUser($conn," WHERE uid = ? ",array("uid"),array($userUid),"s");    

    // if(!$user)
    if($user)
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";

        if($status)
        {
            array_push($tableName,"login_type");
            array_push($tableValue,$status);
            $stringType .=  "i";
        }

        array_push($tableValue,$userUid);
        $stringType .=  "s";
        $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($passwordUpdated)
        {
            // $_SESSION['messageType'] = 2;
            // header('Location: ../adminViewMembers.php?type=1');
            if($status == '1')
            {
                header('Location: ../adminViewMembers.php');
            }
            else
            {
                $_SESSION['messageType'] = 2;
                header('Location: ../adminViewMembers.php?type=1');
            }
        }
        else
        {
            $_SESSION['messageType'] = 2;
            header('Location: ../adminViewMembers.php?type=2');
        }
    }
    else
    {
        $_SESSION['messageType'] = 2;
        header('Location: ../adminViewMembers.php?type=3');
    }
}
else 
{
    header('Location: ../index.php');
}
?>