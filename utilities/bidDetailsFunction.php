<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/BidData.php';
require_once dirname(__FILE__) . '/../classes/BidRecord.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/languageFunction.php';

$conn = connDB();

$bid_uid = rewrite($_POST['bid_uid']);
$bidDetails = getBidRecord($conn, "WHERE trade_uid =?",array("trade_uid"),array($bid_uid),"s");

if($bidDetails)
{
    for($cnt = 0;$cnt < count($bidDetails) ;$cnt++)
    {
        if($bidDetails[$cnt]->getStatus()== 'Refund')
        {
        $bidDetails[$cnt]->getStatus()."<br>";
        $amount[] = $bidDetails[$cnt]->getAmount();
        $AmountImplode = implode(",",$amount); //display array value
        $AmountExplode = explode(",",$AmountImplode);
        $userUid[] = $bidDetails[$cnt]->getUserUid();
        $UidImplode = implode(",",$userUid); //display array value
        $UidExplode = explode(",",$UidImplode);
        }
    }
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $bid_uid = rewrite($_POST['bid_uid']);
    $bidDetails = getBidRecord($conn, "WHERE trade_uid =?",array("trade_uid"),array($bid_uid),"s");
    // $status = "NULL";
    // $shipping_details = "NULL";
    // $bidding_status = "Running";

    // //for debugging
    // echo "<br>";
    // echo $AmountImplode."<br>";
    // echo $UidImplode."<br>";
    // echo $userUid."<br>";
    // echo $bid_uid."<br>";
    // echo count($UidExplode);
    // echo count($AmountExplode);
    // echo $status."<br>";
    // echo $shipping_details."<br>";

    if ($UidExplode && $AmountExplode)
    {
        for ($i=0; $i < count($AmountExplode); $i++)
        {
                $amount = $AmountExplode[$i];
                $uid = $UidExplode[$i];
                $user = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
                    $tableName = array();
                    $tableValue =  array();
                    $stringType =  "";
                    $AmountExplodes = $user[0]->getBtcCredit() - $amount;
                    if($AmountExplodes)
                    {
                        array_push($tableName,"btc_credit");
                        array_push($tableValue,$AmountExplodes);
                        $stringType .=  "s";
                    }
                        array_push($tableValue,$uid);
                        $stringType .=  "s";
                        $userDetails = updateDynamicData($conn,"user", " WHERE uid=? ",$tableName,$tableValue,$stringType);
                        if($userDetails)
                        {
                            $tableName = array();
                            $tableValue =  array();
                            $stringType =  "";

                            $bidData = getBidData($conn, "WHERE uid =?",array("uid"),array($bid_uid),"s");
                            // pending status
                            // $statusBidData = "Running";
                            $statusBidData = "Revert";
                            if($statusBidData)
                            {
                                array_push($tableName,"status");
                                array_push($tableValue,$statusBidData);
                                $stringType .=  "s";
                            }
                                array_push($tableValue,$bidData[0]->getUid());
                                $stringType .=  "s";
                                $bidDataDetails = updateDynamicData($conn,"bid_data", " WHERE uid=? ",$tableName,$tableValue,$stringType);
                                if($bidDataDetails)
                                {
                                    $tableName = array();
                                    $tableValue =  array();
                                    $stringType =  "";

                                    $bidRecord = getBidRecord($conn, "WHERE trade_uid =?",array("trade_uid"),array($bid_uid),"s");
                                    $statusRecord = "Revert";
                                    $shippingDetails = null;
                                    if($statusRecord)
                                    {
                                        array_push($tableName,"status");
                                        array_push($tableValue,$statusRecord);
                                        $stringType .=  "s";
                                    }
                                    if(!$shippingDetails)
                                    {
                                        array_push($tableName,"shipping_details");
                                        array_push($tableValue,$shippingDetails);
                                        $stringType .=  "s";
                                    }
                                        array_push($tableValue,$bid_uid);
                                        $stringType .=  "s";
                                        $bidDataDetails = updateDynamicData($conn,"bid_record", " WHERE trade_uid=? ",$tableName,$tableValue,$stringType);
                                        if($bidDataDetails)
                                        {
                                            // $_SESSION['messageType'] = 1;
                                            // header('Location: ../adminAllBid.php?type=4');
                                            header('Location: ../adminAllRevertBid.php');
                                        }

                                }
                                    
                        }
        }
    }
    // else
    // {
    //     echo "error";
    // }

    // if(isset($_POST['bid_uid']))
    // {   
    //     $tableName = array();
    //     $tableValue =  array();
    //     $stringType =  "";
        //echo "save to database";
        // if($status)
        // {
        //     array_push($tableName,"status");
        //     array_push($tableValue,$status);
        //     $stringType .=  "s";
        // }     
        // if($shipping_details)
        // {
        //     array_push($tableName,"shipping_details");
        //     array_push($tableValue,$shipping_details);
        //     $stringType .=  "s";
        // }     
        
        // array_push($tableValue,$bid_uid);
        // $stringType .=  "s";
        // $orderUpdated = updateDynamicData($conn,"bid_record"," WHERE trade_uid = ? ",$tableName,$tableValue,$stringType);
        // echo "success";
        // if($bidDetails)
        // {
        //     for($cnt = 0;$cnt < count($bidDetails) ;$cnt++)
        //     {
        //         if($bidDetails[$cnt]->getTradeUid()==$bid_uid)
        //         {
        //             echo $bidDetails[$cnt]->getStatus();
        //         }
        //     }
        // }
        // if($orderUpdated)
        // {
        //     // echo "<br>";
        //     // echo $_POST['order_id']."<br>";
        //     // echo $payment_status."<br>";
        //     // echo $shipping_status."<br>";
        //     // echo $shipping_date."<br>";
        //     // echo $shipping_status."<br>";
        //     // echo "success";
        //     if($shipping_status =='SHIPPING'){
        //         $_SESSION['messageType'] = 1;
        //         header('Location: ../shippingOut.php?type=11');
        //     }
        //     else if($shipping_status =='DELIVERED'){
        //         $_SESSION['messageType'] = 1;
        //         header('Location: ../shippingCompleted.php?type=11');
        //     }
        // }
        // else
        // {
        //     //echo "fail";
        //     $_SESSION['messageType'] = 1;
        //     header('Location: ../editShippingRequset.php?type=2');
        // }
    // }
    // else
    // {
    //     echo "dunno";
    //     // $_SESSION['messageType'] = 1;
    //     // header('Location: ../editShippingRequset.php?type=3');
    // }

}
else 
{
    header('Location: ../index.php');
}
?>
