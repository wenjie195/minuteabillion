<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/BidData.php';
require_once dirname(__FILE__) . '/../classes/BidRecord.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();
   
     $biddingUid = rewrite($_POST['bid_uid']);

     $bidDetails = getBidRecord($conn, "WHERE trade_uid =?",array("trade_uid"),array($biddingUid),"s");
     $userUid = $bidDetails[0]->getUserUid(); 
     $bidAmount = $bidDetails[0]->getAmount(); 
     $tradingUid = $bidDetails[0]->getUid(); 

     $userRows = getUser($conn, "WHERE uid =?",array("uid"),array($userUid),"s");
     $currentCredit = $userRows[0]->getBtcCredit(); 
     $totalWin = $userRows[0]->getTotalWin();
     $addWin = $totalWin + 1;

     $refundCredit = $currentCredit + $bidAmount;

     $bidData = getBidData($conn," WHERE uid = ? ",array("uid"),array($biddingUid),"s");
     // $bidStatus = "Bid Cancelled";
     $bidStatus = "Stop";
     // $result = "Bid Cancelled";

     $resultStatus = "Win";
     $shippingStatus = "Pending";

     $notification = "Start";

     //   FOR DEBUGGING
     // echo "<br>";
     // echo $Arr."<br>";
     // echo $ArrImplode."<br>";
     // echo $ArrExplode."<br>";
     // echo $biddingUid."<br>";

     if($bidData)
     {   
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database";

          if($bidStatus)
          {
               array_push($tableName,"status");
               array_push($tableValue,$bidStatus);
               $stringType .=  "s";
          }
          array_push($tableValue,$biddingUid);
          $stringType .=  "s";
          $updateBidData = updateDynamicData($conn,"bid_data"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          if($updateBidData)
          {

               if($bidDetails)
               {   
                    $tableName = array();
                    $tableValue =  array();
                    $stringType =  "";
                    //echo "save to database";
          
                    // if($result)
                    // {
                    //      array_push($tableName,"status");
                    //      array_push($tableValue,$result);
                    //      $stringType .=  "s";
                    // }

                    if($resultStatus)
                    {
                         array_push($tableName,"status");
                         array_push($tableValue,$resultStatus);
                         $stringType .=  "s";
                    }
                    if($shippingStatus)
                    {
                         array_push($tableName,"shipping_details");
                         array_push($tableValue,$shippingStatus);
                         $stringType .=  "s";
                    }
                    if($notification)
                    {
                         array_push($tableName,"start_rate");
                         array_push($tableValue,$notification);
                         $stringType .=  "s";
                    }

                    array_push($tableValue,$tradingUid);
                    $stringType .=  "s";
                    $updateUserCredit = updateDynamicData($conn,"bid_record"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                    if($updateUserCredit)
                    {
                         // header('Location: ../adminAllBid.php');
                         // header('Location: ../adminShippingRequest.php');

                         if($userRows)
                         {   
                              $tableName = array();
                              $tableValue =  array();
                              $stringType =  "";
                              //echo "save to database";
                              if($addWin)
                              {
                                   array_push($tableName,"total_win");
                                   array_push($tableValue,$addWin);
                                   $stringType .=  "s";
                              }
                              array_push($tableValue,$userUid);
                              $stringType .=  "s";
                              $updateUserCredit = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                              if($updateUserCredit)
                              {
                                   header('Location: ../adminShippingRequest.php');
                              }
                              else
                              {
                                   echo "fail";
                              }
                         }
                         else
                         {
                              echo "error";
                         }

                    }
                    else
                    {
                         echo "fail";
                    }
               }
               else
               {
                    echo "error";
               }

          }
          else
          {
               echo "fail";
          }
     }
     else
     {
          echo "error";
     }
}
else 
{
     header('Location: ../index.php');
}  
?>