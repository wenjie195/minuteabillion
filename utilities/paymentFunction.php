<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Payments.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
// require_once dirname(__FILE__) . '/mailerFunction.php';

$userUid = $_SESSION['uid'];

function registerPayments($conn,$uid,$username,$userUid,$fromCurrency,$enteredAmount,$toCurrency,$amount,$gatewayId,$gatewayUrl,$status)
{
     if(insertDynamicData($conn,"payments",array("uid","username","user_uid","from_currency","entered_amount","to_currency","amount","gateway_id","gateway_url","status"),
          array($uid,$username,$userUid,$fromCurrency,$enteredAmount,$toCurrency,$amount,$gatewayId,$gatewayUrl,$status),"ssssssssss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());

     $userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($userUid),"s");
     $username = $userDetails[0]->getUsername();

     $enteredAmount = rewrite($_POST['entered_amount']);
     $fromCurrency = rewrite($_POST['from_currency']);
     $toCurrency = rewrite($_POST['to_currency']);
     $amount = rewrite($_POST['amount']);
     $str1 = $_POST['gateway_id'];
     // echo "<br>";

     // https://www.coinpayments.net/index.php?cmd=status&id=CPEH1TYKUNEHJVAT7X3RPLM5YG&key=35cb0afcd6ad207dc8bc8a4597e2f583

     // if(isset($_GET['id']))
     // {
     //      echo $gatewayId = $_GET['id'];
     //      echo "aaa";
     // }
     // else
     // {
     //      echo $gatewayId = "";
     //      echo "bbb";
     // }

     $gatewayId = str_replace( 'https://www.coinpayments.net/index.php?cmd=status&', '', $str1);
     
     $gatewayUrl = $_POST['gateway_url'];

     $status = "Pending";

     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $gatewayId."<br>";
     // echo $gatewayUrl."<br>";

     if(registerPayments($conn,$uid,$username,$userUid,$fromCurrency,$enteredAmount,$toCurrency,$amount,$gatewayId,$gatewayUrl,$status))
     {
          header("Location: $gatewayUrl" );
          // header('Location: ../profile.php');
          // echo "success";
     }
     else
     {    
          echo "fail";
     }  
}
else 
{
     header('Location: ../index.php');
}
?>