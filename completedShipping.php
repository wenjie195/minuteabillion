<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<meta property="og:title" content="<?php echo _ADMIN_COMPLETED_SHIPPING_REQUEST ?> | Bid Win 劲拍" />
<title><?php echo _ADMIN_COMPLETED_SHIPPING_REQUEST ?> | Bid Win 劲拍</title>
<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header.php'; ?>
<div class="width100 black-bg min-height menu-distance same-padding text-center">

	<img src="img/shipping.png" class="title-icon" alt="<?php echo _ADMIN_COMPLETED_SHIPPING_REQUEST ?>" title="<?php echo _ADMIN_COMPLETED_SHIPPING_REQUEST ?>">
	<h1 class="title-h1 blue-text"><?php echo _ADMINDASH_SHIPPING_REQUEST3 ?></h1>
	<div class="title-border margin-bottom30"></div>
	<div class="clear"></div>
              <p class="link-p"><a href="shippingRequest.php" class="white-to-blue-link"><?php echo _ADMIN_REQUEST ?></a> | <?php echo _ADMIN_COMPLETED ?></p>
                
            <div class="table-scroll">
            	<table class="table-css">
                	<thead>
                    	<tr>
                        	<th><?php echo _PROFILE_NO ?></th>
                            <th><?php echo _ADMIN_REQUEST_DATE ?></th>
                            <th><?php echo _ADMIN_MEMBER ?></th>
                            <th><?php echo _ADMIN_AUCTION_ITEM ?></th>
                            <th><?php echo _PROFILE_AUCTION_ID ?></th>
                            <th><?php echo _PROFILE_STATUS ?></th>
                        </tr>
                    </thead>
                	<tbody>
                    	<tr  data-link="./shippingDetails.php">
                        	<td>1.</td>
                            <td>06/06/2020 10:00</td>
                            <td>Alicia</td>
                            <td>0.05 BTC</td>
                            <td>JXGAHSJHSJ</td>
                            <td>Received</td>
                        </tr>
                    	                                  
                    </tbody>
                </table>
            
            </div>              
    	</div>
    
</div>



<?php include 'bottomButton.php'; ?>
<?php include 'js.php'; ?>
<script>
$('.table-css tr').click(function(){
   window.location.href = $(this).data('link');
});
</script>
<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Profile Updated !!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "fail to update profile !!"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }

    if($_SESSION['messageType'] == 2)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Password Updated !!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "fail to update password !!"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "New Password does not match with Retype Password !!";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "password length must be more that 5"; 
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "Wrong Password !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>