<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<meta property="og:title" content="<?php echo _MAINJS_INDEX_RESET_PASSWORD ?> | Bid Win 劲拍" />
<title><?php echo _MAINJS_INDEX_RESET_PASSWORD ?> | Bid Win 劲拍</title>
<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header.php'; ?>

<div class="width100 black-bg min-height menu-distance same-padding text-center">
    <img src="img/login.png" class="title-icon" alt="<?php echo _MAINJS_INDEX_RESET_PASSWORD ?>" title="<?php echo _MAINJS_INDEX_RESET_PASSWORD ?>">
    <h1 class="title-h1 blue-text"><?php echo _MAINJS_INDEX_RESET_PASSWORD ?></h1>
    <div class="title-border margin-bottom30"></div>

    <div class="clear"></div>

    <div class="middle-width">	
        <!-- <form method="POST" action=""> -->
        <form action="utilities/resetPasswordFunction.php" method="POST">

        <input type="hidden" name="checkThat" value="<?php if(isset($_GET['uid'])){echo $_GET['uid'] ;}?>">

        <p class="input-top-p">Verify Code</p>
        <div class="fake-pass-input-div">
            <input class="input-name clean password-input" type="text" placeholder="Verify Code" id="verify_code" name="verify_code" required>
        </div>

        <p class="input-top-p"><?php echo _RESET_PASSWORD_NEW_PASSWORD ?></p>
        <div class="fake-pass-input-div">
            <input class="input-name clean password-input" type="text" placeholder="<?php echo _RESET_PASSWORD_NEW_PASSWORD ?>" id="register_password" name="register_password" required>
            <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionA()" alt="View Password" title="View Password">
        </div>

        <p class="input-top-p"><?php echo _RESET_PASSWORD_CONFIRM_NEW_PASSWORD ?></p>
        <div class="fake-pass-input-div">
            <input class="input-name clean password-input" type="password" placeholder="<?php echo _RESET_PASSWORD_CONFIRM_NEW_PASSWORD ?>" id="register_retype_password" name="register_retype_password" required>
            <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionB()" alt="View Password" title="View Password">
        </div>
        <button class="blue-button white-text width100 clean register-button"><?php echo _MAINJS_INDEX_SUBMIT ?></button>

        <div class="clear"></div>

        </form>
    </div>
</div>

<?php include 'js.php'; ?>

</body>
</html>