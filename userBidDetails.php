<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BidData.php';
require_once dirname(__FILE__) . '/classes/BidRecord.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

// $bidAmount = getBidRecord($conn,"WHERE user_uid = ? ", array("user_uid") ,array($uid),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<meta property="og:title" content="<?php echo _PROFILE ?> | Bid Win 劲拍" />
<title><?php echo _PROFILE ?> | Bid Win 劲拍</title>
<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header.php'; ?>
<div class="width100 black-bg min-height menu-distance same-padding text-center force-center">
	<img src="img/bidding.png" class="title-icon" alt="<?php echo _ADMIN_BID_DETAILS ?>" title="<?php echo _ADMIN_BID_DETAILS ?>">
	<h1 class="title-h1 blue-text opacity-hover"  onclick="goBack()"><img src="img/back.png" alt="<?php echo _BOTTOM_BACK ?>" title="<?php echo _BOTTOM_BACK ?>" class="back-png"><?php echo _ADMIN_BID_DETAILS ?></h1>
	<div class="title-border margin-bottom30"></div>
    <div class="clear"></div>  


    <?php
    if(isset($_POST['bid_uid']))
    {
    $conn = connDB();
    $bidDetails = getBidData($conn,"WHERE uid = ? ", array("uid") ,array($_POST['bid_uid']),"s");

    $bidValue = getBidRecord($conn,"WHERE trade_uid = ? ", array("trade_uid") ,array($_POST['bid_uid']),"s");
    $bidAmount = getBidRecord($conn,"WHERE trade_uid = ? AND user_uid = '$uid' ", array("trade_uid") ,array($_POST['bid_uid']),"s");
    // $bidAmount = getBidRecord($conn,"WHERE trade_uid = ? AND status = 'Win' ", array("trade_uid") ,array($_POST['bid_uid']),"s");
    ?>	
        <div class="width100 overflow">
        	<img src="bidItemImage/<?php echo $bidDetails[0]->getImage(); ?>"class="item-png" alt="<?php echo $bidDetails[0]->getBidName();?>" title="<?php echo $bidDetails[0]->getBidName();?>">
			<p class="item-name-p"><?php echo $bidDetails[0]->getBidName();?></p>
        </div>
        <div class="dual-input">
            <p class="input-top-p"><?php echo _PROFILE_AUCTION_ID ?></p>
           	<p class="no-input"><?php echo $bidDetails[0]->getBidId();?></p>
        </div>
        <div class="dual-input second-dual-input">
            <p class="input-top-p"><?php echo _PROFILE_STATUS ?></p>
            <p class="no-input">
                <?php 
                    $biddingStatus = $bidAmount[0]->getStatus();
                    if($biddingStatus == 'Win')
                    {   
                        echo "Win";
                    }
                    // elseif($biddingStatus == 'Stop')
                    elseif($biddingStatus == '')
                    {   
                        echo "Winner Selection";
                    }
                    else
                    {   
                        echo "Lose";
                    }
                ?>
            </p>
        </div>

        <div class="clear"></div>

        <div class="dual-input">
            <p class="input-top-p"><?php echo _ADD_BID_STARTING_DATE ?></p>
            <p class="no-input"><?php echo $bidDetails[0]->getStartingTime();?></p>
        </div>
        <div class="dual-input second-dual-input">
            <p class="input-top-p"><?php echo _ADMIN_BID_ENDING_TIME ?></p>
            <p class="no-input"><?php echo $bidDetails[0]->getEndingTime();?></p>
        </div>

        <div class="clear"></div>
        <div class="dual-input">
            <p class="input-top-p"><?php echo _PROFILE_TOTAL_PARTICIPATE ?></p>
            <p class="no-input">
                <?php
                if($bidValue)
                {   
                    echo $totalBid = count($bidValue);
                }
                else
                {   echo $totalBid = 0;   }
                ?>
            </p>
        </div>

        <div class="dual-input second-dual-input">
            <p class="input-top-p"><?php echo _USER_YOUR_BID ?></p>
            <p class="no-input"><?php echo $bidAmount[0]->getAmount();?></p>
        </div>

        <div class="clear"></div>

        <?php
        $bidStatus = $bidAmount[0]->getStatus();
        if($bidStatus == 'Win')
        {   
        ?>

            <div class="dual-input">
                <p class="input-top-p"><?php echo _ADMIN_SHIPPING_METHOD ?></p>
                <p class="no-input">
                    <?php 
                        $shippingMethod = $bidAmount[0]->getShippingMethod();
                        if($shippingMethod == '')
                        {   
                            echo "To Be Confirm";
                        }
                        else
                        {   echo $shippingMethod;   }
                    ?>
                </p>
            </div>
            <div class="dual-input second-dual-input">
                <p class="input-top-p"><?php echo _ADMIN_DELIVERY_DATE ?></p>
                <p class="no-input">
                    <?php 
                        $shippigTimeline = $bidAmount[0]->getShippingTimeline();
                        if($shippigTimeline == '')
                        {   
                            echo "To Be Confirm";
                        }
                        else
                        {   echo $shippigTimeline;   }
                    ?>
                </p>
            </div>

        <?php
        }
        else
        {}
        ?>

        <div class="clear"></div>

    <?php
    }
    else
    {}
    ?>

</div>

<style>
.profile-div{
background: rgba(9,197,249,1);
background: -moz-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: -webkit-gradient(left top, right bottom, color-stop(0%, rgba(9,197,249,1)), color-stop(100%, rgba(4,92,233,1)));
background: -webkit-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: -o-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: -ms-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: linear-gradient(135deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#09c5f9', endColorstr='#045ce9', GradientType=1 );
}

</style>

<?php include 'bottomButton.php'; ?>
<?php include 'js.php'; ?>


<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Profile Updated !!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "fail to update profile !!"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }

    if($_SESSION['messageType'] == 2)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Password Updated !!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "fail to update password !!"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "New Password does not match with Retype Password !!";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "password length must be more that 5"; 
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "Wrong Password !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }

    if($_SESSION['messageType'] == 3)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "insufficient credit !!"; 
        }
        // else if($_GET['type'] == 2)
        // {
        //     $messageType = "fail to update profile !!"; 
        // }
        // else if($_GET['type'] == 3)
        // {
        //     $messageType = "ERROR !!";
        // }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }

}
?>

</body>
</html>