<?php
// Include the database configuration file
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BidData.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];
$newBidUid = $_SESSION['new_bid_uid'];

$conn = connDB();

$timestamp = time();

if(isset($_POST["image"]))
{
	$data = $_POST["image"];

	$image_array_1 = explode(";", $data);
	$image_array_2 = explode(",", $image_array_1[1]);
	$data = base64_decode($image_array_2[1]);
	$imageName = time() . '.png';
	file_put_contents('bidItemImage/'.$imageName, $data);

	if(isset($_POST["image"]))
	{
		$tableName = array();
		$tableValue =  array();
		$stringType =  "";	
		if($imageName)
		{
			array_push($tableName,"image");
			array_push($tableValue,$imageName);
			$stringType .=  "s";
		}
		array_push($tableValue,$newBidUid);
		$stringType .=  "s";
		$updateImage = updateDynamicData($conn,"bid_data"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
		unset($_SESSION['new_bid_uid']);
		if($updateImage)
		{
		?>
			<div>
				<img src=bidItemImage/<?php echo $imageName  ?> />
			</div>
			<h4><?php echo "Item Image Added"; ?></h4>
			<!-- <form action="./adminViewBids.php"> -->
			<form action="adminViewBids.php">
				<div class="width100 overflow text-center">     
					<button class="blue-button margin-bottom20 open-coming">Complete</button>
				</div>
			</form>
		<?php
		}
		else
		{
			echo "fail";
		}
	}
}
?>