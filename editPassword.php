<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<meta property="og:title" content="<?php echo _HEADER_EDIT_PASSWORD ?> | Bid Win 劲拍" />
<title><?php echo _HEADER_EDIT_PASSWORD ?> | Bid Win 劲拍</title>
<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header.php'; ?>

<div class="width100 black-bg min-height menu-distance same-padding text-center">
	<img src="img/edit-profile.png" class="title-icon" alt="<?php echo _HEADER_EDIT_PASSWORD ?>" title="<?php echo _HEADER_EDIT_PASSWORD ?>">
	<h1 class="title-h1 blue-text"><?php echo _HEADER_EDIT_PASSWORD ?></h1>
	<div class="title-border margin-bottom30"></div>
	<div class="clear"></div>     
        <div class="middle-width">
        <form method="POST"  action="utilities/editPasswordFunction.php">

            
                <p class="input-top-p"><?php echo _EDIT_PASSWORD_CURRENT ?></p>
                <div class="fake-pass-input-div">
                    <input class="input-name clean password-input" type="password" placeholder="<?php echo _EDIT_PASSWORD_CURRENT ?>" id="password" name="password" required>
                    <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionA()" alt="<?php echo _JS_VIEW_PASSWORD ?>" title="<?php echo _JS_VIEW_PASSWORD ?>">
                </div>
           
            <div class="clear"></div>

 			          
                <p class="input-top-p"><?php echo _MAINJS_INDEX_PASSWORD ?></p>
                <div class="fake-pass-input-div">
                    <input class="input-name clean password-input" type="password" placeholder="<?php echo _MAINJS_INDEX_PASSWORD ?>" id="register_password" name="register_password" required>
                    <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionB()" alt="<?php echo _JS_VIEW_PASSWORD ?>" title="<?php echo _JS_VIEW_PASSWORD ?>">
                </div>
            <div class="clear"></div>
                <p class="input-top-p"><?php echo _MAINJS_INDEX_CONFIRM_PASSWORD ?></p>
                <div class="fake-pass-input-div">
                    <input class="input-name clean password-input" type="password" placeholder="<?php echo _MAINJS_INDEX_CONFIRM_PASSWORD ?>" id="register_retype_password" name="register_retype_password" required>
                    <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionC()" alt="<?php echo _JS_VIEW_PASSWORD ?>" title="<?php echo _JS_VIEW_PASSWORD ?>"> 
                </div>   
			
			</div>
            <div class="clear"></div>

            <div class="width100 text-center margin-top20">
                <button class="blue-button white-text clean pointer" name="submit"><?php echo _MAINJS_INDEX_SUBMIT ?></button>
            </div>

        </form> 

</div>

<?php include 'bottomButton.php'; ?>
<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Password Updated !!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "fail to update password !!"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "New Password does not match with Retype Password !!";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "password length must be more that 5"; 
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "Wrong Password !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

<script>
function myFunctionA()
{
    var x = document.getElementById("password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionB()
{
    var x = document.getElementById("register_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionC()
{
    var x = document.getElementById("register_retype_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
</script>

</body>
</html>