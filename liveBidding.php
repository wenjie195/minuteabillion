<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BidData.php';
require_once dirname(__FILE__) . '/classes/BidRecord.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$tz = 'Asia/Kuala_Lumpur';
$timestamp = time();
$dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
$dt->setTimestamp($timestamp); //adjust the object to correct timestamp
$time = $dt->format('Y-m-d H:i');
// $time2 = "2020-08-08 00:00";

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

// $bidData = getBidData($conn, "WHERE status = 'Running' AND starting_time < '$time' AND ending_time > '$time' ORDER BY date_created DESC LIMIT 3");

//testing
// $bidData = getBidData($conn, "WHERE status = 'Running' ORDER BY date_created DESC");
//real 
$bidData = getBidData($conn, "WHERE status = 'Running' AND starting_time < '$time' AND ending_time > '$time' ORDER BY date_created DESC");

if($bidData)
{
    for($cnt = 0;$cnt < count($bidData) ;$cnt++)
    {
    ?>    

        <div class="bidding-repeat-div">
            <img src="bidItemImage/<?php echo $bidData[$cnt]->getImage(); ?>" class="step-png" alt="<?php echo $bidData[$cnt]->getBidName(); ?>" title="<?php echo $bidData[$cnt]->getBidName(); ?>">
            <p class="content-title bid-item-title text-overflow"><?php echo $bidData[$cnt]->getBidName(); ?></p>

            <?php 
                $bidUid = $bidData[$cnt]->getUid(); 
                $conn = connDB();
                $bidDetails = getBidRecord($conn, "WHERE trade_uid =?",array("trade_uid"),array($bidUid),"s");
                if($bidDetails)
                {   
                    $totalBid = count($bidDetails);
                }
                else
                {   $totalBid = 0;   }
            ?>

            <p class="content-text"><?php echo _PROFILE_TOTAL_BIDS ?>: <?php echo $totalBid; ?></p>
            <!-- <div class="blue-button open-coming "><?php //echo _INDEX_PLACE_BID ?></div> -->
            <p class="content-text white-text margin-top20">
                <?php 
                    $endingTime = $bidData[$cnt]->getEndingTime();
                    $timestamp;
                    $date2 = strtotime($endingTime);  
                    $diff = abs($date2 - $timestamp);  
                    
                    $years = floor($diff / (365*60*60*24));  
                    $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));  
                    $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24)); 
                    $hours = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24) / (60*60));  
                    $minutes = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60);
                    $seconds = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60 - $minutes*60));  

                    $spacing = " " ;
                    $input1 = "d";
                    $input2 = "h";
                    $input3 = "m";
                    $input4 = "s";
                    echo $duration = $spacing.$days.$input1.$spacing.$hours.$input2.$spacing.$minutes.$input3.$spacing.$seconds.$input4;
                ?>
            </p>

            <?php
            $bidRows = getBidRecord($conn," WHERE trade_uid = '$bidUid' AND user_uid = '$uid' ");
            $existingBidDetails = $bidRows[0];
            if (!$existingBidDetails)
            {
            ?>
                <form method="POST" action="bid.php" class="hover1">
                    <button class="blue-button" type="submit" name="bid_uid" value="<?php echo $bidData[$cnt]->getUid(); ?>">
                        <?php echo _INDEX_PLACE_BID ?>
                    </button>
                </form>
            <?php
            }
            else
            {
                echo "<p class='content-text'>Bid Placed</p>";
            }
            ?>
            
            <!-- <p class="content-text white-text margin-top20" id="timer"></p> -->
        </div>  

    <?php
    }
}
?> 