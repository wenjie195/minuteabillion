<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Countries.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$countryList = getCountries($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<meta property="og:title" content="<?php echo _ADMIN_SHIPPING_DETAILS ?> | Bid Win 劲拍" />
<title><?php echo _ADMIN_SHIPPING_DETAILS ?> | Bid Win 劲拍</title>
<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header.php'; ?>

<div class="width100 black-bg min-height menu-distance same-padding text-center">
	<img src="img/shipping.png" class="title-icon" alt="<?php echo _ADMIN_SHIPPING_DETAILS ?>" title="<?php echo _ADMIN_SHIPPING_DETAILS ?>">
	<h1 class="title-h1 blue-text opacity-hover"  onclick="goBack()"><img src="img/back.png" alt="<?php echo _BOTTOM_BACK ?>" title="<?php echo _BOTTOM_BACK ?>" class="back-png"><?php echo _ADMIN_SHIPPING_DETAILS3 ?></h1>
	<div class="title-border margin-bottom30"></div>
	<div class="clear"></div>    
        <form method="POST"  action="utilities/editProfileFunction.php">
		<div class="width100 overflow">
        	<img src="img/item-01.png" class="item-png" alt="">
            <p class="item-name-p">0.005 BTC</p>
        </div>
        <div class="dual-input">
            <p class="input-top-p"><?php echo _BOTTOM_BID ?></p>
           	<p class="no-input-p">5000 <?php echo _PROFILE_CREDITS ?></p>
        </div>
        <div class="dual-input second-dual-input">
            <p class="input-top-p"><?php echo _PROFILE_AUCTION_ID ?></p>
            <p class="no-input-p">JSH1212821</p>
        </div>

        <div class="clear"></div>

        <div class="dual-input">
            <p class="input-top-p"><?php echo _ADMIN_WINNER ?></p>
            <p class="no-input-p">Alicia</p>
        </div>
        <div class="dual-input second-dual-input">
            <p class="input-top-p"><?php echo _ADMIN_BID_ENDING_TIME ?></p>
            <input class="input-name clean" type="text" placeholder="<?php echo _MAINJS_INDEX_CONTACT ?>" value="<?php echo $userData->getPhone() ;?>" id="update_contact" name="update_contact" required>
        </div>
        <div class="clear"></div>

        <div class="dual-input">
            <p class="input-top-p"><?php echo _MAINJS_INDEX_CONTACT ?></p>
            <p class="no-input-p">016 522 9192</p>
        </div>
        <div class="dual-input second-dual-input">
            <p class="input-top-p"><?php echo _MAINJS_INDEX_EMAIL ?></p>
            <p class="no-input-p">alicia@gmail.com</p>
        </div>
        <div class="clear"></div>

        <div class="dual-input">
            <p class="input-top-p"><?php echo _MAINJS_INDEX_ADDRESS ?></p>
            <p class="no-input-p">133, Street, City, Postcode, State, Country</p>
        </div>
        <div class="dual-input second-dual-input">
            <p class="input-top-p"><?php echo _MAINJS_INDEX_POSTCODE ?></p>
            <p class="no-input-p">34000</p>
        </div>

        <div class="clear"></div>

        <div class="dual-input">
            <p class="input-top-p"><?php echo _MAINJS_INDEX_CITY ?></p>
            <p class="no-input-p">Taiping</p>
        </div>
        <div class="dual-input second-dual-input">
            <p class="input-top-p"><?php echo _MAINJS_INDEX_COUNTRY ?></p>
            <p class="no-input-p">Malaysia</p>
        </div>

        <div class="clear"></div>

        <div class="dual-input">
            <p class="input-top-p"><?php echo _JS_USDT_ADDRESS ?></p>
            <input class="input-name clean" type="text" placeholder="<?php echo _JS_USDT_ADDRESS ?>" value="<?php echo $userData->getUsdtWallet() ;?>" id="update_usdt_add" name="update_usdt_add" required>
        </div>
        <div class="dual-input second-dual-input">
            <p class="input-top-p"><?php echo _JS_BTC_ADDRESS ?></p>
            <input class="input-name clean" type="text" placeholder="<?php echo _JS_BTC_ADDRESS ?>" value="<?php echo $userData->getBtcWallet() ;?>" id="update_btc_add" name="update_btc_add" required>
        </div>

        <div class="clear"></div>

        <div class="width100 text-center margin-top20">
            <button class="blue-button white-text clean pointer" name="submit"><?php echo _MAINJS_INDEX_SUBMIT ?></button>
        </div>
        </form> 

</div>
<?php include 'bottomButton.php'; ?>
<?php include 'js.php'; ?>

</body>
</html>