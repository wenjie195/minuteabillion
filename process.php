<?php

require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BidData.php';
require_once dirname(__FILE__) . '/classes/BidRecord.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

require_once dirname(__FILE__) . '/init.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$bidAmount = getBidRecord($conn,"WHERE user_uid = ? ", array("user_uid") ,array($uid),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

$basicInfo = $coin->GetBasicProfile();
$username = $basicInfo['result']['public_name'];

$amount = $_POST['amount'];
$email = $_POST['email'];
// $email = "asd@gmail.com";

$scurrency = "USD";
$rcurrency = "BTC";

$request = [
    'amount' => $amount,
    'currency1' => $scurrency,
    'currency2' => $rcurrency,
    'buyer_email' => $email,
    // 'item' => "Donate to myPHPnotes",
    'item' => "Payment to Bid Win (Testing)",
    'address' => "",
    // 'ipn_url' => "https://donate.myphpnotes.com/webhook.php"
    'ipn_url' => "https://minuteabillion.co/testing/biddingRules.php"
];

$result = $coin->CreateTransaction($request);
// var_dump($result);
if ($result['error'] == "ok")
{

} 
    else
{
    print 'Error: ' . $result['error'] . "\n";
    die();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<meta property="og:title" content="<?php echo _PAYMENT ?> | Bid Win 劲拍" />
<title><?php echo _PAYMENT ?> | minuteabillion</title>
<?php include 'css.php'; ?>	
</head>
<body>
<?php include 'header.php'; ?>

<div class="width100 black-bg min-height menu-distance text-center">
    <img src="img/payment.png" class="title-icon" alt="<?php echo _PAYMENT ?>" title="<?php echo _PAYMENT ?>">
    <h1 class="title-h1 blue-text"><?php echo _PAYMENT ?></h1>
    <div class="title-border margin-bottom30"></div>
    <h1 class="title-h1"><?php echo _PAYMENT_PAY_WITH ?></h1>
    <p style="font-style: italic;"><?php echo _PAYMENT_TO ?> <strong><?php echo $username; ?></strong></p>
    <div class="middle-width mob-padding">
        <!-- <form> -->
        <form method="POST" action="utilities/paymentFunction.php">
            <label for="amount" class="input-top-p"><?php echo _DEPOSIT_AMOUNT ?> (<?php echo $rcurrency; ?>)</label>
            <h1  class="title-h1"><?php echo $result['result']['amount'] ?> <?php echo $rcurrency ?></h1>

            <input class="input-name clean" type="hidden" value="<?php echo $amount;?>" id="entered_amount" name="entered_amount" readonly>
            <input class="input-name clean" type="hidden" value="<?php echo $scurrency;?>" id="from_currency" name="from_currency" readonly>
            <input class="input-name clean" type="hidden" value="<?php echo $rcurrency;?>" id="to_currency" name="to_currency" readonly>
            <input class="input-name clean" type="hidden" value="<?php echo $result['result']['amount'];?>" id="amount" name="amount" readonly>
            <input class="input-name clean" type="hidden" value="<?php echo $result['result']['status_url'];?>" id="gateway_id" name="gateway_id" readonly>
            <input class="input-name clean" type="hidden" value="<?php echo $result['result']['status_url'];?>" id="gateway_url" name="gateway_url" readonly>

            <div class="width100 text-center margin-top20">
                <!-- <a target="_blank" href="<?php //echo $result['result']['status_url'] ?>">
                    <div class="blue-button white-text clean pointer ow-width100"><?php //echo _PAYMENT_PAY ?></div>
                </a> -->

                <button target="_blank" class="blue-button white-text clean pointer" name="submit">
                    <div class="blue-button white-text clean pointer ow-width100"><?php echo _PAYMENT_PAY ?></div>
                </button>
            </div>
        </form>
    </div>
</div>

<div class="clear"></div>

<?php include 'bottomButton.php'; ?>
<?php include 'js.php'; ?>

</body>
</html>`