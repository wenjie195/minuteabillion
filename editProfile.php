<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Countries.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$countryList = getCountries($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<meta property="og:title" content="<?php echo _PROFILE_EDIT ?> | Bid Win 劲拍" />
<title><?php echo _PROFILE_EDIT ?> | Bid Win 劲拍</title>
<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header.php'; ?>

<div class="width100 black-bg min-height menu-distance same-padding text-center">
	<img src="img/edit-profile.png" class="title-icon" alt="<?php echo _PROFILE_EDIT ?>" title="<?php echo _PROFILE_EDIT ?>">
	<h1 class="title-h1 blue-text"><?php echo _PROFILE_EDIT ?></h1>
	<div class="title-border margin-bottom30"></div>
	<div class="clear"></div>    
        <form method="POST"  action="utilities/editProfileFunction.php">
        <!-- <form method="POST"  action="#"> -->
        <div class="dual-input">
            <p class="input-top-p"><?php echo _MAINJS_INDEX_NICKNAME ?></p>
            <input class="input-name clean" type="text" placeholder="<?php echo _MAINJS_INDEX_NICKNAME ?>" value="<?php echo $userData->getNickname() ;?>" id="update_nickname" name="update_nickname" required>
        </div>
        <div class="dual-input second-dual-input">
            <p class="input-top-p"><?php echo _MAINJS_INDEX_USERNAME ?></p>
            <input class="input-name clean" type="text" placeholder="<?php echo _MAINJS_INDEX_USERNAME ?>" value="<?php echo $userData->getUsername() ;?>" id="update_username" name="update_username" required>
        </div>

        <div class="clear"></div>

        <div class="dual-input">
            <p class="input-top-p"><?php echo _MAINJS_INDEX_EMAIL ?></p>
            <input class="input-name clean" type="email" placeholder="<?php echo _MAINJS_INDEX_EMAIL ?>" value="<?php echo $userData->getEmail() ;?>" id="update_email" name="update_email" required>
        </div>
        <div class="dual-input second-dual-input">
            <p class="input-top-p"><?php echo _MAINJS_INDEX_CONTACT ?></p>
            <input class="input-name clean" type="text" placeholder="<?php echo _MAINJS_INDEX_CONTACT ?>" value="<?php echo $userData->getPhone() ;?>" id="update_contact" name="update_contact" required>
        </div>

        <div class="clear"></div>

        <div class="dual-input">
            <p class="input-top-p"><?php echo _MAINJS_INDEX_ADDRESS ?></p>
            <input class="input-name clean" type="text" placeholder="<?php echo _MAINJS_INDEX_ADDRESS ?>" value="<?php echo $userData->getAddress() ;?>" id="update_address" name="update_address" required>
        </div>
        <div class="dual-input second-dual-input">
            <p class="input-top-p"><?php echo _MAINJS_INDEX_POSTCODE ?></p>
            <input class="input-name clean" type="text" placeholder="<?php echo _MAINJS_INDEX_POSTCODE ?>" value="<?php echo $userData->getPostcode() ;?>" id="update_postcode" name="update_postcode" required>
        </div>

        <div class="clear"></div>

        <div class="dual-input">
            <p class="input-top-p"><?php echo _MAINJS_INDEX_CITY ?></p>
            <input class="input-name clean" type="text" placeholder="<?php echo _MAINJS_INDEX_CITY ?>" value="<?php echo $userData->getCity() ;?>" id="update_city" name="update_city" required>
        </div>
        <div class="dual-input second-dual-input">
            <p class="input-top-p"><?php echo _MAINJS_INDEX_COUNTRY ?></p>
            <select class="input-name clean" id="update_country" name="update_country" required>
                <?php
                    {
                        for ($cnt=0; $cnt <count($countryList) ; $cnt++)
                        {
                            if ($userData->getCountry() == $countryList[$cnt]->getEnName())
                            {
                            ?>
                                <option selected value="<?php echo $countryList[$cnt]->getEnName(); ?>"> 
                                    <?php echo $countryList[$cnt]->getEnName(); ?>
                                </option>
                            <?php
                            }
                            else
                            {
                            ?>
                                <option value="<?php echo $countryList[$cnt]->getEnName(); ?>"> 
                                    <?php echo $countryList[$cnt]->getEnName(); ?>
                                </option>
                            <?php
                            }
                        }
                    }
                ?>
            </select>
        </div>

        <div class="clear"></div>

        <div class="dual-input">
            <p class="input-top-p"><?php echo _JS_USDT_ADDRESS ?></p>
            <input class="input-name clean" type="text" placeholder="<?php echo _JS_USDT_ADDRESS ?>" value="<?php echo $userData->getUsdtWallet() ;?>" id="update_usdt_add" name="update_usdt_add" required>
        </div>
        <div class="dual-input second-dual-input">
            <p class="input-top-p"><?php echo _JS_BTC_ADDRESS ?></p>
            <input class="input-name clean" type="text" placeholder="<?php echo _JS_BTC_ADDRESS ?>" value="<?php echo $userData->getBtcWallet() ;?>" id="update_btc_add" name="update_btc_add" required>
        </div>

        <div class="clear"></div>

        <div class="width100 text-center margin-top20">
            <button class="blue-button white-text clean pointer" name="submit"><?php echo _MAINJS_INDEX_SUBMIT ?></button>
        </div>
        </form> 

</div>
<?php include 'bottomButton.php'; ?>
<?php include 'js.php'; ?>

</body>
</html>