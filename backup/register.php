<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Countries.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$countryList = getCountries($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Register | minuteabillion" />
<title>Register | minuteabillion</title>
<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header.php'; ?>

        <form method="POST" action="utilities/registerFunction.php">

            <div class="dual-input">
                <p class="input-top-p"><?php echo _MAINJS_INDEX_NICKNAME ?></p>
                <input class="input-name clean" type="text" placeholder="<?php echo _MAINJS_INDEX_NICKNAME ?>" id="register_nickname" name="register_nickname" required>
			</div>
            <div class="dual-input second-dual-input">
                <p class="input-top-p"><?php echo _MAINJS_INDEX_USERNAME ?></p>
                <input class="input-name clean" type="text" placeholder="<?php echo _MAINJS_INDEX_USERNAME ?>" id="register_username" name="register_username" required>
			</div>

            <div class="clear"></div>

            <div class="dual-input">
                <p class="input-top-p"><?php echo _MAINJS_INDEX_EMAIL ?></p>
                <input class="input-name clean" type="email" placeholder="<?php echo _MAINJS_INDEX_EMAIL ?>" id="register_email" name="register_email" required>
			</div>
            <div class="dual-input second-dual-input">
                <p class="input-top-p"><?php echo _MAINJS_INDEX_CONTACT ?></p>
                <input class="input-name clean" type="text" placeholder="<?php echo _MAINJS_INDEX_CONTACT ?>" id="register_contact" name="register_contact" required>
			</div>
            
            <div class="clear"></div>

            <div class="dual-input">
                <p class="input-top-p"><?php echo _MAINJS_INDEX_PASSWORD ?></p>
                <input class="input-name clean" type="password" placeholder="<?php echo _MAINJS_INDEX_PASSWORD ?>" id="register_password" name="register_password" required>
                <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionA()" alt="View Password" title="View Password">
			</div>
            <div class="dual-input second-dual-input">
                <p class="input-top-p"><?php echo _MAINJS_INDEX_CONFIRM_PASSWORD ?></p>
                <input class="input-name clean" type="password" placeholder="<?php echo _MAINJS_INDEX_CONFIRM_PASSWORD ?>" id="register_retype_password" name="register_retype_password" required>
                <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionB()" alt="View Password" title="View Password">    
			</div>
            
            <div class="clear"></div>

            <div class="dual-input">
                <p class="input-top-p"><?php echo _MAINJS_INDEX_ADDRESS ?></p>
                <input class="input-name clean" type="text" placeholder="<?php echo _MAINJS_INDEX_ADDRESS ?>" id="register_address" name="register_address" required>
			</div>
            <div class="dual-input second-dual-input">
                <p class="input-top-p"><?php echo _MAINJS_INDEX_POSTCODE ?></p>
                <input class="input-name clean" type="text" placeholder="<?php echo _MAINJS_INDEX_POSTCODE ?>" id="register_postcode" name="register_postcode" required>
			</div>
            
            <div class="clear"></div>

            <div class="dual-input">
                <p class="input-top-p"><?php echo _MAINJS_INDEX_CITY ?></p>
                <input class="input-name clean" type="text" placeholder="<?php echo _MAINJS_INDEX_CITY ?>" id="register_city" name="register_city" required>
			</div>
            <div class="dual-input second-dual-input">
                <p class="input-top-p"><?php echo _MAINJS_INDEX_COUNTRY ?></p>
                <select class="clean de-input" id="register_country" name="register_country" required>
                    <option>Please Select Country</option>
                    <?php
                    for ($cntPro=0; $cntPro <count($countryList) ; $cntPro++)
                    {
                    ?>
                        <option value="<?php echo $countryList[$cntPro]->getEnName(); ?>">
                            <?php echo $countryList[$cntPro]->getEnName();?>
                        </option>
                    <?php
                    }
                    ?>
                </select>
			</div>
            
            <div class="clear"></div>

            <div class="dual-input">
                <p class="input-top-p">USDT (ERC20) Wallet Address</p>
                <input class="input-name clean" type="text" placeholder="USDT (ERC20) Wallet Address" id="register_usdt_add" name="register_usdt_add" required>
			</div>
            <div class="dual-input second-dual-input">
                <p class="input-top-p">BTC Wallet Address</p>
                <input class="input-name clean" type="text" placeholder="BTC Wallet Address" id="register_btc_add" name="register_btc_add" required>
			</div>
            
            <div class="clear"></div>

            <button class="blue-button white-text width100 clean register-button"  name="register"><?php echo _MAINJS_INDEX_REGISTER ?></button>

            <div class="clear"></div>
        </form>

<?php include 'js.php'; ?>

<script>
function myFunctionA()
{
    var x = document.getElementById("register_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionB()
{
    var x = document.getElementById("register_retype_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
</script>

</body>
</html>