<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<meta property="og:title" content="<?php echo _PROFILE ?> | minuteabillion" />
<title><?php echo _PROFILE ?> | minuteabillion</title>
<?php include 'css.php'; ?>
</head>
<body class="body">

<div class="width100 min-height100 menu-distance same-padding text-center">
    <img src="img/bidding.png" class="title-icon" alt="<?php echo _JS_CONFIRM_BIDDING ?>" title="<?php echo _JS_CONFIRM_BIDDING ?>">
    <h1 class="title-h1 blue-text"><?php echo _JS_CONFIRM_BIDDING ?></h1>
    <div class="title-border margin-bottom30"></div>
	<p class="p-text  ow-black-text"><?php echo _PROFILE_ITEM ?></p>
    <p class="p-title ow-black-text">0.05 Bitcoin</p>    
	<p class="p-text  ow-black-text"><?php echo _PROFILE_AUCTION_ID ?></p>
    <p class="p-title ow-black-text">J113124214</p>      
	<p class="p-text  ow-black-text"><?php echo _PROFILE_TOTAL_BIDS ?></p>
    <p class="p-title ow-black-text">7</p>
    <div class="width100 overflow">
    <form>
						<input class="checkbox-budget" type="radio" name="budget" id="bid100" checked>
						<label class="for-checkbox-budget" for="bid100">
							<span data-hover="100">100</span>
						</label>
						<input class="checkbox-budget" type="radio" name="budget" id="bid250">
						<label class="for-checkbox-budget middle-checkbox" for="bid250">							
							<span data-hover="250">250</span>
						</label>
						<input class="checkbox-budget" type="radio" name="budget" id="bid500">
						<label class="for-checkbox-budget" for="bid500">							
							<span data-hover="500">500</span>
						</label>
						<input class="checkbox-budget" type="radio" name="budget" id="bid1000">
						<label class="for-checkbox-budget" for="bid1000">							
							<span data-hover="1000">1000</span>
						</label>
						<input class="checkbox-budget" type="radio" name="budget" id="bid1500">
						<label class="for-checkbox-budget middle-checkbox" for="bid1500">							
							<span data-hover="1500">1500</span>
						</label>
						<input class="checkbox-budget" type="radio" name="budget" id="bid2000">
						<label class="for-checkbox-budget" for="bid2000">							
							<span data-hover="2000">2000</span>
						</label> 
						<input class="checkbox-budget" type="radio" name="budget" id="bid3000">
						<label class="for-checkbox-budget" for="bid3000">							
							<span data-hover="3000">3000</span>
						</label>                        
						<input class="checkbox-budget" type="radio" name="budget" id="bid4000">
						<label class="for-checkbox-budget middle-checkbox" for="bid4000">							
							<span data-hover="4000">4000</span>
						</label>                          
						<input class="checkbox-budget" type="radio" name="budget" id="bid5000">
						<label class="for-checkbox-budget" for="bid5000">							
							<span data-hover="5000">5000</span>
						</label>                         
                        <button class="blue-button clean"><?php echo _MAINJS_INDEX_SUBMIT ?></button>
                        
                           
    </form>    
    </div>
</div>

<?php include 'js.php'; ?>


<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Profile Updated !!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "fail to update profile !!"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }

    if($_SESSION['messageType'] == 2)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Password Updated !!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "fail to update password !!"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "New Password does not match with Retype Password !!";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "password length must be more that 5"; 
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "Wrong Password !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>