<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BidData.php';
require_once dirname(__FILE__) . '/classes/BidRecord.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$tz = 'Asia/Kuala_Lumpur';
$timestamp = time();
$dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
$dt->setTimestamp($timestamp); //adjust the object to correct timestamp
$time = $dt->format('Y-m-d H:i');
// $time2 = "2020-08-08 00:00";

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

// $bidData = getBidData($conn);
// $bidData = getBidData($conn, "WHERE status = 'Running' ORDER BY date_created DESC LIMIT 3");
// $bidData = getBidData($conn, "WHERE status = 'Running' AND starting_time < '$time' AND ending_time > '$time' ORDER BY date_created DESC LIMIT 3");
$bidData = getBidData($conn, "WHERE status = 'Running' AND starting_time < '$time' AND ending_time > '$time' ORDER BY date_created DESC");
// $bidData = getBidData($conn, "ORDER BY date_created ASC LIMIT 3");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<meta property="og:title" content="<?php echo _BIDDING_ITEMS ?> | minuteabillion" />
<title><?php echo _BIDDING_ITEMS ?> | minuteabillion</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 black-bg min-height menu-distance same-padding text-center">

    <img src="img/items.png" class="title-icon" alt="<?php echo _BIDDING_ITEMS ?>" title="<?php echo _BIDDING_ITEMS ?>">
    <h1 class="title-h1 blue-text"><?php echo _BIDDING_ITEMS ?></h1>
    <div class="title-border"></div>
    <div class="clear"></div>

    <div class="bidding-big-width margin-top30">

    <div id="divLiveBidding">
    </div>

                            <?php
                            if($bidData)
                            {
                                for($cnt = 0;$cnt < count($bidData) ;$cnt++)
                                {
                                ?>    

                                    <div class="bidding-repeat-div">
                                        <img src="bidItemImage/<?php echo $bidData[$cnt]->getImage(); ?>" class="step-png" alt="BITCOIN" title="BITCOIN">
                                        <p class="content-title bid-item-title text-overflow"><?php echo $bidData[$cnt]->getBidName(); ?></p>

                                        <?php 
                                            $bidUid = $bidData[$cnt]->getUid(); 
                                            $conn = connDB();
                                            $bidDetails = getBidRecord($conn, "WHERE trade_uid =?",array("trade_uid"),array($bidUid),"s");
                                            if($bidDetails)
                                            {   
                                                $totalBid = count($bidDetails);
                                            }
                                            else
                                            {   $totalBid = 0;   }
                                        ?>

                                        <p class="content-text"><?php echo _PROFILE_TOTAL_BIDS ?>: <?php echo $totalBid; ?></p>
                                        <!-- <div class="blue-button open-coming "><?php //echo _INDEX_PLACE_BID ?></div> -->
                                        <p class="content-text white-text margin-top20">
                                            <?php 
                                                $endingTime = $bidData[$cnt]->getEndingTime();
                                                $timestamp;
                                                $date2 = strtotime($endingTime);  
                                                $diff = abs($date2 - $timestamp);  
                                                
                                                $years = floor($diff / (365*60*60*24));  
                                                $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));  
                                                $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24)); 
                                                $hours = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24) / (60*60));  
                                                $minutes = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24 - $days*60*60*24 - $hours*60*60)/ 60);

                                                $spacing = " " ;
                                                $input3 = "Days";
                                                $input4 = "Hours";
                                                $input5 = "Minutes";
                                                echo $duration = $spacing.$days.$input3.$spacing.$hours.$input4.$spacing.$minutes.$input5;
                                            ?>
                                        </p>

                                        <?php
                                        $bidRows = getBidRecord($conn," WHERE trade_uid = '$bidUid' AND user_uid = '$uid' ");
                                        $existingBidDetails = $bidRows[0];
                                        if (!$existingBidDetails)
                                        {
                                        ?>
                                            <form method="POST" action="bid.php" class="hover1">
                                                <button class="blue-button" type="submit" name="bid_uid" value="<?php echo $bidData[$cnt]->getUid(); ?>">
                                                    <?php echo _INDEX_PLACE_BID ?>
                                                </button>
                                            </form>
                                        <?php
                                        }
                                        else
                                        {
                                            echo "You already place a bid for this item !!";
                                        }
                                        ?>
                                        
                                        <!-- <p class="content-text white-text margin-top20" id="timer"></p> -->
                                    </div>  

                                <?php
                                }
                            }
                            ?> 

    </div>           

<div class="clear"></div>

</div>

<style>
.bid-div{
background: rgba(9,197,249,1);
background: -moz-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: -webkit-gradient(left top, right bottom, color-stop(0%, rgba(9,197,249,1)), color-stop(100%, rgba(4,92,233,1)));
background: -webkit-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: -o-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: -ms-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: linear-gradient(135deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#09c5f9', endColorstr='#045ce9', GradientType=1 );
}
</style>

<?php include 'bottomButton.php'; ?>
<?php include 'js.php'; ?>

<script type="text/javascript">
    $(document).ready(function()
    {
        $("#divLiveBidding").load("liveBidding.php");
    setInterval(function()
    {
        $("#divLiveBidding").load("liveBidding.php");
    }, 5000);
    });
</script>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Bid Placed Successfully !!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "fail to update credit on user table !!"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR !!";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "fail to backup bid data !!"; 
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "fail to record bid data !!"; 
        }
        else if($_GET['type'] == 6)
        {
            $messageType = "you already bid for this item !!"; 
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }

}
?>

<script type="text/javascript" src="js/firefly.js"></script>
<script>
  $.firefly({
    color: '#3399ff',
    minPixel: 1,
    maxPixel: 4,
    total : 60,
    on: '#firefly'
});</script>

<?php

if( $_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['send_email_button'])) {

    // EDIT THE 2 LINES BELOW AS REQUIRED
    $email_to = "sherry2.vidatech@gmail.com";
    $email_subject = "Contact Form via minuteabillion website";
 
    function died($error) 
	{
        // your error code can go here
		echo '<script>alert("We are very sorry, but there were error(s) found with the form you submitted.\n\nThese errors appear below.\n\n';
		echo $error;
        echo '\n\nPlease go back and fix these errors.\n\n")</script>';
        die();
    }
 
 
    // validation expected data exists
    if(!isset($_POST['name']) ||
        !isset($_POST['email']) ||
		!isset($_POST['telephone'])) {
        died('We are sorry, but there appears to be a problem with the form you submitted.');       
    }
	
     
 
    $first_name = $_POST['name']; // required
    $email_from = $_POST['email']; // required
	$telephone = $_POST['telephone']; //required
    $comments = $_POST['comments']; 
    $contactOption = $_POST['contact-option']; // required
    $contactMethod = null;
	
	//$error_message = '<script>alert("The name you entered does not appear to be valid.");</script>';
	//if($first_name == ""){
	//	echo $error_message;
	//}

    if($contactOption == null || $contactOption == ""){
        $contactMethod = "don\'t bother me";
    }else if($contactOption == "contact-more-info"){
        $contactMethod = "I want to be contacted with more information about your company's offering marketing services and consulting";
    }else if($contactOption == "contact-on-request"){
        $contactMethod = "I just want to be contacted based on my request/ inquiry";
    }else{
        $contactMethod = "error getting contact options";
		$error_message .="Error getting contact options\n\n";
    }

    $error_message = "";
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
 
  if(!preg_match($email_exp,$email_from)) {
    $error_message .= 'The email address you entered does not appear to be valid.\n';
  }
 
 
    $string_exp = "/^[A-Za-z .'-]+$/";
 
  if(!preg_match($string_exp,$first_name)) {
    $error_message .= 'The name you entered does not appear to be valid.\n';
  }
 


 
  if(strlen($error_message) > 0) {
    died($error_message);
  }
 
    $email_message = "Form details below.\n\n";
 
     
    function clean_string($string) {
      $bad = array("content-type","bcc:","to:","cc:","href");
      return str_replace($bad,"",$string);
    }
 
    $email_message .= "Name: ".clean_string($first_name)."\n";
    $email_message .= "Email: ".clean_string($email_from)."\n";
	$email_message .= "Telephone: ".clean_string($telephone)."\n";   
    $email_message .= "Message : ".clean_string($comments)."\n";
    $email_message .= "Contact Option : ".clean_string($contactMethod)."\n";

// create email headers
$headers = 'From: '.$email_from."\r\n".
'Reply-To: '.$email_from."\r\n" .
'X-Mailer: PHP/' . phpversion();
@mail($email_to, $email_subject, $email_message, $headers);  
echo '<script>alert("Thank you! We will be in contact shortly!")</script>';

?>
<!-- include your own success html here -->

<!--Thank you for contacting us. We will be in touch with you very soon.-->
<?php
 
}
?>

</body>
</html>