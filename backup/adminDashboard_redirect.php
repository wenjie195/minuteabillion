<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BidData.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

// $userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
// $userData = $userDetails[0];

$allUser = getUser($conn, "WHERE user_type = 1");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<meta property="og:title" content="<?php echo _ADMINDASH_DASHBOARD ?> | minuteabillion" />

<!-- <meta http-equiv="refresh" content="5;url=https://www.google.com/"> -->

<title><?php echo _ADMINDASH_DASHBOARD ?> | minuteabillion</title>
<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header.php'; ?>
<div class="width100 black-bg min-height menu-distance same-padding text-center">
	<img src="img/dashboard.png" class="title-icon" alt="<?php echo _ADMINDASH_DASHBOARD ?>" title="<?php echo _ADMINDASH_DASHBOARD ?>">
    <h1 class="title-h1 blue-text"><?php echo _ADMINDASH_DASHBOARD ?></h1>
	<div class="title-border margin-bottom30"></div>
	<div class="clear"></div> 
    <a href="depositRequest.php">
        <div class="five-div darkbg-box">
            <img src="img/dash1.png" class="dash-img" alt="<?php echo _ADMINDASH_DEPOSIT_REQUEST ?>" title="<?php echo _ADMINDASH_DEPOSIT_REQUEST ?>">
            <p class="five-div-title"><?php echo _ADMINDASH_DEPOSIT_REQUEST ?></p>
        </div>
    </a>
    <a href="adminShippingRequest.php">
        <div class="five-div second-five-div darkbg-box third-mid-box">
            <img src="img/dash2.png" class="dash-img" alt="<?php echo _ADMINDASH_SHIPPING_REQUEST ?>" title="<?php echo _ADMINDASH_SHIPPING_REQUEST ?>">
            <p class="five-div-title"><?php echo _ADMINDASH_SHIPPING_REQUEST ?></p>
        </div>
    </a>
    <a href="adminAddNewBidding.php">
        <div class="five-div darkbg-box">
            <img src="img/dash3.png" class="dash-img" alt="<?php echo _ADMINDASH_ADD_NEW_BIDDING ?>" title="<?php echo _ADMINDASH_ADD_NEW_BIDDING ?>">
            <p class="five-div-title"><?php echo _ADMINDASH_ADD_NEW_BIDDING ?></p>
        </div> 
    </a>
    <a href="adminViewBiddingItems.php">
        <div class="five-div second-five-div darkbg-box">
            <img src="img/dash5.png" class="dash-img" alt="<?php echo _ADMINDASH_SELECT_WINNER ?>" title="<?php echo _ADMINDASH_SELECT_WINNER ?>">
            <p class="five-div-title"><?php echo _ADMINDASH_SELECT_WINNER ?></p>
        </div>    
    </a>
    <a href="adminViewLiveBid.php">
    <div class="five-div darkbg-box third-mid-box">
    	<img src="img/dash4.png" class="dash-img" alt="<?php echo _ADMINDASH_BIDDING_IN_PROGRESS ?>" title="<?php echo _ADMINDASH_BIDDING_IN_PROGRESS ?>">
        <p class="five-div-title"><?php echo _ADMINDASH_BIDDING_IN_PROGRESS ?></p>
    </div>      
    </a>  
</div>

<style>
.home-div{
background: rgba(9,197,249,1);
background: -moz-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: -webkit-gradient(left top, right bottom, color-stop(0%, rgba(9,197,249,1)), color-stop(100%, rgba(4,92,233,1)));
background: -webkit-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: -o-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: -ms-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: linear-gradient(135deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#09c5f9', endColorstr='#045ce9', GradientType=1 );
}

</style>

<?php include 'bottomButton.php'; ?>
<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Message 1 !!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Message 2 !!"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "Message 3 !!"; 
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>