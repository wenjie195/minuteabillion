<header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
        <div class="big-container-size hidden-padding" id="top-menu">
            <div class="float-left left-logo-div">
                <img src="img/logo.png" class="logo-img" alt="minuteabillion" title="minuteabillion">
            </div>



            <div class="right-menu-div float-right before-header-div">
				<a href="index.php" class="white-text menu-margin-right menu-item opacity-hover">
                	Home
                </a>
                <a class="white-text menu-margin-right menu-item opacity-hover open-signup">
                    Sign Up
                </a>             
            	<a class="white-text menu-item menu-margin-right opacity-hover open-login">
                    Login
                </a>  

                <div class="dropdown  menu-item menu-a pink-hover-text hover1 hover-effect menu-margin-right">
                    Language / 语言
                    <div class="dropdown-content yellow-dropdown-content">
                        <p class="dropdown-p"><a href="<?php $link ?>?lang=en"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text">English</a></p>
                        <p class="dropdown-p"><a href="<?php $link ?>?lang=ch"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text">中文</a></p>
                    </div>
                </div> 
            	             
            	<!--
                <div class="dropdown  white-text menu-item">
                 -
               
                            	<img src="img/dropdown.png" class="dropdown-img" >

                	<div class="dropdown-content yellow-dropdown-content">
                        <p class="dropdown-p pink-hover"><a href="userUploadArticles.php"  class="menu-padding dropdown-a menu-a pink-hover">-</a></p>
                        <p class="dropdown-p pink-hover"><a href="viewArticles.php"  class="menu-padding dropdown-a menu-a pink-hover">-</a></p>
                	</div>
                </div> -->          

          
                           	<div id="dl-menu" class="dl-menuwrapper before-dl">
                                <button class="dl-trigger">Open Menu</button>
                                <ul class="dl-menu">
                                  <li><a href="index.php">Home</a></li>
                                  <li><a class="open-signup">Sign Up</a></li>                                
                                  <li><a class="open-login">Login</a></li>
                                </ul>
							</div> 
                                       	
            </div>
        </div>

</header>
