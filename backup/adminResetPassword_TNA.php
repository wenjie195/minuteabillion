<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

// $userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
// $userData = $userDetails[0];

$allUser = getUser($conn, "WHERE user_type = 1");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<meta property="og:title" content="<?php echo _ADMIN_MEMBER ?> | minuteabillion" />
<title><?php echo _ADMIN_MEMBER ?> | minuteabillion</title>
<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header.php'; ?>
<div class="width100 black-bg min-height menu-distance same-padding text-center">

   				<img src="img/profile-page.png" class="title-icon" alt="<?php echo _ADMIN_MEMBER ?>" title="<?php echo _ADMIN_MEMBER ?>">
                <h1 class="title-h1 blue-text"><?php echo _ADMIN_MEMBER ?></h1>
                <div class="title-border"></div>
                <div class="clear"></div> 
                
                
                <div class="table-scroll margin-top30">
                    <table class="table-css">
                        <thead>
                            <tr>
                                <th><?php echo _PROFILE_NO ?></th>
                                <th><?php echo _MAINJS_INDEX_USERNAME ?></th>
                                <th><?php echo _MAINJS_INDEX_EMAIL ?></th>
                                <th><?php echo _MAINJS_INDEX_CONTACT ?></th>
                                <th><?php echo _ADMIN_BALANCE ?></th>
                                <th><?php echo _PROFILE_BIDDING_HISTORY ?></th>
                                <th><?php echo _ADMIN_DETAILS ?></th>
                                <th><?php echo _ADMINDASH_RESETPW ?></th>
                            </tr>
                        </thead>
                        <!-- <tbody>
                            <tr>
                                <td>1.</td>
                                <td>Maggie</td>
                                <td>J123123123</td>
                                <td>Bitcoin - 0.1</td>
                                <td>View</td>
                                <td>Reset</td>
                            </tr>                                            
                        </tbody> -->

                        <tbody>
                            <?php
                            if($allUser)
                            {
                                for($cnt = 0;$cnt < count($allUser) ;$cnt++)
                                {
                                ?>    
                                    <tr>
                                        <td><?php echo ($cnt+1)?></td>
                                        <td><?php echo $allUser[$cnt]->getUsername();?></td>
                                        <td><?php echo $allUser[$cnt]->getEmail();?></td>
                                        <td><?php echo $allUser[$cnt]->getPhone();?></td>
                                        <td>
                                        800
                                        </td>
                                        <td>
                                            <form method="POST" action="adminViewUserBidHistory.php">
                                                <button class="clean blue-button small-btn" type="submit" name="user_uid" value="<?php echo $allUser[$cnt]->getUid();?>">
                                                    <?php echo _ADMINDASH_VIEW ?>
                                                </button>
                                            </form>                                        
                                        </td>
                                        <td>
                                            <form method="POST" action="#">
                                                <button class="clean blue-button small-btn" type="submit" name="user_uid" value="<?php echo $allUser[$cnt]->getUid();?>">
                                                    <?php echo _ADMINDASH_VIEW ?>
                                                </button>
                                            </form>                                        
                                        </td>                                        
                                        <td>
                                            <form method="POST" action="utilities/adminResetUserPasswordFunction.php" >
                                                <button class="clean blue-button small-btn" type="submit" name="user_uid" value="<?php echo $allUser[$cnt]->getUid();?>">
                                                    <?php echo _ADMINDASH_RESET ?>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                <?php
                                }
                            }
                            ?> 
                        </tbody>

                    </table>
                </div>                        
           
    
</div>

<style>
.profile-div{
background: rgba(9,197,249,1);
background: -moz-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: -webkit-gradient(left top, right bottom, color-stop(0%, rgba(9,197,249,1)), color-stop(100%, rgba(4,92,233,1)));
background: -webkit-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: -o-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: -ms-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: linear-gradient(135deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#09c5f9', endColorstr='#045ce9', GradientType=1 );
}

</style>

<?php include 'bottomButton.php'; ?>
<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "User Password Reset !!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "fail to update password !!"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>