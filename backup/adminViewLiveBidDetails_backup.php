<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BidData.php';
require_once dirname(__FILE__) . '/classes/BidRecord.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<meta property="og:title" content="<?php echo _BIDDING_ITEMS ?> | minuteabillion" />
<title><?php echo _BIDDING_ITEMS ?> | minuteabillion</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 black-bg min-height menu-distance same-padding text-center">

    <?php
    if(isset($_POST['bid_uid']))
    {
    $conn = connDB();
    $bidDetails = getBidData($conn,"WHERE uid = ? ", array("uid") ,array($_POST['bid_uid']),"s");

    // $userBidDetails = getBidRecord($conn,"WHERE trade_uid = ? ", array("trade_uid") ,array($_POST['bid_uid']),"s");

    $bidAmount100 = getBidRecord($conn,"WHERE trade_uid = ? AND amount = 100 ", array("trade_uid") ,array($_POST['bid_uid']),"s");
    $bidAmount250 = getBidRecord($conn,"WHERE trade_uid = ? AND amount = 250 ", array("trade_uid") ,array($_POST['bid_uid']),"s");
    $bidAmount500 = getBidRecord($conn,"WHERE trade_uid = ? AND amount = 500 ", array("trade_uid") ,array($_POST['bid_uid']),"s");
    $bidAmount1000 = getBidRecord($conn,"WHERE trade_uid = ? AND amount = 1000 ", array("trade_uid") ,array($_POST['bid_uid']),"s");
    $bidAmount1500 = getBidRecord($conn,"WHERE trade_uid = ? AND amount = 1500 ", array("trade_uid") ,array($_POST['bid_uid']),"s");
    $bidAmount2000 = getBidRecord($conn,"WHERE trade_uid = ? AND amount = 2000 ", array("trade_uid") ,array($_POST['bid_uid']),"s");
    $bidAmount3000 = getBidRecord($conn,"WHERE trade_uid = ? AND amount = 3000 ", array("trade_uid") ,array($_POST['bid_uid']),"s");
    $bidAmount4000 = getBidRecord($conn,"WHERE trade_uid = ? AND amount = 4000 ", array("trade_uid") ,array($_POST['bid_uid']),"s");
    $bidAmount5000 = getBidRecord($conn,"WHERE trade_uid = ? AND amount = 5000 ", array("trade_uid") ,array($_POST['bid_uid']),"s");

    ?>

        <img src="bidItemImage/<?php echo $bidDetails[0]->getImage(); ?>" class="title-icon" alt="<?php echo $bidDetails[0]->getBidName(); ?>" title="<?php echo $bidDetails[0]->getBidName(); ?>">
        <h1 class="title-h1 blue-text"><?php echo $bidDetails[0]->getBidName(); ?></h1>

            <div class="table-scroll margin-top30">
                <?php
                if($bidAmount100)
                {   
                    $totalBid100 = count($bidAmount100);
                }
                else
                {   $totalBid100 = 0;   }
                ?>
                <h1 class="title-h1 blue-text">Bid Amount 100 (<?php echo $totalBid100?>)</h1>
                <table class="table-css">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Username</th>
                            <th>Bid Amount</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if($bidAmount100)
                        {
                        for($cnt = 0;$cnt < count($bidAmount100) ;$cnt++)
                        {
                        ?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $bidAmount100[$cnt]->getUsername();?></td>
                                <td><?php echo $bidAmount100[$cnt]->getAmount();?></td>
                                <td><?php echo $bidAmount100[$cnt]->getDateCreated();?></td>
                            </tr> 
                        <?php
                        }
                        }
                        ?>                                           
                    </tbody>
                </table>
            </div>

            <div class="table-scroll margin-top30">
                <?php
                if($bidAmount250)
                {   
                    $totalBid250 = count($bidAmount250);
                }
                else
                {   $totalBid250 = 0;   }
                ?>
                <h1 class="title-h1 blue-text">Bid Amount 250 (<?php echo $totalBid250;?>)</h1>
                <table class="table-css">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Username</th>
                            <th>Bid Amount</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if($bidAmount250)
                        {
                        for($cnt = 0;$cnt < count($bidAmount250) ;$cnt++)
                        {
                        ?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $bidAmount250[$cnt]->getUsername();?></td>
                                <td><?php echo $bidAmount250[$cnt]->getAmount();?></td>
                                <td><?php echo $bidAmount250[$cnt]->getDateCreated();?></td>
                            </tr> 
                        <?php
                        }
                        }
                        ?>                                           
                    </tbody>
                </table>
            </div>

            <div class="table-scroll margin-top30">
                <?php
                if($bidAmount500)
                {   
                    $totalBid500 = count($bidAmount500);
                }
                else
                {   $totalBid500 = 0;   }
                ?>
                <h1 class="title-h1 blue-text">Bid Amount 500 (<?php echo $totalBid500?>)</h1>
                <table class="table-css">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Username</th>
                            <th>Bid Amount</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if($bidAmount500)
                        {
                        for($cnt = 0;$cnt < count($bidAmount500) ;$cnt++)
                        {
                        ?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $bidAmount500[$cnt]->getUsername();?></td>
                                <td><?php echo $bidAmount500[$cnt]->getAmount();?></td>
                                <td><?php echo $bidAmount500[$cnt]->getDateCreated();?></td>
                            </tr> 
                        <?php
                        }
                        }
                        ?>                                           
                    </tbody>
                </table>
            </div>

            <div class="table-scroll margin-top30">
                <?php
                if($bidAmount1000)
                {   
                    $totalBid1000 = count($bidAmount1000);
                }
                else
                {   $totalBid1000 = 0;   }
                ?>
                <h1 class="title-h1 blue-text">Bid Amount 1000 (<?php echo $totalBid1000?>)</h1>
                <table class="table-css">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Username</th>
                            <th>Bid Amount</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if($bidAmount1000)
                        {
                        for($cnt = 0;$cnt < count($bidAmount1000) ;$cnt++)
                        {
                        ?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $bidAmount1000[$cnt]->getUsername();?></td>
                                <td><?php echo $bidAmount1000[$cnt]->getAmount();?></td>
                                <td><?php echo $bidAmount1000[$cnt]->getDateCreated();?></td>
                            </tr> 
                        <?php
                        }
                        }
                        ?>                                           
                    </tbody>
                </table>
            </div>

            <div class="table-scroll margin-top30">
                <?php
                if($bidAmount1500)
                {   
                    $totalBid1500 = count($bidAmount1500);
                }
                else
                {   $totalBid1500 = 0;   }
                ?>
                <h1 class="title-h1 blue-text">Bid Amount 1500 (<?php echo $totalBid1500?>)</h1>
                <table class="table-css">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Username</th>
                            <th>Bid Amount</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if($bidAmount1500)
                        {
                        for($cnt = 0;$cnt < count($bidAmount1500) ;$cnt++)
                        {
                        ?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $bidAmount1500[$cnt]->getUsername();?></td>
                                <td><?php echo $bidAmount1500[$cnt]->getAmount();?></td>
                                <td><?php echo $bidAmount1500[$cnt]->getDateCreated();?></td>
                            </tr> 
                        <?php
                        }
                        }
                        ?>                                           
                    </tbody>
                </table>
            </div>

            <div class="table-scroll margin-top30">
                <?php
                if($bidAmount2000)
                {   
                    $totalBid2000 = count($bidAmount2000);
                }
                else
                {   $totalBid2000 = 0;   }
                ?>
                <h1 class="title-h1 blue-text">Bid Amount 2000 (<?php echo $totalBid2000?>)</h1>
                <table class="table-css">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Username</th>
                            <th>Bid Amount</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if($bidAmount2000)
                        {
                        for($cnt = 0;$cnt < count($bidAmount2000) ;$cnt++)
                        {
                        ?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $bidAmount2000[$cnt]->getUsername();?></td>
                                <td><?php echo $bidAmount2000[$cnt]->getAmount();?></td>
                                <td><?php echo $bidAmount2000[$cnt]->getDateCreated();?></td>
                            </tr> 
                        <?php
                        }
                        }
                        ?>                                           
                    </tbody>
                </table>
            </div>

            <div class="table-scroll margin-top30">
                <?php
                if($bidAmount3000)
                {   
                    $totalBid3000 = count($bidAmount3000);
                }
                else
                {   $totalBid3000 = 0;   }
                ?>
                <h1 class="title-h1 blue-text">Bid Amount 3000 (<?php echo $totalBid3000?>)</h1>
                <table class="table-css">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Username</th>
                            <th>Bid Amount</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if($bidAmount3000)
                        {
                        for($cnt = 0;$cnt < count($bidAmount3000) ;$cnt++)
                        {
                        ?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $bidAmount3000[$cnt]->getUsername();?></td>
                                <td><?php echo $bidAmount3000[$cnt]->getAmount();?></td>
                                <td><?php echo $bidAmount3000[$cnt]->getDateCreated();?></td>
                            </tr> 
                        <?php
                        }
                        }
                        ?>                                           
                    </tbody>
                </table>
            </div>

            <div class="table-scroll margin-top30">
                <?php
                if($bidAmount4000)
                {   
                    $totalBid4000 = count($bidAmount4000);
                }
                else
                {   $totalBid4000 = 0;   }
                ?>
                <h1 class="title-h1 blue-text">Bid Amount 4000 (<?php echo $totalBid4000?>)</h1>
                <table class="table-css">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Username</th>
                            <th>Bid Amount</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if($bidAmount4000)
                        {
                        for($cnt = 0;$cnt < count($bidAmount4000) ;$cnt++)
                        {
                        ?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $bidAmount4000[$cnt]->getUsername();?></td>
                                <td><?php echo $bidAmount4000[$cnt]->getAmount();?></td>
                                <td><?php echo $bidAmount4000[$cnt]->getDateCreated();?></td>
                            </tr> 
                        <?php
                        }
                        }
                        ?>                                           
                    </tbody>
                </table>
            </div>

            <div class="table-scroll margin-top30">
                <?php
                if($bidAmount5000)
                {   
                    $totalBid5000 = count($bidAmount5000);
                }
                else
                {   $totalBid5000 = 0;   }
                ?>
                <h1 class="title-h1 blue-text">Bid Amount 5000 (<?php echo $totalBid5000?>)</h1>
                <table class="table-css">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Username</th>
                            <th>Bid Amount</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if($bidAmount5000)
                        {
                        for($cnt = 0;$cnt < count($bidAmount5000) ;$cnt++)
                        {
                        ?>
                            <tr>
                                <td><?php echo ($cnt+1)?></td>
                                <td><?php echo $bidAmount5000[$cnt]->getUsername();?></td>
                                <td><?php echo $bidAmount5000[$cnt]->getAmount();?></td>
                                <td><?php echo $bidAmount5000[$cnt]->getDateCreated();?></td>
                            </tr> 
                        <?php
                        }
                        }
                        ?>                                           
                    </tbody>
                </table>
            </div>

    <?php
    }
    ?>


<div class="clear"></div>

</div>

<style>
.bid-div{
background: rgba(9,197,249,1);
background: -moz-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: -webkit-gradient(left top, right bottom, color-stop(0%, rgba(9,197,249,1)), color-stop(100%, rgba(4,92,233,1)));
background: -webkit-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: -o-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: -ms-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: linear-gradient(135deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#09c5f9', endColorstr='#045ce9', GradientType=1 );
}
</style>

<?php include 'bottomButton.php'; ?>
<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Bid Placed Successfully !!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "fail to update credit on user table !!"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR !!";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "fail to backup bid data !!"; 
        }
        else if($_GET['type'] == 5)
        {
            $messageType = "fail to record bid data !!"; 
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }

}
?>

<script type="text/javascript" src="js/firefly.js"></script>
<script>
  $.firefly({
    color: '#3399ff',
    minPixel: 1,
    maxPixel: 4,
    total : 60,
    on: '#firefly'
});</script>
<script>

// Set the date we're counting down to
var countDownDate = new Date("Aug 30, 2020 15:37:25").getTime();

// Update the count down every 1 second
var x = setInterval(function() {

  // Get today's date and time
  var now = new Date().getTime();

  // Find the distance between now and the count down date
  var distance = countDownDate - now;

  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);

  // Display the result in the element with id="demo"
  document.getElementById("timer").innerHTML = days + "d " + hours + "h "
  + minutes + "m " + seconds + "s ";
  document.getElementById("timer2").innerHTML = days + "d " + hours + "h "
  + minutes + "m " + seconds + "s ";
  document.getElementById("timer3").innerHTML = days + "d " + hours + "h "
  + minutes + "m " + seconds + "s ";  
  // If the count down is finished, write some text
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("timer").innerHTML = "Ended";
	document.getElementById("timer2").innerHTML = "Ended";
	document.getElementById("timer3").innerHTML = "Ended";
  }
}, 1000);
</script>
<?php

if( $_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['send_email_button'])) {

    // EDIT THE 2 LINES BELOW AS REQUIRED
    $email_to = "sherry2.vidatech@gmail.com";
    $email_subject = "Contact Form via minuteabillion website";
 
    function died($error) 
	{
        // your error code can go here
		echo '<script>alert("We are very sorry, but there were error(s) found with the form you submitted.\n\nThese errors appear below.\n\n';
		echo $error;
        echo '\n\nPlease go back and fix these errors.\n\n")</script>';
        die();
    }
 
 
    // validation expected data exists
    if(!isset($_POST['name']) ||
        !isset($_POST['email']) ||
		!isset($_POST['telephone'])) {
        died('We are sorry, but there appears to be a problem with the form you submitted.');       
    }
	
     
 
    $first_name = $_POST['name']; // required
    $email_from = $_POST['email']; // required
	$telephone = $_POST['telephone']; //required
    $comments = $_POST['comments']; 
    $contactOption = $_POST['contact-option']; // required
    $contactMethod = null;
	
	//$error_message = '<script>alert("The name you entered does not appear to be valid.");</script>';
	//if($first_name == ""){
	//	echo $error_message;
	//}

    if($contactOption == null || $contactOption == ""){
        $contactMethod = "don\'t bother me";
    }else if($contactOption == "contact-more-info"){
        $contactMethod = "I want to be contacted with more information about your company's offering marketing services and consulting";
    }else if($contactOption == "contact-on-request"){
        $contactMethod = "I just want to be contacted based on my request/ inquiry";
    }else{
        $contactMethod = "error getting contact options";
		$error_message .="Error getting contact options\n\n";
    }

    $error_message = "";
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
 
  if(!preg_match($email_exp,$email_from)) {
    $error_message .= 'The email address you entered does not appear to be valid.\n';
  }
 
 
    $string_exp = "/^[A-Za-z .'-]+$/";
 
  if(!preg_match($string_exp,$first_name)) {
    $error_message .= 'The name you entered does not appear to be valid.\n';
  }
 


 
  if(strlen($error_message) > 0) {
    died($error_message);
  }
 
    $email_message = "Form details below.\n\n";
 
     
    function clean_string($string) {
      $bad = array("content-type","bcc:","to:","cc:","href");
      return str_replace($bad,"",$string);
    }
 
    $email_message .= "Name: ".clean_string($first_name)."\n";
    $email_message .= "Email: ".clean_string($email_from)."\n";
	$email_message .= "Telephone: ".clean_string($telephone)."\n";   
    $email_message .= "Message : ".clean_string($comments)."\n";
    $email_message .= "Contact Option : ".clean_string($contactMethod)."\n";

// create email headers
$headers = 'From: '.$email_from."\r\n".
'Reply-To: '.$email_from."\r\n" .
'X-Mailer: PHP/' . phpversion();
@mail($email_to, $email_subject, $email_message, $headers);  
echo '<script>alert("Thank you! We will be in contact shortly!")</script>';

?>
<!-- include your own success html here -->

<!--Thank you for contacting us. We will be in touch with you very soon.-->
<?php
 
}
?>

</body>
</html>