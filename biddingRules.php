<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

// $uid = $_SESSION['uid'];
// if (isset($_SESSION['uid']))
// {
//     $uid = $_SESSION['uid'];
// }

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<meta property="og:title" content="<?php echo _BIDDING_RULES2 ?> | Bid Win 劲拍" />
<title><?php echo _BIDDING_RULES2 ?> | Bid Win 劲拍</title>


<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

<div class="width100 data-white-bg min-height same-padding text-center" id="firefly">



   <div class="data-content" id="style-1">
   
         <h1 class="data-h1">BIDDING RULES</h1>
         <p class="data-p">
         	This agreement ("<b>Agreement</b>") is made between:<br>
			<a href="https://www.minuteabillion.co/" target="_blank" class="white-to-blue-link">www.minuteabillion.co</a>, a website setup under the BVI Business Companies Act No 16 of 2004 with its business address at P.O. Box 957, Offshore Incorporations Centre, Road Town, Tortola, British Virgin Islands. (“<b>the Company</b>”, “<b>we</b>”, “<b>our</b>” or “<b>us</b>”);<br>
            and<br>       
         	You, being an individual or a body corporate ("<b>subscriber</b>” or “<b>you</b>"), who wishes to consider bidding to items in response to a Bidding Event (defined hereunder) published on the Website (defined hereunder).<br>
			This Agreement constitutes a binding contract between the Company and you as a <b>Subscriber</b>. This Agreement shall be effective when you tick a box confirming that you have read and understood the terms and conditions and agree to be bound by the terms of this Agreement.            
         </p>
  
         <h1 class="data-h1">DEFINITIONS AND INTERPRETATIONS</h1>
         <p class="data-p"><b>Definitions</b><br>In this Agreement, unless the context otherwise requires, the following expressions shall have the meanings set forth opposite such expressions:-</p>
			<table class="transparent-table">
            	<tr>
                	<td><b>Bidding Event</b></td>
                    <td>means invitation or offer to bid for items, services or products, through the Website</td>
            	</tr>
            	<tr>
                	<td><b>Bidding Event Materials</b></td>
                    <td>means all information relating to Bidding Event, including but not limited to the specifications, market value, products or services terms and conditions as well as any other relevant information published on the Website;</td>
            	</tr>   
            	<tr>
                	<td><b>Bid Payment</b></td>
                    <td>means payment of money for the purchase of the item in response to an Bidding Event;</td>
            	</tr>                  
            	<tr>
                	<td><b>Bidding Period</b></td>
                    <td>means the period of time when Bidding Event is listed on the Website and open to the Bidders for bidding;</td>
            	</tr>                 
            	<tr>
                	<td><b>Foreign Subscriber</b></td>
                    <td>means subscriber who accesses a Bidding Event from outside BVI or who is not a BVI citizen; and</td>
            	</tr>                
            	<tr>
                	<td><b>Website</b></td>
                    <td>means www.minuteabillion.co and/or direct sub-domains, subpages, redirected sub-domains, subpages and any other web display pages directly and indirectly correlated to <a href="https://www.minuteabillion.co/" target="_blank" class="white-to-blue-link">www.minuteabillion.co</a></td>
            	</tr>                              
            </table>   
         <h1 class="data-h1">Interpretations</h1>
         <p class="data-p">
           <ul class="data-ul">
             <li class="data-li">The headings in this Agreement are inserted for convenience of reference only and shall not be taken, read and construed as essential parts of this Agreement.</li>
             <li class="data-li">All references to provisions of statutes include such provisions as modified, re-certified or re-enacted. All references to this Agreement include this Agreement as amended or modified;</li>
             <li class="data-li">All references to:-</li>
                 <ol class="roman-ol">
                     <li class="data-li">the singular includes the plural and vice versa;</li>
                     <li class="data-li">a gender includes all genders;</li>
                     <li class="data-li">a date or time of day is a reference to the BVI time; and</li>
                     <li class="data-li">business day shall mean a day (excluding Saturdays, Sundays and public holidays) on which government offices and banks are open for business in British Virgin Islands</li>
                 </ol>
           </ul>
         </p>
   
         
         <h1 class="data-h1">PURPOSE OF THIS AGREEMENT</h1>
         <div class="data-p">
          <p class="data-p2">Please take notice that the information in this Agreement is not exhaustive and you must also read the following documents which are available on our Website:</p>
                 <ol>
                     <li class="data-li">Bidding Terms and Conditions, which can be found at the page of each Bidding Event;</li>
                     <li class="data-li">Our Privacy Notice, which can be found at <a href="https://www.minuteabillion.co/" target="_blank" class="white-to-blue-link">www.minuteabillion.co</a> ; and</li>
                     <li class="data-li">Other useful information can be found at our main webpage at <a href="https://www.minuteabillion.co/" target="_blank" class="white-to-blue-link">www.minuteabillion.co</a></li>
                 </ol>          
         </div>
   
        <h1 class="data-h1">HOW TO BECOME A SUBSCRIBER?</h1>
        <div class="data-p">
            <p class="data-title">Eligibility Criteria for Subscriber</p>
            <p class="data-p2">Individual subscriber must be at least 18 years old. Whereas subscriber who is a body corporate must be a legal commercial entity established under your respective jurisdiction.</p>
            
            <p class="data-title">Registration Process</p>
            <p class="data-p2">In order to be eligible to bid in the Bidding Event through the Website, Subscriber must first be accepted as a member by the Company.</p>
            <p class="data-p2">To become a subscriber:</p>  
			<ol>
           		<li class="data-li">Subscriber will need to sign up through the Website and be registered as a user. Once we have verified your email address, you may be required to provide further information which may include personal and payment options and relevant documents. Information that you will need to provide during the registration process, shall include but not limited to the following:
                <ol class="latin-ol">
                	<li>full name;</li>
                    <li>residential address (which need not be in BVI);</li>
                    <li>email address;</li>
                    <li>contact number; and</li>
                    <li>address in country of residence.</li>
                </ol>
                
                </li> 
            	<li class="data-li">Upon receipt of your information and relevant documents (if required), we will send you a verification email and to activate your account with us. Do note that pursuant to Anti- Terrorism Financing and Proceeds of Unlawful Activities Act 2001 and other relevant regulations and guidelines, you may need to furnish further information in relation to the source of funds which you seek to utilise in the Bidding Event either generally or in any particular Bidding Event, upon our request at any time and from time to time.</li>
                <li class="data-li">We will notify you as to whether or not your registration to become a Subscriber is successful. We may, at its absolute discretion, decline to accept any registered user as a Subscriber without providing any reason.</li>
                <li class="data-li">Once you are successfully registered as a Subscriber, you will be able to view details of current Bidding Events.</li>
                
            </ol>        
            <p class="data-title">Foreign Subscriber</p>
            <p class="data-p2">Our services and the Website are designed for persons who are residents in BVI. Foreign Subscriber is solely responsible for ensuring compliance with all laws in their country of residence or the country from which they may access the Website and the Company accepts no responsibility for any breach of such laws by the Foreign Subscriber.</p>
            <p class="data-p2">Some countries have laws which regulate the Bidding Event of products. No Bidding Event is made through the Website or may be accepted by any Foreign Subscriber if to do so would constitute a breach of any foreign laws; and any such Bidding Event or any purchase of services or products as a result of such Bidding Event will be void and of no effect in relation to that country and persons subject to its laws.</p>

              
        </div>
        
       

        <h1 class="data-h1">SUBSCRIBER’ OBLIGATIONS AND REPRESENTATIONS</h1>
        <div class="data-p">
         <p class="data-p2">Each Subscriber, by accessing the Website (whether from within or outside BVI or who are resident within or outside BVI), shall be deemed to represent to the Company and its board of directors that on each occasion the Website is accessed; a bid is made for an Bidding Event; or any money is utilised in response to the Bidding Event, the Subscriber is doing so in compliance with all applicable laws and all information provided to the Company is true, accurate and complete.</p> 
         <p class="data-p2">The Subscriber will be liable to the Company and any of its directors and hereby agrees to indemnify the Company and any of its directors against any and all claims, losses, expenses or liabilities arising out of or in connection with any such non-compliance or alleged noncompliance by the Subscriber. The directors of the Company may enforce these obligations directly against the Subscribers to the extent permitted by the applicable law.</p>
         <p class="data-p2">You must notify us should any of the details or information which you have provided to us have changed. This is particularly important for any change in email address, as email is the principal medium of communication from the Company to you.</p> 
        </div>
		<h1 class="data-h1">SUSPENTION, REMOVAL AND TERMINATION</h1>        
        <div class="data-p">
         
         <p class="data-p2">We reserve the right to refuse, suspend or cancel your membership as a Subscriber at any time without providing any reason whatsoever if at any time we are concerned about any of the following:</p>
         <ol>
         	<li>where there is doubt as to whether you are at least 18 years old;</li>
            <li>whether you have provided true and accurate information in relation to your personal details and all other dealings with us;</li>
            <li>where the information provided by you cannot be verified or confirmed; or</li>
            <li>the non-satisfactory result of our anti-money laundering and other legal compliance checks on you;</li>
            <li>where you have breached any of your obligations under this Agreement;</li>
            <li>where there is doubt as to whether you are an undischarged bankrupt or being a body corporate, being wound up or liquidated; or</li>
            <li>such other matter affect your eligibility as an Subscriber.</li>
         </ol>
        </div>
        <h1 class="data-h1">OUR SERVICES</h1>        
        <div class="data-p">
         
         <p class="data-p2">Our primary service is that we provide a platform whereby Subscribers may bid interested services or products through our Website. Pursuant to the Privacy Notice published on the Website, we may maintain a database of registered Subscribers who will receive information on the Bidding Event.</p>
         <p class="data-p2">Merchants seeking to conduct or carry out Bidding Events may approach us, or they may be approached by us, with a prospect of them conducting or carrying out their Bidding Event through the Website to the Subscriber.</p>
          <p class="data-title">Checks</p>
          <p class="data-p2">We undertake limited checking on each services or products before publishing any Bidding Events on the Website. The limited checking is to verify the specifications and descriptions of the services or products to be featured in Bidding Event are not misleading. Any decision to allow Bidding Event to be listed on the Website following our checking is at the sole discretion of the Company.</p>
          <p class="data-p2">Our decision to feature the Bidding Event on the Website must not be taken by any person as an indication of the quality, functionality, safety and/or authenticity of the services or products or our endorsement of the same.</p>
          <p class="data-title">No Warranty</p>
          <p class="data-p2">We only perform preliminary checking to the services or products. We are not obliged to monitor after sale services or actual delivery of services or products. The decision to proceed with any bidding is based on your independent assessment and made at your own risk.</p>
          <p class="data-p2">We do not provide any warranty, promise or even any indication about the quality, functionality, safety and/or authenticity of the services or products.</p>
          <p class="data-title">Fees and Charges</p>
          <p class="data-p2">Subscriber do not need to pay any fees or charges to us in relation to any Bidding Event.</p>
          <p class="data-p2">We reserve the right to introduce fees or charges payable by Subscribers. Any introduction of fees or charges payable by Subscribers will only be effective upon this Agreement being varied or revised in the manner provided herein.</p>
          <p class="data-p2">Subscribers are not entitled to receive any interest or other return in respect of the moneys paid by them to us relating to any Bidding Events on the Website.</p>
          <p class="data-title">Limited Role of the Company in the Bidding Event Process</p>
          <p class="data-p2">It is important to note that all Bidding Event Materials are prepared and provided on best effort basis. The Company makes no representation in relation to the accuracy or completeness of any information contained in the Bidding Event Materials and gives no warranty to the Subscribers.</p>
          <p class="data-p2">Subscribers shall decide independently whether there has been sufficient or accurate disclosure prior to making any decision to bid. We accept no liability whatsoever in regard to your decision to bid or not to bid.</p>
          
        </div>
        <h1 class="data-h1">THE BIDDING PROCESS</h1>
        <div class="data-p">
         
         <p class="data-p2">For any Bidding Event, the Company will arrange for the Bidding Event Materials to be uploaded to the Website. Subscribers who have completed the registration process will be able to browse and review the Bidding Event Materials.</p>
         <p class="data-p2">Subscribers will be able to seek further information and ask questions of the Bidding Events through the live chat or forum on the Website.</p>
         <p class="data-p2">The Bidding Events listed on the Website are open to Subscribers for bidding during the Bidding Period only. Each Bidding Event will lapse upon the expiry date and time published on the Website and may be extended on the option of the Company by publishing a notice on the Website before the expiry date. The Bidding Period may be terminated earlier than the scheduled expiry date and time upon the occurrence of any of the following:
         <ol class="latin-ol">
         	<li>the Bidding Event has received bids amounting to its targeted number of bids sought; or</li>
            <li>the Company cancels or withdraws the Bidding Event.</li>
         </ol>
         </p>
        </div> 
        <h1 class="data-h1">PAYING FOR YOUR BIDS</h1>
        <div class="data-p">
         
         <p class="data-p2">The Bidding Event terms will typically contain obligations about the timing of settling payment for your Bid Payment. After you have made your bid in response to a Bidding Event on the Website, you will be directed to the payment page and you are required to make payment within Twelve (12) hours upon expiry of the Bidding Period or such other time specifically stipulated therein. Subscribers may select the following payment methods in respect of payment for Bid Payment:
         <ol class="latin-ol">
         	<li>E-payment via the available payment system or payment gateway on the Website; or</li>
            <li>Manual online payment to the designated account of the Company published on the page for the Bidding Event, if any.</li>
         </ol>
         </p>
         <p class="data-p2">For Subscribers who wish to make their Bid Payment by utilising the E-payment via the available payment system or payment gateway, you will be required to confirm your bank account details.</p>
         <p class="data-p2">For manual online payments, the Subscriber must transfer the Bid Payment to the designated account of the Company and email the bank-in slip to the Company at hello@bidwin.online</p>
         <p class="data-p2">The Bid Payment received from Subscribers will be held by the Company. The money paid by you into our account will be released to the respective merchants for the services or products featuring in the Bidding Event when the following conditions have been satisfied:
         <ol class="latin-ol">
         	<li>the targeted number of bids has been met; and</li>
            <li>the delivery of the services or products has been completed Or upon expiry of Twenty One (21) days from expiry of the Event Period, whichever is the earlier; and</li>
            <li>any other additional conditions in respect of the Bidding Event imposed by the Company have been fulfilled and/or waived.</li>
         </ol>         
         </p>
         
         
         
        </div> 
        <h1 class="data-h1">PURCHASE OF SERVICES OR PRODUCTS</h1>
        <div class="data-p">
         
         <p class="data-p2">Subscribers who bid for services or products pursuant to a Bidding Event are deemed to have agreed and confirmed that, amongst others the specifications and details of the services or products are acceptable.</p>
        </div>  
        <div class="data-p">
         <p class="data-title">Delivery of Services or Products</p>
         <p class="data-p2">Once Bidding Event is completed, the respective merchant(s) will be obliged to deliver to you the services or products at its sole responsibility.</p>
        </div>        
        <div class="data-p">
         <p class="data-title">Non-Completion and Refund of Payment</p>
         <p class="data-p2">Should any Bidding Event not proceed for any reason whatsoever, including where the targeted number of bids has not been achieved, or if your bid in response to the Bidding Event is unsuccessful and not accepted, the Company will, within Thirty (30) business days, return your Bid Payment to you at the bank account details of which were provided by you, without any interest and after deduction of any transaction fees(s) and/or any exchange rate loss.</p>
        </div>         
        <div class="data-p">
         <p class="data-title">Overbid</p>
         <p class="data-p2">While bids for Bidding Events will typically be accepted in order of receipt of the complete bid together with the payment, in the event the targeted numbers of bid sought your bid may be rejected.</p>
         <p class="data-p2">If there is an overbid (that is, bids received are for aggregate amounts that exceed the targeted numbers of bid sought) then the Company may either close the Bidding Event immediately or increase the maximum numbers of bid sought in consultation.</p>
        </div>         
        <h1 class="data-h1">INDEMNITY</h1>
        <div class="data-p">
         
         <p class="data-p2">You agree to indemnify and hold harmless the Company, its officers and employees from and against any and all claims, losses, expenses, demands or liabilities arising out of or in connection with your breach of this Agreement, including solicitors’ fees and costs incurred by the Company defending against them.</p>
        </div>          
        <h1 class="data-h1">NOTICE</h1>
        <div class="data-p">
         
         <p class="data-p2">Notice to us: Any notice pursuant to this Agreement shall be in writing and be given by sending the same by email to us at hello@bidwin.online</p>
         <p class="data-p2">Notice to you: Any notice pursuant to this Agreement shall be in writing and be given by sending the same by email to your email address; or by prepaid registered post or personally sent to your physical address, that you have provided during user registration process.</p>
        </div>         
        
        <h1 class="data-h1">VARIATION</h1>
        <div class="data-p">
           <p class="data-p2">We reserve the right to vary the terms and conditions of this Agreement from time to time. Any access to or use of the Website by you and any bid for any Bidding Event after the effective date of the amended Agreement shall constitute your agreement to and acceptance of the changed Agreement.</p>
        </div>

        <h1 class="data-h1">FURTHER INFORMATION</h1>
        <div class="data-p"> 
         
         <p class="data-p2">All information which we are required to make available to Subscribers are provided on the Website.</p>
         <p class="data-p2">If Subscribers seek for any other information from us, we may agree to provide the requested information subject always to any obligations of privacy or confidentiality that we may owe to third parties. The requested information will ordinarily be sent by email, but should it be sent by courier or post then additional charges as stated above may apply.</p>
         <p class="data-p2">Subscribers will be able to view their own bidding history by logging onto their account on the Website and accessing their transaction history. No charges will be made for any such direct access by the Subscribers.</p>
         
         
        </div> 
		<h1 class="data-h1">GOVERNING LAW</h1>
        <div class="data-p"> 
         
         <p class="data-p2">This Agreement shall be governed by and construed in accordance with the laws of BVI.</p>
         <p class="data-p2">Updated as at 9th June 2020.</p>
         <p class="data-p2"><b>SIGNED BY</b><br><a href="https://www.minuteabillion.co/" target="_blank" class="white-to-blue-link">www.minuteabillion.co</a></p>
         <p class="data-p2"><b>SIGNED BY</b><br>You, as the Subscribers, by ticking the box confirming that you have read and understood the terms and conditions and agree to be bound by the terms of this Agreement.</p>
        </div> 
	
        
        
        

   
   
</div>

</div>
<?php include 'bottomButton.php'; ?>
<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully Login!"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Wrong Password !!"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "There are no user with this username ! <br> Please register !";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }

    if($_SESSION['messageType'] == 2)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Reset password link has been sent to your email !"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "no user with ths email !!"; 
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "wrong email format !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

<script type="text/javascript" src="js/firefly.js"></script>
<script>
  $.firefly({
    color: '#3399ff',
    minPixel: 1,
    maxPixel: 4,
    total : 60,
    on: '#firefly'
});</script>
<script>

// Set the date we're counting down to
var countDownDate = new Date("Aug 30, 2020 15:37:25").getTime();

// Update the count down every 1 second
var x = setInterval(function() {

  // Get today's date and time
  var now = new Date().getTime();

  // Find the distance between now and the count down date
  var distance = countDownDate - now;

  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);

  // Display the result in the element with id="demo"
  document.getElementById("timer").innerHTML = days + "d " + hours + "h "
  + minutes + "m " + seconds + "s ";
  document.getElementById("timer2").innerHTML = days + "d " + hours + "h "
  + minutes + "m " + seconds + "s ";
  document.getElementById("timer3").innerHTML = days + "d " + hours + "h "
  + minutes + "m " + seconds + "s ";  
  // If the count down is finished, write some text
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("timer").innerHTML = "Ended";
	document.getElementById("timer2").innerHTML = "Ended";
	document.getElementById("timer3").innerHTML = "Ended";
  }
}, 1000);
</script>
<?php

if( $_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['send_email_button'])) {

    // EDIT THE 2 LINES BELOW AS REQUIRED
    $email_to = "sherry2.vidatech@gmail.com";
    $email_subject = "Contact Form via Bid Win website";
 
    function died($error) 
	{
        // your error code can go here
		echo '<script>alert("We are very sorry, but there were error(s) found with the form you submitted.\n\nThese errors appear below.\n\n';
		echo $error;
        echo '\n\nPlease go back and fix these errors.\n\n")</script>';
        die();
    }
 
 
    // validation expected data exists
    if(!isset($_POST['name']) ||
        !isset($_POST['email']) ||
		!isset($_POST['telephone'])) {
        died('We are sorry, but there appears to be a problem with the form you submitted.');       
    }
	
     
 
    $first_name = $_POST['name']; // required
    $email_from = $_POST['email']; // required
	$telephone = $_POST['telephone']; //required
    $comments = $_POST['comments']; 
    $contactOption = $_POST['contact-option']; // required
    $contactMethod = null;
	
	//$error_message = '<script>alert("The name you entered does not appear to be valid.");</script>';
	//if($first_name == ""){
	//	echo $error_message;
	//}

    if($contactOption == null || $contactOption == ""){
        $contactMethod = "don\'t bother me";
    }else if($contactOption == "contact-more-info"){
        $contactMethod = "I want to be contacted with more information about your company's offering marketing services and consulting";
    }else if($contactOption == "contact-on-request"){
        $contactMethod = "I just want to be contacted based on my request/ inquiry";
    }else{
        $contactMethod = "error getting contact options";
		$error_message .="Error getting contact options\n\n";
    }

    $error_message = "";
    $email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
 
  if(!preg_match($email_exp,$email_from)) {
    $error_message .= 'The email address you entered does not appear to be valid.\n';
  }
 
 
    $string_exp = "/^[A-Za-z .'-]+$/";
 
  if(!preg_match($string_exp,$first_name)) {
    $error_message .= 'The name you entered does not appear to be valid.\n';
  }
 


 
  if(strlen($error_message) > 0) {
    died($error_message);
  }
 
    $email_message = "Form details below.\n\n";
 
     
    function clean_string($string) {
      $bad = array("content-type","bcc:","to:","cc:","href");
      return str_replace($bad,"",$string);
    }
 
    $email_message .= "Name: ".clean_string($first_name)."\n";
    $email_message .= "Email: ".clean_string($email_from)."\n";
	$email_message .= "Telephone: ".clean_string($telephone)."\n";   
    $email_message .= "Message : ".clean_string($comments)."\n";
    $email_message .= "Contact Option : ".clean_string($contactMethod)."\n";

// create email headers
$headers = 'From: '.$email_from."\r\n".
'Reply-To: '.$email_from."\r\n" .
'X-Mailer: PHP/' . phpversion();
@mail($email_to, $email_subject, $email_message, $headers);  
echo '<script>alert("Thank you! We will be in contact shortly!")</script>';

?>
<!-- include your own success html here -->

<!--Thank you for contacting us. We will be in touch with you very soon.-->
<?php
 
}
?>
<script type="text/javascript" src="js/firefly.js"></script>
<script>
  $.firefly({
    color: '#3399ff',
    minPixel: 1,
    maxPixel: 4,
    total : 60,
    on: '#firefly'
});</script>
<script>
</body>
</html>