<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$conn->close();
?>

<div class="footer-div">
	<p class="footer-p">@ <?php echo $time;?> Bid Win 劲拍, <?php echo _JS_ALL_RIGHT ?></p>
</div>
<div class="bottom-button-spacing"></div>
<div class="width100 bottom-button black-bg overflow">
	
        <div class="bottom-div back-div" onclick="goBack()">
            <img src="img/menu-01.png" alt="<?php echo _BOTTOM_BACK ?>" title="<?php echo _BOTTOM_BACK ?>">
        </div>
   


<?php
if(isset($_SESSION['uid']))
{
?>

    <?php
    if($_SESSION['user_type'] == 0)
    //admin
    {
    ?>
        <a href="adminDashboard.php">
            <div class="bottom-div home-div">
                <img src="img/menu-02.png" alt="<?php echo _BOTTOM_HOME ?>" title="<?php echo _BOTTOM_HOME ?>">
            </div>
        </a>       
    
        	<a href="adminViewLiveBid.php">
            <div class="bottom-div bid-div">
                <img src="img/menu-03.png" alt="<?php echo _BOTTOM_BID ?>" title="<?php echo _BOTTOM_BID ?>">
            </div> 
            </a>
            <a href="depositRequest.php">
            <div class="bottom-div topup-div">
                <img src="img/menu-04.png" alt="<?php echo _PROFILE_DEPOSIT_FUND ?>" title="<?php echo _PROFILE_DEPOSIT_FUND ?>">
            </div>  
            </a> 
            <a href="adminViewMembers.php">
            <div class="bottom-div profile-div">
                <img src="img/menu-05.png" alt="<?php echo _ADMIN_MEMBER ?>" title="<?php echo _ADMIN_MEMBER ?>">
            </div> 
            </a> 
           
    <?php
    }
    elseif($_SESSION['user_type'] == 1)
    //user
    {
    ?>
        <a href="index.php">
            <div class="bottom-div home-div">
                <img src="img/menu-02.png" alt="<?php echo _BOTTOM_HOME ?>" title="<?php echo _BOTTOM_HOME ?>">
            </div>
        </a>       
    
        	<a href="biddingItems.php">
            <div class="bottom-div bid-div">
                <img src="img/menu-03.png" alt="<?php echo _BOTTOM_BID ?>" title="<?php echo _BOTTOM_BID ?>">
            </div> 
            </a>
            <a href="depositFund.php">
                <div class="bottom-div topup-div">
                    <img src="img/menu-04.png" alt="<?php echo _PROFILE_DEPOSIT_FUND ?>" title="<?php echo _PROFILE_DEPOSIT_FUND ?>">
                </div>   
            </a>
           <a href="profile.php">
            <div class="bottom-div profile-div">
                <img src="img/menu-05.png" alt="<?php echo _PROFILE ?>" title="<?php echo _PROFILE ?>">
            </div>      
    		</a>
    
    
    <?php
    }
    ?>

<?php
}
else
//no login
{
?>
        <a href="index.php">
            <div class="bottom-div home-div">
                <img src="img/menu-02.png" alt="<?php echo _BOTTOM_HOME ?>" title="<?php echo _BOTTOM_HOME ?>">
            </div>
        </a>   
       
            <div class="bottom-div bid-div open-login">
                <img src="img/menu-03.png" alt="<?php echo _BOTTOM_BID ?>" title="<?php echo _BOTTOM_BID ?>">
            </div>
        
            <div class="bottom-div topup-div open-login">
                <img src="img/menu-04.png" alt="<?php echo _PROFILE_DEPOSIT_FUND ?>" title="<?php echo _PROFILE_DEPOSIT_FUND ?>">

                
            </div>   
            <div class="bottom-div profile-div open-login">
                <img src="img/menu-05.png" alt="<?php echo _PROFILE ?>" title="<?php echo _PROFILE ?>">
            </div>  
        <?php
        }
        ?>

</div>

