<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BidData.php';
require_once dirname(__FILE__) . '/classes/BidRecord.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

require_once dirname(__FILE__) . '/init.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$bidAmount = getBidRecord($conn,"WHERE user_uid = ? ", array("user_uid") ,array($uid),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

// require "init.php";

$basicInfo = $coin->GetBasicProfile();
// var_dump($basicInfo);
$username = $basicInfo['result']['public_name'];

?>

<!DOCTYPE html>
<html lang="en">
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<meta property="og:title" content="<?php echo _PAYMENT ?> | Bid Win 劲拍" />
<title><?php echo _PAYMENT ?> | Bid Win 劲拍</title>
<?php include 'css.php'; ?>	
</head>
<body>
<?php include 'header.php'; ?>

<div class="width100 black-bg min-height menu-distance text-center">

    <img src="img/payment.png" class="title-icon" alt="<?php echo _PAYMENT ?>" title="<?php echo _PAYMENT ?>">
    <h1 class="title-h1 blue-text"><?php echo _PAYMENT ?></h1>
    <div class="title-border margin-bottom30"></div>
    <h1 class="title-h1"><?php echo _PAYMENT_PAY_WITH ?></h1>
    <p style="font-style: italic;"><?php echo _PAYMENT_TO ?> <strong><?php echo $username; ?></strong></p>
    <div class="middle-width mob-padding">
        <form action="process.php" method="post" autocomplete="off">
            <label for="amount" class="input-top-p"><?php echo _DEPOSIT_AMOUNT ?> (USD)</label>
            <input type="text"  name="amount" placeholder="<?php echo _DEPOSIT_AMOUNT ?> (USD)" class="input-name clean text-center">
            <br>
            <label for="email"  class="input-top-p"><?php echo _MAINJS_INDEX_EMAIL ?></label>
            <input type="email" name="email" class="input-name clean text-center" placeholder="<?php echo _MAINJS_INDEX_EMAIL ?>">
            <div class="clear"></div>
            <div class="width100 text-center margin-top20">
            <button type="submit"  class="blue-button white-text clean pointer ow-width100"><?php echo _PAYMENT_PAY_TO ?> <?php echo $username; ?></button>
            </div>
        </form>
    </div>

</div>

<div class="clear"></div>

<?php include 'bottomButton.php'; ?>
<?php include 'js.php'; ?>

</body>
</html>